# Lithium

[![pipeline status](https://gitlab.com/asayapin/lithium/badges/master/pipeline.svg)](https://gitlab.com/asayapin/lithium/-/commits/master)
[![coverage report](https://gitlab.com/asayapin/lithium/badges/master/coverage.svg)](https://gitlab.com/asayapin/lithium/-/commits/master)
[![docs](https://img.shields.io/badge/docs-gitlab_pages-blue)](https://asayapin.gitlab.io/lithium)

LISP interpreter engine for .NET, made as lightweight as possible

## Command overview

This interpreter implements some core commands, some additional commands
for string tasks (formatting & interpolation) and several commands for 
.Net integration

In most cases all commands' arguments are evaluated on list evaluation

### General commands

Includes:
- arithmetics: `+`, `-`, `*`, `/`, `%`, `^` - for 1+ operand
- `set`
- `atom` - check whether value is atom
- `isdef` - check whether symbol is defined
- `cond` - ternary
- comparison: `eq`/`==`, `neq`/`!=`, `>`, `>=`, `<`, `<=`
- logic: `&`, `|`, `~` (inversion)
- strings formatting
  - `format`, `$`, `fmt` - wrapper for `String.Format`
  - `inline`, `$$`, `fmt-il` - evaluate all args and concat using `String.Join(" ")`
- functions
  - `eval`
  - `defun`, `def`
  - `lambda`, `λ`, `\`
  - `proc`, `|>`
- list operations: 
  - `car`, `head`, `first`
  - `cdr`, `tail`, `rest`
  - `c([ad]+)r`
  - `@`, `ix`, `idx`, `index`
  - `cat`, `concat`
  - `append`
  - `map`, `->`
  - `reduce`, `<-` (left-to-right)
  - `filter`, `?>`
- .NET integration
  - `new`
  - `nav`, `.`
  - `import`
  
## Usage

...is simple:

```c#
var engine = Engine.Create(); // call .Extend(containerInstance) to add your methods
var scope = new LexicalScope(null); // or provide extension container here, instead of null

object result = engine.Evaluate("\"hello!\"", scope);
```

## Extensibility

...is simple as hell:

Create root class inheriting `asjp.lithium.Contracts.Extensibility.ExtensionsPartsCollectorBase`. 
Override methods if needed, but make sure that instance is obtainable 

```c#
public class Collector : asjp.lithium.Contracts.Extensibility.ExtensionsPartsCollectorBase {}
```

...then create extension class(es) holding methods. Convention is simple:
- type should have `ExtensionPartAttribute`
- method should have `NamesAttribute`
- method can declare last parameter as variadic

```c#
[ExtensionPart]
public class Example : PartBase {
    [Names("any-name", "you:can:scope:names:like-that"), Eager]
    private static IAtom MyJoin(object first, IScope scope, params object[] variadic){
        return
           (first.ToString()
            + ": " 
            + string.Join(", ", variadic)
            .Wrap();
    }
}
```