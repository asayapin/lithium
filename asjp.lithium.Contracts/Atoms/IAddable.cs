﻿namespace asjp.lithium.Contracts.Atoms
{
    /// <summary>
    /// Declares addition capabilities for atom
    /// </summary>
    public interface IAddable : IAtom
    {
        /// <summary>
        /// Perform addition on this and parameter
        /// </summary>
        /// <param name="right">Right-hand addition operand</param>
        /// <returns>Addition result</returns>
        IAtom Add(IAtom right);
    }
}