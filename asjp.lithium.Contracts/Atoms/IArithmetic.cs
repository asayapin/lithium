﻿namespace asjp.lithium.Contracts.Atoms
{
    /// <summary>
    /// Declares arithmetic capabilities for atom
    /// </summary>
    public interface IArithmetic : IAtom
    {
        /// <summary>
        /// Subtract argument from this
        /// </summary>
        /// <param name="right">Value to subtract</param>
        /// <returns></returns>
        IAtom Sub(IAtom right);
        /// <summary>
        /// Multiply this and argument
        /// </summary>
        /// <param name="right"></param>
        /// <returns></returns>
        IAtom Mul(IAtom right);
        /// <summary>
        /// Divide this by argument
        /// </summary>
        /// <param name="right">Divisor</param>
        /// <returns>Division result</returns>
        IAtom Div(IAtom right);
        /// <summary>
        /// Get value of this modulo argument
        /// </summary>
        /// <param name="right"></param>
        /// <returns></returns>
        IAtom Mod(IAtom right);
        /// <summary>
        /// Raise this to power of argument
        /// </summary>
        /// <param name="right"></param>
        /// <returns></returns>
        IAtom Pow(IAtom right);
        /// <summary>
        /// Pure negate (i.e. multiply by -1)
        /// </summary>
        /// <returns>Negated value, not affecting original one</returns>
        IAtom Neg();
        /// <summary>
        /// Pure increment
        /// </summary>
        /// <returns>Incremented value, not affecting original one</returns>
        IAtom Inc();
        /// <summary>
        /// Pure decrement
        /// </summary>
        /// <returns>Decremented value, not affecting original one</returns>
        IAtom Dec();
    }
}