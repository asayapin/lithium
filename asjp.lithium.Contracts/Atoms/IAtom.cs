﻿using System;
using System.Threading.Tasks;
using asjp.lithium.Contracts.Services;

namespace asjp.lithium.Contracts.Atoms
{
    /// <summary>
    /// Central contract for interpreter objects - all other are inherited from this
    /// </summary>
    public interface IAtom : IEquatable<IAtom>
    {
        /// <summary>
        /// Evaluate atom - implementation-specific
        /// </summary>
        /// <param name="scope">Scope to evaluate in</param>
        /// <returns>Evaluation result</returns>
        IAtom Evaluate(IScope scope);
        /// <summary>
        /// Get CLR value of atom
        /// </summary>
        /// <param name="scope">Scope to consider while obtaining value</param>
        /// <returns>Inner CLR value of atom</returns>
        object GetValue(IScope scope);
        (int line, int col, int offset) Position { get; }

        event Func<IAtom, Task> EvaluationStarted;
    }
}