﻿namespace asjp.lithium.Contracts.Atoms
{
    /// <summary>
    /// Atom supporting enumeration
    /// </summary>
    public interface IEnumerableAtom : IAtom
    {
        /// <summary>
        /// First atom of sequence
        /// </summary>
        /// <returns></returns>
        IAtom Head();
        /// <summary>
        /// Last atom of sequence - implementation-specific
        /// </summary>
        /// <returns></returns>
        IAtom Tail();
        /// <summary>
        /// Raw view over atoms in sequence
        /// </summary>
        IAtom[] Atoms { get; }
    }
}