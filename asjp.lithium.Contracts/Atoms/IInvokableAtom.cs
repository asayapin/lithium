﻿using System.Collections.Generic;
using asjp.lithium.Contracts.Services;

namespace asjp.lithium.Contracts.Atoms
{
    /// <summary>
    /// Declares invocation capabilities for atoms 
    /// </summary>
    public interface IInvokableAtom : IAtom
    {
        /// <summary>
        /// Invoke atom as function
        /// </summary>
        /// <param name="args">Arguments to pass to atom</param>
        /// <param name="scope">Scope to consider for invocation</param>
        /// <returns>Invocation result</returns>
        IAtom Invoke(IEnumerable<IAtom> args, IScope scope);
    }
}