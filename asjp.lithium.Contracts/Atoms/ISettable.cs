﻿using asjp.lithium.Contracts.Services;

namespace asjp.lithium.Contracts.Atoms
{
    /// <summary>
    /// Declares capability to update value
    /// </summary>
    public interface ISettable : IAtom
    {
        /// <summary>
        /// Updates value - implementation-specific
        /// </summary>
        /// <param name="value">value to set</param>
        /// <param name="scope">scope to consider</param>
        /// <returns>status of operation</returns>
        bool TrySet(IAtom value, IScope scope);
    }
}