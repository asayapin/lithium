﻿using asjp.lithium.Contracts.Services;

namespace asjp.lithium.Contracts.Atoms
{
    public interface TypeAtom : IAtom
    {
        IAtom CreateInstance(IScope scope, IAtom[] args);
    }
}