﻿using System;
using System.Linq;
using System.Reflection;

namespace asjp.lithium.Contracts.Extensibility
{
    /// <summary>
    /// Reflection-based collector for scope extensions
    /// </summary>
    public abstract class CollectorBase
    {
        /// <summary>
        /// Determines if type is suitable for processing
        /// </summary>
        /// <param name="type">Type to check</param>
        /// <returns>True if type should be processed with <see cref="ProcessMethod"/></returns>
        protected abstract bool IsSuitable(Type type);
        /// <summary>
        /// Determines if method should be processed with <see cref="ProcessMethod"/> 
        /// </summary>
        /// <param name="method">Method to check</param>
        /// <returns>True if method should be processed</returns>
        protected abstract bool IsSuitable(MethodInfo method);
        /// <summary>
        /// Flags to apply while retrieving methods information
        /// </summary>
        protected abstract BindingFlags Flags { get; }
        /// <summary>
        /// Act on suitable method - typically, add to internal storage
        /// </summary>
        /// <param name="method">Method to process</param>
        protected abstract void ProcessMethod(MethodInfo method);
        /// <summary>
        /// Act on suitable type - typically, do nothing
        /// </summary>
        /// <param name="type">Type to process</param>
        protected abstract void ProcessType(Type type);

        /// <summary>
        /// Process assembly metadata to find all methods suitable for scope extension
        /// </summary>
        /// <param name="assembly">Entry-point assembly</param>
        protected void Collect(Assembly assembly = null) {
            foreach (var type in (assembly ?? GetType().Assembly).GetTypes().Where(IsSuitable))
            {
                ProcessType(type);
                foreach (var methodInfo in type.GetMethods(Flags).Where(IsSuitable))
                    ProcessMethod(methodInfo);
            }
        }
    }
}