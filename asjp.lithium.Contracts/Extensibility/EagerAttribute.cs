﻿using System;

namespace asjp.lithium.Contracts.Extensibility
{
    /// <summary>
    /// Marks method or argument as requiring call to <see cref="asjp.lithium.Contracts.Atoms.IAtom.Evaluate"/> prior to use
    /// </summary>
    [AttributeUsage(AttributeTargets.Parameter | AttributeTargets.Method)]
    public class EagerAttribute : Attribute {
    
    }
}