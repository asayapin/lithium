﻿using System;

namespace asjp.lithium.Contracts.Extensibility
{
    /// <summary>
    /// Marks class holding methods for scope extension
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class ExtensionPartAttribute : Attribute { }
}