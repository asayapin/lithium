﻿using asjp.lithium.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace asjp.lithium.Contracts.Extensibility
{
    /// <summary>
    /// Engine method metadata
    /// </summary>
    public class EngineMethodInfo
    {
        /// <summary>
        /// Method to call
        /// </summary>
        public readonly Delegate Method;
        /// <summary>
        /// Names to bind to this method
        /// </summary>
        public readonly string[] Names;
        /// <summary>
        /// True to overwrite previously added methods with same name(s)
        /// </summary>
        public readonly bool CanHide;

        public EngineMethodInfo(Delegate method, string[] names, bool canHide = true)
        {
            Method = method;
            Names = names;
            CanHide = canHide;
        }
    }
    /// <summary>
    /// Simple collector utilizing <see cref="ExtensionPartAttribute"/>
    /// </summary>
    public abstract class ExtensionsPartsCollectorBase : CollectorBase, IExtensionsContainer
    {
        readonly List<EngineMethodInfo> _definitions = new();

        /// <summary>
        /// Checks whether type is marked with <see cref="ExtensionPartAttribute"/>
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        protected override bool IsSuitable(Type type) => !(type.GetCustomAttribute<ExtensionPartAttribute>() is null);
        /// <summary>
        /// Checks whether method is marked with <see cref="NamesAttribute"/>
        /// </summary>
        /// <param name="method"></param>
        /// <returns></returns>
        protected override bool IsSuitable(MethodInfo method) => !(method.GetCustomAttribute<NamesAttribute>() is null);
        /// <summary>
        /// Flags to search for static non-inherited members of any visibility
        /// </summary>
        protected override BindingFlags Flags => BindingFlags.DeclaredOnly | BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public;
        /// <summary>
        /// Store method metadata in inner storage
        /// </summary>
        /// <param name="method"></param>
        protected override void ProcessMethod(MethodInfo method)
        {
            try
            {
                var em = method.CreateDelegate(
                    Expression.GetDelegateType(
                        method
                            .GetParameters()
                            .Select(x => x.ParameterType)
                            .Append(method.ReturnType)
                            .ToArray()),
                    null);
                _definitions.Add(new(em, method.GetCustomAttribute<NamesAttribute>().Names));
            }
            catch { /* unused */ }
        }
        /// <summary>
        /// Does nothing
        /// </summary>
        /// <param name="type"></param>
        protected override void ProcessType(Type type) { }

        /// <summary>
        /// Initialize with collecting methods from provided (or declaring) assembly
        /// </summary>
        /// <param name="assembly"></param>
        protected ExtensionsPartsCollectorBase(Assembly assembly = null) => Collect(assembly);

        /// <inheritdoc cref="asjp.lithium.Contracts.Services.IExtensionsContainer"/>
        public IEnumerable<EngineMethodInfo> GetMethodDefinitions() => _definitions.AsEnumerable();
    }
}