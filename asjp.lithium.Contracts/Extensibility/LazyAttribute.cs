﻿using System;

namespace asjp.lithium.Contracts.Extensibility
{
    /// <summary>
    /// Marks argument as requiring no evaluation prior to use
    /// </summary>
    [AttributeUsage(AttributeTargets.Parameter)]
    public class LazyAttribute : Attribute {}
}