﻿using System;

namespace asjp.lithium.Contracts.Extensibility
{
    /// <summary>
    /// Specifies names to bind for marked method
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class NamesAttribute : Attribute
    {
        /// <summary>
        /// Provided names
        /// </summary>
        public string[] Names { get; }

        /// <summary>
        /// Specify names to bind for marked method
        /// </summary>
        /// <param name="names">Bound names</param>
        public NamesAttribute(params string[] names)
        {
            Names = names;
        }
    }
}