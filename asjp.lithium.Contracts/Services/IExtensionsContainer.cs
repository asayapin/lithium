﻿using System.Collections.Generic;
using asjp.lithium.Contracts.Extensibility;

namespace asjp.lithium.Contracts.Services
{
    /// <summary>
    /// Container for methods for scope extension
    /// </summary>
    public interface IExtensionsContainer
    {
        /// <summary>
        /// Sequence of method metadata to load into scope
        /// </summary>
        /// <returns></returns>
        IEnumerable<EngineMethodInfo> GetMethodDefinitions();
    }
}
