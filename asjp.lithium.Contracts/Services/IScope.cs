﻿using System;
using System.Collections.Generic;
using asjp.lithium.Contracts.Atoms;

namespace asjp.lithium.Contracts.Services
{
    /// <summary>
    /// Evaluation scope
    /// </summary>
    public interface IScope
    {
        /// <summary>
        /// Parent scopes
        /// </summary>
        IEnumerable<IScope> Parents { get; }
        /// <summary>
        /// Methods loaded to current scope
        /// </summary>
        IReadOnlyDictionary<string, Delegate> Methods { get; }
        /// <summary>
        /// Data loaded to current scope
        /// </summary>
        IReadOnlyDictionary<string, IAtom> Data { get; }
        /// <summary>
        /// Traverse scope and parents searching for atom with specified name
        /// </summary>
        /// <param name="name">name to search for</param>
        /// <param name="result">search result</param>
        /// <returns>true if atom was found, false otherwise</returns>
        bool TryGetData(string name, out IAtom result);
        /// <summary>
        /// Traverse scopes to find delegate bound to provided name
        /// </summary>
        /// <param name="name">name to search for</param>
        /// <returns>Bound delegate or null</returns>
        Delegate Resolve(string name);
        /// <summary>
        /// Bind name to atom for current scope
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value">Atom to bind</param>
        void SetData(string name, IAtom value);
        /// <summary>
        /// Set of namespaces imported in current scopes graph
        /// </summary>
        IEnumerable<string> ImportedNamespaces { get; }
        /// <summary>
        /// Add parent to current scope
        /// </summary>
        /// <param name="scope">Additional parent scope</param>
        /// <returns>Current scope</returns>
        IScope AddParent(IScope scope);
        /// <summary>
        /// Create sub-scope based on current one
        /// </summary>
        /// <param name="extensions">Extensions to load in child scope</param>
        /// <param name="parent">Additional parent for scope</param>
        /// <returns></returns>
        IScope CreateChild(IExtensionsContainer extensions = null, IScope parent = null);
        /// <summary>
        /// Add imported namespace to current scope
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        bool AddImport(string i);
        /// <summary>
        /// provides stack trace
        /// </summary>
        Stack<StackFrame> Trace { get; }
    }
}