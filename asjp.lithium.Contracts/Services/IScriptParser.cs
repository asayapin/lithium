﻿using asjp.lithium.Contracts.Atoms;

namespace asjp.lithium.Contracts.Services
{
    /// <summary>
    /// Script -> IAtom converter
    /// </summary>
    public interface IScriptParser
    {
        /// <summary>
        /// Parse script and convert it to sequence of atoms
        /// </summary>
        /// <param name="script">Script to parse</param>
        /// <returns>Corresponding sequence of atoms</returns>
        IAtom[] Parse(string script);
    }
}