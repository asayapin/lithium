﻿using asjp.lithium.Contracts.Atoms;

namespace asjp.lithium.Contracts.Services;

public record StackFrame
{
    public StackFrame(IAtom target)
    {
        Target = target;
    }

    public IAtom Target { get; }
}