﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using asjp.lithium.Contracts.Atoms;
using asjp.lithium.ObjectModel;
using asjp.lithium.Contracts.Services;
using Glob = System.Globalization;

namespace asjp.lithium.Defaults
{
    /// <summary>
    /// Default LISP parser implementation
    /// </summary>
    public class DefaultParser : IScriptParser
    {
        /// <summary>
        /// Convert script to corresponding sequence of atoms 
        /// </summary>
        /// <param name="script"></param>
        /// <returns></returns>
        public IAtom[] Parse(string script)
        {
            var tokensEnumerator = GetTokens(script).GetEnumerator();
            var ret = new List<IAtom>();
            try
            {
                while (tokensEnumerator.MoveNext())
                    ret.Add(Parse(tokensEnumerator.Current, tokensEnumerator));
            }
            finally
            {
                tokensEnumerator.Dispose();
            }
            return ret.ToArray();
        }

        /// <summary>
        /// Split script to tokens corresponding to lexical entities
        /// </summary>
        /// <param name="script"></param>
        /// <returns>Sequence of lexical entities</returns>
        public IEnumerable<(string, int, int, int)> GetTokens(string script)
        {
            bool isString = false, isComment = false, escapeThis = false;
            var buffer = new StringBuilder();
            int offset = 0, line = 0, col = 0;

            foreach (var @char in script)
            {
                if (isComment && !char.IsWhiteSpace(@char)) continue;
                if (escapeThis)
                {
                    escapeThis = false;
                    buffer.Append(@char);
                    continue;
                }
                if (@char == '\\')
                {
                    escapeThis = isString;
                    if (!escapeThis)
                        buffer.Append(@char);
                    continue;
                }
                if (isString && @char != '"') buffer.Append(@char);
                else if (@char == '\'')
                {
                    if (buffer.Length == 0)
                        yield return ("'", line, col, offset);
                    else buffer.Append(@char);
                }
                else if (@char == '"')
                {
                    if (isString)
                    {
                        yield return ($@"""{buffer}""", line, col, offset);
                        buffer = new StringBuilder();
                    }
                    isString ^= true;
                }
                else if (char.IsWhiteSpace(@char))
                {
                    if (@char == '\n' || @char == '\r')
                    {
                        isComment = false;
                        line++;
                        col = 0;
                    }
                    if (buffer.Length != 0)
                    {
                        yield return (buffer.ToString(), line, col - buffer.Length, offset - buffer.Length);
                        buffer = new StringBuilder();
                    }
                }
                else if ("()".Contains(@char))
                {
                    if (buffer.Length > 0) yield return (buffer.ToString(), line, col - buffer.Length, offset - buffer.Length);
                    buffer = new StringBuilder();
                    yield return (@char.ToString(), line, col, offset);
                }
                else if (@char == ';')
                    isComment = true;
                else
                    buffer.Append(@char);

                offset++;
                col++;
            }
            if (buffer.Length > 0)
                yield return (buffer.ToString(), line, col, offset);
        }

        /// <summary>
        /// Convert set of tokens to atom - right-recursively
        /// </summary>
        /// <param name="item">item to start with</param>
        /// <param name="tokensEnumerator">cursor over token sequence - mutated in process of conversion</param>
        /// <returns>parsed atom</returns>
        IAtom Parse((string, int, int, int) item, IEnumerator<(string, int, int, int)> tokensEnumerator)
        {
            var (content, l, c, o) = item;
            if (content.StartsWith(@"""") && content.EndsWith(@""""))
                return new ValueAtom(content.Substring(1, content.Length - 2), (l, c, o));
            if (TryParseNumber(content, out var number))
                return new ValueAtom(number, (l, c, o));
            if (content == "'")
            {
                if (tokensEnumerator.MoveNext())
                    return new QuotedAtom(Parse(tokensEnumerator.Current, tokensEnumerator), (l, c, o));
            }
            else if (content == "(")
            {
                var atoms = new List<IAtom>();
                while (tokensEnumerator.MoveNext() && tokensEnumerator.Current.Item1 != ")")
                    atoms.Add(Parse(tokensEnumerator.Current, tokensEnumerator));
                if (!atoms.Any()) return ValueAtom.Nil;
                if (atoms.Count == 3 && atoms[1].GetValue(null)?.ToString() == ".")
                    return new TupleAtom(atoms[0], atoms[2], (l, c, o));
                return new ListAtom(atoms, (l, c, o));
            }
            else if (content.Equals("nil", StringComparison.CurrentCultureIgnoreCase))
                return ValueAtom.Nil;
            else if (content.Equals("t", StringComparison.CurrentCultureIgnoreCase))
                return ValueAtom.T;
            return new IdentifierAtom(content, (l, c, o));
        }

        /// <summary>
        /// helpers for numeric literals parsing
        /// </summary>
        static readonly Dictionary<string, Func<string, (bool, ValueType)>> Converters = new()
        {
            ["byte"] = s => byte.TryParse(s, out var b) ? (true, b) : new ValueTuple<bool, ValueType>(false, false),
            ["sbyte"] = s => sbyte.TryParse(s, out var b) ? (true, b) : new ValueTuple<bool, ValueType>(false, false),
            ["short"] = s => short.TryParse(s, out var b) ? (true, b) : new ValueTuple<bool, ValueType>(false, false),
            ["ushort"] = s => ushort.TryParse(s, out var b) ? (true, b) : new ValueTuple<bool, ValueType>(false, false),
            ["uint"] = s => uint.TryParse(s, out var b) ? (true, b) : new ValueTuple<bool, ValueType>(false, false),
            ["long"] = s => long.TryParse(s, out var b) ? (true, b) : new ValueTuple<bool, ValueType>(false, false),
            ["ulong"] = s => ulong.TryParse(s, out var b) ? (true, b) : new ValueTuple<bool, ValueType>(false, false),
            ["float"] = s => float.TryParse(s, out var b) ? (true, b) : new ValueTuple<bool, ValueType>(false, false),
            ["decimal"] = s => decimal.TryParse(s, out var b) ? (true, b) : new ValueTuple<bool, ValueType>(false, false),
        };

        /// <summary>
        /// parse string to numeric literal
        /// </summary>
        /// <param name="value">value to parse</param>
        /// <param name="number">resulting number</param>
        /// <returns>true if parsing completed successfully</returns>
        bool TryParseNumber(string value, out ValueType number)
        {
            var ns = Glob.NumberStyles.Any;
            var ci = Glob.CultureInfo.InvariantCulture;
            if (value.Contains(':'))
            {
                var vs = value.Split(':');
                if (Converters.ContainsKey(vs[1]))
                {
                    bool res;
                    (res, number) = Converters[vs[1]](vs[0]);
                    return res;
                }
                value = vs[0];
            }
            if (int.TryParse(value, ns, ci, out var i) && !value.Contains('.'))
            {
                number = i;
                return true;
            }
            if (double.TryParse(value, ns, ci, out var d))
            {
                number = d;
                return true;
            }
            number = null;
            return false;
        }
    }
}
