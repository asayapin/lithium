﻿using asjp.lithium.Contracts.Atoms;
using asjp.lithium.Contracts.Services;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace asjp.lithium.Defaults
{
    /// <summary>
    /// Default implementation of IScope
    /// </summary>
    public class LexicalScope : IScope
    {
        readonly List<IScope> _parents = new();

        readonly ConcurrentDictionary<string, IAtom> _data = new();

        readonly ConcurrentDictionary<string, Delegate> _methods =
            new();

        readonly HashSet<string> _importedNamespaces = new();

        /// <summary>
        /// Create scope
        /// </summary>
        /// <param name="container">Optional extensions container</param>
        public LexicalScope(IExtensionsContainer container = null)
        {
            if (container is not null)
                foreach (var info in container.GetMethodDefinitions())
                foreach (var name in info.Names)
                    if (!_methods.ContainsKey(name) || info.CanHide)
                        _methods[name] = info.Method;
        }

        /// <inheritdoc cref="IScope.Data"/>
        public IReadOnlyDictionary<string, IAtom> Data => _data;

        /// <inheritdoc cref="IScope.TryGetData"/>
        public bool TryGetData(string name, out IAtom result)
        {
            return TryFind(scope =>
            {
                var flag = scope.Data.TryGetValue(name, out var r);
                return (flag, r);
            }, out result);
        }
        /// <inheritdoc cref="IScope.Resolve"/>
        public Delegate Resolve(string name)
        {
            return TryFind(scope =>
            {
                var flag = scope.Methods.TryGetValue(name, out var e);
                return (flag, e);
            }, out var em)
                ? em
                : null;
        }
        /// <inheritdoc cref="IScope.SetData"/>
        public void SetData(string name, IAtom value) => _data[name] = value;
        /// <inheritdoc cref="IScope.ImportedNamespaces"/>
        public IEnumerable<string> ImportedNamespaces
        {
            get
            {
                var traversed = new HashSet<IScope>();
                var queue = new Queue<IScope>();
                queue.Enqueue(this);
                var result = new HashSet<string>();
                
                while (queue.Count > 0)
                {
                    var current = queue.Dequeue();
                    if (current is null || !traversed.Add(current)) continue;
                    if (current is not LexicalScope ls) continue;
                    foreach (var ns in ls._importedNamespaces)
                        result.Add(ns);
                }

                return result;
            }
        }
        /// <inheritdoc cref="IScope.Parents"/>
        public IEnumerable<IScope> Parents => _parents.AsEnumerable();
        /// <inheritdoc cref="IScope.Methods"/>
        public IReadOnlyDictionary<string, Delegate> Methods => _methods;
        /// <inheritdoc cref="IScope.AddImport"/>
        public bool AddImport(string s) => _importedNamespaces.Add(s);

        public Stack<StackFrame> Trace { get; } = new();

        /// <inheritdoc cref="IScope.AddParent"/>
        public IScope AddParent(IScope scope)
        {
            if (scope is null) return this;
            if (ReferenceEquals(this, scope)) return this;
            if (Parents.Any(x => ReferenceEquals(x, scope))) return this;
            _parents.Add(scope);
            return this;
        }
        /// <inheritdoc cref="IScope.CreateChild"/>
        public IScope CreateChild(IExtensionsContainer extensions = null, IScope parent = null) =>
            new LexicalScope(extensions).AddParent(this)
                .AddParent(parent);

        /// <summary>
        /// Breadth-first search by predicate in scope graph
        /// </summary>
        /// <param name="step">Search function - returns (true, T) on success of (false, _) to continue searching</param>
        /// <param name="result">search result</param>
        /// <typeparam name="T">any type</typeparam>
        /// <returns>true if search succeeds</returns>
        bool TryFind<T>(Func<IScope, (bool, T)> step, out T result)
        {
            result = default;

            var processed = new HashSet<IScope>();
            var queue = new Queue<IScope>();
            queue.Enqueue(this);
            bool flag;

            while (queue.Count > 0)
            {
                var curr = queue.Dequeue();
                if (curr is null) continue;
                processed.Add(curr);
                (flag, result) = step(curr);
                if (flag) return true;
                foreach (var s in curr.Parents)
                    if (processed.Add(s))
                        queue.Enqueue(s);
            }

            return false;
        }
    }
}