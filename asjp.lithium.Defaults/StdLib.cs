﻿using System;
using asjp.lithium.Contracts.Extensibility;

namespace asjp.lithium.Defaults
{
    /// <summary>
    /// Default extensions
    /// </summary>
    public class StdLib : ExtensionsPartsCollectorBase
    {
        /// <summary>
        /// singleton implementation field
        /// </summary>
        static readonly Lazy<StdLib> Lazy = new(() => new());

        /// <summary>
        /// Reference to singleton instance of container
        /// </summary>
        public static StdLib Instance => Lazy.Value;

        /// <summary>
        /// instantiation rules enforcing
        /// </summary>
        StdLib() { }
    }
}