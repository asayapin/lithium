﻿using asjp.lithium.Contracts.Atoms;
using asjp.lithium.Contracts.Extensibility;
using asjp.lithium.Contracts.Services;
using asjp.lithium.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
// ReSharper disable UnusedMember.Local
// ReSharper disable once UnusedType.Global

namespace asjp.lithium.Defaults.StdLibParts
{
    /// <summary>
    /// Arithmetic routines
    /// </summary>
    [ExtensionPart]
    [SuppressMessage(
        "CodeQuality", 
        "IDE0051:Remove unused private members", 
        Justification = "methods are decorated so that they are resolved in runtime; see StdLib & ExtensionPartsCollectorBase")]
    internal class Arithmetic : PartBase
    {
        /// <summary>
        /// Add up all provided atoms
        /// </summary>
        /// <param name="atoms">Atoms to sum</param>
        /// <returns>Sum</returns>
        /// <remarks>
        /// Usage: <br/>
        /// <code>(+ 1 2 3 4)</code> -> 10<br/>
        /// <code>(+ 1)</code> -> 1<br/>
        /// <code>(+ 1 2)</code> -> 3</remarks>
        [Names("+"), Eager]
        static IAtom Add(params IAtom[] atoms)
        {
            if (atoms.Length == 1) return atoms[0];
            return atoms.Skip(1).Aggregate(atoms[0], (acc, curr) => acc is IAddable ad ? ad.Add(curr) : acc);
        }
        /// <summary>
        /// Subtract values from first atom
        /// </summary>
        /// <param name="args">operands</param>
        /// <returns></returns>
        /// <remarks>Usage:<br/>
        /// <code>(- 1 2 3 4)</code> -> 8<br/>
        /// <code>(- 1)</code> -> 1<br/>
        /// <code>(- 1 2)</code> -> -1</remarks>
        [Names("-"), Eager]
        static IAtom Sub(params IAtom[] args) => InvokeOnIArithmetic(args, (ia, curr) => ia.Sub(curr));
        /// <summary>
        /// Multiply all provided args
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        /// <remarks>Usage:<br/>
        /// <code>(* 1 2 3 4)</code> -> 24<br/>
        /// <code>(* 1)</code> -> 1<br/>
        /// <code>(* 1 2)</code> -> 2</remarks>
        [Names("*"), Eager]
        static IAtom Mul(params IAtom[] args) => InvokeOnIArithmetic(args, (ia, curr) => ia.Mul(curr));
        /// <summary>
        /// Perform sequence of divisions
        /// </summary>
        /// <param name="args">Divisible, then sequence of divisors</param>
        /// <returns></returns>
        /// <remarks>Usage:<br/>
        /// <code>(/ 48 2 3 4)</code> -> 2<br/>
        /// <code>(/ 1)</code> -> 1<br/>
        /// <code>(/ 1 2)</code> -> 0<br/>
        /// <code>(/ 1.0 2)</code> -> 0.5</remarks>
        [Names("/"), Eager]
        static IAtom Div(params IAtom[] args) => InvokeOnIArithmetic(args, (ia, curr) => ia.Div(curr));
        /// <summary>
        /// Perform sequence of modulo operations
        /// </summary>
        /// <param name="args">Divisible, then set of divisors</param>
        /// <returns></returns>
        /// <remarks>Usage:<br/>
        /// <code>(% 15 9 4)</code> -> 2<br/>
        /// <code>(% 1)</code> -> 1<br/>
        /// <code>(% 1 2)</code> -> 1</remarks>
        [Names("%"), Eager]
        static IAtom Mod(params IAtom[] args) => InvokeOnIArithmetic(args, (ia, curr) => ia.Mod(curr));
        /// <summary>
        /// Raise to power
        /// </summary>
        /// <param name="args">Base, then sequence of powers</param>
        /// <returns></returns>
        /// <remarks>Usage:<br/>
        /// <code>(^ 2 3 4)</code> -> 4096<br/>
        /// <code>(^ 1)</code> -> 1<br/>
        /// <code>(^ 1 2)</code> -> 1</remarks>
        [Names("^"), Eager]
        static IAtom Pow(params IAtom[] args) => InvokeOnIArithmetic(args, (ia, curr) => ia.Pow(curr));
        /// <summary>
        /// Increment and re-set atom in current scope
        /// </summary>
        /// <param name="a"></param>
        /// <param name="scope"></param>
        /// <returns></returns>
        /// <remarks>Usage:<br/>
        /// <code>(++ a)</code> -> a+1<br/>
        /// <code>(++ 1)</code> -> ()</remarks>
        [Names("++", "inc")]
        static IAtom Inc(IAtom a, IScope scope)
        {
            if (a.Evaluate(scope) is not IArithmetic val || a.GetValue(scope) is not string name) return ValueAtom.Nil;
            var ret = val.Inc();
            scope.SetData(name, ret);
            return ret;

        }
        /// <summary>
        /// Decrement and re-set atom in current scope
        /// </summary>
        /// <param name="a"></param>
        /// <param name="scope"></param>
        /// <returns></returns>
        /// <remarks>Usage:<br/>
        /// <code>(-- a)</code> -> a-1<br/>
        /// <code>(-- 1)</code> -> ()</remarks>
        [Names("--", "dec")]
        static IAtom Dec(IAtom a, IScope scope)
        {
            if (a.Evaluate(scope) is IArithmetic val && a.GetValue(scope) is string name)
            {
                var ret = val.Dec();
                scope.SetData(name, ret);
                return ret;
            }
            return ValueAtom.Nil;
        }
        /// <summary>
        /// Negate atom value
        /// </summary>
        /// <param name="a"><see cref="IArithmetic"/> instance</param>
        /// <returns>Negated value for IArithmetic, nil otherwise</returns>
        /// <remarks>Usage:<br/>
        /// <code>(neg 3)</code> -> -3<br/>
        /// <code>(neg a)</code> -> -a<br/>
        /// <code>(neg '(1 2 3))</code> -> nil</remarks>
        [Names("neg"), Eager]
        static IAtom Neg(IAtom a) => a is IArithmetic atom ? atom.Neg() : ValueAtom.Nil;
    }
}
