﻿using asjp.lithium.Contracts.Atoms;
using asjp.lithium.Contracts.Extensibility;
using asjp.lithium.ObjectModel.Extensions;
using System;
using System.Diagnostics.CodeAnalysis;

// ReSharper disable UnusedMember.Local
// ReSharper disable once UnusedType.Global
namespace asjp.lithium.Defaults.StdLibParts
{
    /// <summary>
    /// Comparison routines
    /// </summary>
    [ExtensionPart]
    [SuppressMessage(
        "CodeQuality",
        "IDE0051:Remove unused private members",
        Justification =
            "methods are decorated so that they are resolved in runtime; see StdLib & ExtensionPartsCollectorBase")]
    internal class Comparison : PartBase
    {
        /// <summary>
        /// Check equality
        /// </summary>
        /// <param name="l"></param>
        /// <param name="r"></param>
        /// <returns></returns>
        /// <remarks>Usage:<br/>
        /// <code>(== 1 2)</code> -> nil<br/>
        /// <code>(== 1 1)</code> -> t</remarks>
        [Names("eq", "=="), Eager]
        static IAtom Eq(IAtom l, IAtom r) => l.Equals(r).Wrap();

        /// <summary>
        /// Check inequality
        /// </summary>
        /// <param name="l"></param>
        /// <param name="r"></param>
        /// <returns></returns>
        /// <remarks>Usage:<br/>
        /// <code>(!= 1 1)</code> -> ()<br/>
        /// <code>(!= 1 2)</code> -> t</remarks>
        [Names("neq", "!=", "<>"), Eager]
        static IAtom Neq(IAtom l, IAtom r) => Eq(l, r).Inv();

        /// <summary>
        /// Check that first is strictly greater than second
        /// </summary>
        /// <param name="l"></param>
        /// <param name="r"></param>
        /// <returns></returns>
        /// <remarks>Usage:<br/>
        /// <code>(> 1 2)</code> -> ()<br/>
        /// <code>(> 2 1)</code> -> t</remarks>
        [Names("gr", ">"), Eager]
        static IAtom Gr(IAtom l, IAtom r)
        {
            if (l is IComparable<IAtom> c)
                return (c.CompareTo(r) > 0).Wrap();
            if (r is IComparable<IAtom> c1)
                return (c1.CompareTo(l) < 0).Wrap();
            return 0.Wrap();
        }

        /// <summary>
        /// Check that l >= r
        /// </summary>
        /// <param name="l"></param>
        /// <param name="r"></param>
        /// <returns></returns>
        /// <remarks>Usage:<br/>
        /// <code>(>= 1 1)</code> -> t<br/>
        /// <code>(>= 2 1)</code> -> t</remarks>
        [Names("ge", ">="), Eager]
        static IAtom GrEq(IAtom l, IAtom r) => Gr(r, l).Inv();

        /// <summary>
        /// Check that l is strictly less than r
        /// </summary>
        /// <param name="l"></param>
        /// <param name="r"></param>
        /// <returns></returns>
        /// <remarks>Usage:<br/>
        /// <code>(< 1 2)</code> -> t<br/>
        /// <code>(< 2 1)</code> -> ()</remarks>
        [Names("ls", "<"), Eager]
        static IAtom Ls(IAtom l, IAtom r) => Gr(r, l);

        /// <summary>
        /// Check that l is less than or equal to r
        /// </summary>
        /// <param name="l"></param>
        /// <param name="r"></param>
        /// <returns></returns>
        /// <remarks>Usage:<br/>
        /// <code>(<= 1 2)</code> -> t<br/>
        /// <code>(<= 1 1)</code> -> t</remarks>
        [Names("le", "<="), Eager]
        static IAtom LsEq(IAtom l, IAtom r) => Gr(l, r).Inv();
    }
}