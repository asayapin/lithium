﻿using asjp.lithium.Contracts.Atoms;
using asjp.lithium.Contracts.Extensibility;
using asjp.lithium.Contracts.Services;
using asjp.lithium.ObjectModel;
using asjp.lithium.ObjectModel.Extensions;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;
// ReSharper disable UnusedMember.Local
// ReSharper disable once UnusedType.Global
namespace asjp.lithium.Defaults.StdLibParts
{
    /// <summary>
    /// CLR integration
    /// </summary>
    [ExtensionPart]
    [SuppressMessage(
        "CodeQuality",
        "IDE0051:Remove unused private members",
        Justification =
            "methods are decorated so that they are resolved in runtime; see StdLib & ExtensionPartsCollectorBase")]
    internal class DotNet : PartBase
    {
        static readonly HashSet<string> ProcessedAssemblies = new();
        static readonly Dictionary<string, Type> LoadedTypes = new();
        static readonly Dictionary<string, Type> ResolvedTypes = new();

        static Dictionary<string, Type> GetLoadedTypes
        {
            get
            {
                foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
                    if (ProcessedAssemblies.Add(assembly.FullName))
                        foreach (var type in assembly.GetTypes())
                            LoadedTypes[type.FullName ?? ""] = type;

                return LoadedTypes;
            }
        }

        static Func<Type, bool> AcceptsArgs(int args) => t =>
            t.IsGenericTypeDefinition ? t.GetGenericArguments().Length == args : args == 0;

        static Type ResolveType(string name, IScope scope, int genericArgs)
        {
            var acceptsArgs = AcceptsArgs(genericArgs);
            var possibleNames = scope.ImportedNamespaces.Select(x => $"{x}.{name}").Concat(new[] {name}).ToList();
            var resolved = ResolvedTypes.FirstOrDefault(x => possibleNames.Contains(x.Key)).Value;
            if (!(resolved is null) && acceptsArgs(resolved)) return resolved;
            var snapshot = GetLoadedTypes;
            foreach (var possible in possibleNames)
                if (snapshot.TryGetValue(possible, out var type) && acceptsArgs(type))
                {
                    ResolvedTypes[possible] = type;
                    return type;
                }

            return null;
        }

        static Type ResolveType(IAtom name, IScope scope)
        {
            if (name is ListAtom la)
            {
                var atoms = la.Atoms;
                if (atoms.Length == 0) return null;
                if (atoms[0] is IdentifierAtom id)
                {
                    var type = ResolveType(id.Identifier, scope, atoms.Length - 1);
                    if (!(type is null))
                    {
                        var args = atoms.Skip(1).Select(x => ResolveType(x, scope)).ToArray();
                        if (args.Length == 0) return type;
                        return type.MakeGenericType(args);
                    }
                }

                return ResolveType(la.Evaluate(scope), scope);
            }
            else if (name is IdentifierAtom id)
                return ResolveType(id.Identifier, scope, 0) ?? ResolveType(id.Evaluate(scope), scope);

            return null;
        }

        /// <summary>
        /// Get type object for provided name sequence
        /// </summary>
        /// <param name="name"></param>
        /// <param name="scope"></param>
        /// <returns></returns>
        /// <remarks>for generics, allows only closed generic types<br/>
        /// Usage:<br/>
        /// <code>(type "System.Int32")</code> -> type object</remarks>
        [Names("type", "get-type"), Eager]
        static IAtom Type(IAtom name, IScope scope) => ResolveType(name, scope).Wrap();

        /// <summary>
        /// Instantiate object given it's type
        /// </summary>
        /// <param name="cls"></param>
        /// <param name="scope"></param>
        /// <param name="ctor"></param>
        /// <returns></returns>
        /// <remarks>Usage:<br/>
        /// <code>(new 'System.Int32)</code> -> 0</remarks>
        [Names("new"), Eager]
        static IAtom New(IAtom cls, IScope scope, params IAtom[] ctor)
        {
            var ctorArgs = ctor.Select(x => x.GetValue(scope)).ToArray();
            if (!(Type(cls, scope).GetValue(scope) is Type type)) return ValueAtom.Nil;
            if (typeof(Delegate).IsAssignableFrom(type)
                && ctorArgs.Length == 1
                && ctorArgs[0] is Delegate d)
                return new MethodGroupAtom(new[]{d.Method}, d.Target);

            return new NetWrapperAtom(Activator.CreateInstance(type, ctorArgs));
        }

        /// <summary>
        /// Traverse object's members
        /// </summary>
        /// <param name="targetAtom"></param>
        /// <param name="scope"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        /// <remarks>
        /// allows navigation through indexers - <code>(nav my-array 3)</code> or <code>(nav my-obj ArrayProp 4)</code>
        /// <br/>
        /// Usage:<br/> <code>(. variable 'Prop 'AnotherProp)</code>
        /// </remarks>
        [Names("nav", "."), Eager]
        static IAtom Nav(IAtom targetAtom, IScope scope, params IAtom[] path)
        {
            var target = targetAtom.GetValue(scope);
            var targetType = target?.GetType();
            if (targetAtom is IdentifierAtom id)
            {
                targetType = ResolveType(id, scope);
                target = null;
            }

            MemberInfo[] member;
            object parent;

            for (var i = 0; i < path.Length; i++)
            {
                if (targetType is null) return ValueAtom.Nil;

                parent = target;
                var name = path[i];
                var lastIter = i == path.Length - 1;
                var (item, indexer) = GetNextNavigationTarget(scope, name);

                if (string.IsNullOrWhiteSpace(item))
                {
                    if (PathPointsNowhereOrToIndexer(indexer is null, indexer, targetType, lastIter, parent, out var property, 
                        out var result)) 
                        return result;
                    target = property.GetValue(target, new[] { indexer });
                    targetType = target?.GetType();
                    continue;
                }

                const BindingFlags attr = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance |
                                          BindingFlags.Static;
                member = targetType.GetMember(item, attr);
                switch (member.Length)
                {
                    // special case - wrap method group
                    case > 1 when lastIter && member.All(x => x is MethodInfo):
                        return new MethodGroupAtom(member.OfType<MethodInfo>(), target);
                    case 0:
                        if (ProcessIndexerAndCheckLastIter(ref targetType, item, lastIter, parent, ref target,
                            out var result))
                            return result;
                        continue;
                }

                if (member.Length != 1) return ValueAtom.Nil;
                if (lastIter)
                {
                    if (member[0] is MethodInfo mi) return new MethodWrapperAtom(mi, target);
                    return new NetWrapperAtom(parent, member[0]);
                }

                var @delegate = member[0].AsDelegate();
                if (@delegate is null) return ValueAtom.Nil;
                target = @delegate(target, Array.Empty<object>());
                targetType = target?.GetType();
            }

            return target.Wrap();
        }

        static bool ProcessIndexerAndCheckLastIter(ref Type targetType, string item, bool lastIter, object parent,
            ref object target, out IAtom result)
        {
            result = ValueAtom.Nil;
            var property = targetType.GetProperty("Item", new[] { item.GetType() });
            if (property is null)
                return true;

            if (lastIter)
            {
                result = new NetWrapperAtom(parent, property, item);
                return true;
            }

            target = property.GetValue(target, new object[] { item });
            targetType = target.GetType();
            return false;
        }

        static bool PathPointsNowhereOrToIndexer(bool ixIsNil, object indexer, Type targetType, bool lastIter, object parent,
            out PropertyInfo property, out IAtom result)
        {
            property = null;
            result = ValueAtom.Nil;
            if (ixIsNil)
                return true;

            var types = indexer is object[] args ? args.Select(x => x.GetType()).ToArray() : new[] { indexer.GetType() };
            property = targetType.GetProperty("Item", types);
            if (property is null) return true;

            if (!lastIter) return false;
            result = new NetWrapperAtom(parent, property,
                indexer is object[] os ? os : new[] { indexer });
            return true;
        }

        static (string, object) GetNextNavigationTarget(IScope scope, IAtom name)
        {
            string item = null;
            object indexer = null;
            
            switch (name)
            {
                case IdentifierAtom id1:
                    item = id1.Identifier;
                    break;
                case ValueAtom va:
                {
                    var value = va.GetValue(scope);
                    if (value is string s) item = s;
                    else indexer = value;
                    break;
                }
                case IEnumerableAtom ia:
                    indexer = ia.Atoms.Select(x => x.Evaluate(scope).GetValue(scope)).ToArray();
                    break;
            }

            return (item, indexer);
        }

        /// <summary>
        /// Add imports to current scope
        /// </summary>
        /// <param name="scope"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        /// <remarks>Usage: <code>(import System System.Collections)</code></remarks>
        [Names("import")]
        static IAtom Import(IScope scope, params IAtom[] args)
        {
            var names = args.OfType<IdentifierAtom>().Select(x => x.Identifier);
            return new ListAtom(names.Select(scope.AddImport).Select(x => x.Wrap()), scope.GetPosition());
        }
    }
}