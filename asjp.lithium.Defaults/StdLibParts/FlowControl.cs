﻿using System;
using asjp.lithium.Contracts.Atoms;
using asjp.lithium.Contracts.Extensibility;
using asjp.lithium.Contracts.Services;
using asjp.lithium.ObjectModel;
using asjp.lithium.ObjectModel.Extensions;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
// ReSharper disable UnusedMember.Local
// ReSharper disable once UnusedType.Global
namespace asjp.lithium.Defaults.StdLibParts
{
    /// <summary>
    /// Flow control routines
    /// </summary>
    [ExtensionPart]
    [SuppressMessage(
        "CodeQuality",
        "IDE0051:Remove unused private members",
        Justification = "methods are decorated so that they are resolved in runtime; see StdLib & ExtensionPartsCollectorBase")]
    internal class FlowControl : PartBase
    {
        /// <summary>
        /// Ternary with branches lazy evaluation
        /// </summary>
        /// <param name="cond">Condition to evaluate</param>
        /// <param name="onT">Atom to return on true</param>
        /// <param name="onNil">Atom to return on false (nil)</param>
        /// <param name="scope"></param>
        /// <returns></returns>
        /// <remarks>Usage: <code>(cond condition if-true if-false)</code></remarks>
        [Names("?", "cond")]
        static IAtom Cond([Eager] IAtom cond, IAtom onT, IAtom onNil, IScope scope)
        {
            return (cond.IsT() ? onT : onNil).Evaluate(scope);
        }
        /// <summary>
        /// Process sequence of atoms
        /// </summary>
        /// <param name="ls">names to create in inner scope</param>
        /// <param name="scope">outer scope that will be used as parent for inner one</param>
        /// <param name="atoms">atoms to evaluate</param>
        /// <returns>result of evaluation of last atom</returns>
        /// <remarks>Usage: <code>(|> () atom-1 atom-2)</code></remarks>
        [Names("proc", "|>")]
        static IAtom Proc(IAtom ls, IScope scope, params IAtom[] atoms)
        {
            var locals = ls.IsNil() ? new ListAtom(Array.Empty<IAtom>(), scope.GetPosition()) : ls as IEnumerableAtom;
            if (locals is null) return ValueAtom.Nil;
            if (locals.Atoms.Length > 0)
            {
                var s = scope.CreateChild();
                foreach (var name in locals.Atoms.Select(x => x.GetValue(scope) as string).Where(x => !string.IsNullOrWhiteSpace(x)))
                    s.SetData(name, ValueAtom.Nil);
                scope = s;
            }
            var ret = ValueAtom.Nil;
            foreach (var atom in atoms)
                ret = atom.Evaluate(scope);
            return ret;
        }
        /// <summary>
        /// Double evaluate atom
        /// </summary>
        /// <param name="a"></param>
        /// <param name="scope"></param>
        /// <returns></returns>
        /// <remarks>Usage: <code>(eval atom)</code></remarks>
        [Names("eval")]
        static IAtom Eval([Eager] IAtom a, IScope scope) => a.Evaluate(scope);
        /// <summary>
        /// Declare anonymous method
        /// </summary>
        /// <param name="vars">arguments</param>
        /// <param name="body">method body</param>
        /// <param name="scope">scope to bind evaluation to</param>
        /// <returns>invocable atom</returns>
        /// <remarks>Usage: <code>(λ () atom)</code></remarks>
        [Names("lambda", "λ", "\\")]
        static IAtom Lambda(IAtom vars, IAtom body, IScope scope)
        {
            var locals = vars.IsNil()
                ? new string[0]
                : (vars as IEnumerableAtom)?.Atoms.Select(x => x.GetValue(scope) as string).Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();
            return new InvokableAtom(locals, body);
        }
        /// <summary>
        /// Store a function in current scope
        /// </summary>
        /// <param name="name">Name to bind</param>
        /// <param name="vars">Function arguments</param>
        /// <param name="body">Function body</param>
        /// <param name="scope">Scope to store function in</param>
        /// <returns>Created function</returns>
        /// <remarks> The function doesn't bind atoms on definition; actually, it acts more like a macro
        /// <br/> Usage: <code>(def name () atom)</code></remarks>
        [Names("def", "defun")]
        static IAtom Def(IAtom name, IAtom vars, IAtom body, IScope scope)
        {
            var l = Lambda(vars, body, scope);
            scope.SetData(name.GetValue(scope) as string, l);
            return l;
        }
        
        /// <summary>
        /// run one action of a set
        /// </summary>
        /// <param name="target">target to evaluate guards with</param>
        /// <param name="scope"></param>
        /// <param name="branches">set of tuples (guard atom), where guard is unary function evaluated against target, and atom is possible result of ct:switch evaluation</param>
        /// <returns>atom for which guard evaluated to true - evaluated</returns>
        [Names("switch")]
        static IAtom Switch([Eager] IAtom target, IScope scope, params IAtom[] branches)
        {
            var args = new[] { target };
            foreach (var branch in branches.OfType<IEnumerableAtom>())
                if (branch.Head().Evaluate(scope) is IInvokableAtom ivk && ivk.Invoke(args, scope).IsT())
                {
                    IAtom atom = branch.Tail();
                    return (atom is IEnumerableAtom e ? e.Head() : atom).Evaluate(scope);
                }

            return ValueAtom.Nil;
        }

        /// <summary>
        /// function to mark default branch for ct:switch
        /// </summary>
        /// <returns>true</returns>
        [Names("otherwise")]
        static bool Otherwise() => true;
        
        /// <summary>
        /// Compose single-argument functions into pipeline
        /// </summary>
        /// <param name="fs">Functions in order of application</param>
        /// <returns>Invocable atom wrapping composition of functions</returns>
        /// <example>
        /// if, say, you want to check whether first element of sequence which elements are atoms is bigger than 5,
        /// for arbitrary sequence, you can write either
        /// <code>(&lt; 5 (car (filter atom seq)))</code>
        /// or
        /// <code>((pipe (filter atom) car (&lt; 5)) seq)</code> 
        /// </example>
        /// <remarks>Usage: <br/>
        /// <code>(pipe func-1 func-2 func-3)</code><br/>
        /// <code>(~> func-1 func-2 func-3)</code></remarks>
        [Names("pipe", "~>"), Eager]
        static IAtom Pipe(params IAtom[] fs)
        {
            return new MethodWrapperAtom(new Func<IAtom, IScope, IAtom>(new PipeWrapper(fs).Evaluate));
        }

        /// <summary>
        /// Utility class to invoke composition
        /// </summary>
        class PipeWrapper
        {
            readonly IAtom[] _functions;

            public PipeWrapper(IAtom[] functions)
            {
                _functions = functions;
            }
            /// <summary>
            /// Perform invocation for provided argument
            /// </summary>
            /// <param name="a">Argument to pass to pipeline</param>
            /// <param name="scope">Scope to invoke in</param>
            /// <returns>Application result</returns>
            public IAtom Evaluate([Eager] IAtom a, IScope scope)
            {
                var s = scope.CreateChild();
                var args = new[]{a};
                foreach (var function in _functions)
                    if (function is IInvokableAtom ia)
                        args = new[] {ia.Invoke(args, s)};
                return args[0];
            }
        }
    }
}
