﻿using System;
using asjp.lithium.Contracts.Atoms;
using asjp.lithium.Contracts.Extensibility;
using asjp.lithium.Contracts.Services;
using asjp.lithium.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using asjp.lithium.ObjectModel.Extensions;

// ReSharper disable UnusedMember.Local
// ReSharper disable once UnusedType.Global
namespace asjp.lithium.Defaults.StdLibParts
{
    /// <summary>
    /// General utilities
    /// </summary>
    [ExtensionPart]
    [SuppressMessage(
        "CodeQuality",
        "IDE0051:Remove unused private members",
        Justification = "methods are decorated so that they are resolved in runtime; see StdLib & ExtensionPartsCollectorBase")]
    internal class General : PartBase
    {
        /// <summary>
        /// Bind name to atom in current scope - effectively create named variable
        /// </summary>
        /// <param name="target">target to set - name to bind in scope or CLR wrapper</param>
        /// <param name="value">Value to bind</param>
        /// <param name="scope">Scope to bind in</param>
        /// <returns>Bound atom</returns>
        /// <remarks>Usage: <code>(set name atom)</code></remarks>
        [Names("setx", "set")]
        static IAtom Setx(IAtom target, [Eager] IAtom value, IScope scope)
        {
            if (target is ISettable s && s.TrySet(value, scope))
                return value;

            target = target.Evaluate(scope);
            if (target is ISettable s1 && s1.TrySet(value, scope))
                return value;
            
            return ValueAtom.Nil;
        }

        [Names("print")]
        static IAtom Print(string s){
            System.Console.WriteLine(s);
            return ValueAtom.T;
        }

        [Names("input")]
        static IAtom Input(string prompt){
            System.Console.Write(prompt);
            return new ValueAtom(System.Console.ReadLine(), (0, 0, 0));
        }
        
        /// <summary>
        /// evaluate and unwrap value. useful when working with .NET atoms
        /// </summary>
        /// <param name="atom"></param>
        /// <param name="scope"></param>
        /// <returns></returns>
        [Names("val")]
        static object Value(IAtom atom, IScope scope)
        {
            return atom.Evaluate(scope).GetValue(scope);
        }
        
        /// <summary>
        /// convert atoms to dictionary
        /// </summary>
        /// <param name="scope"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        [Names("util:dict")]
        internal static DictionaryAtom Dict(IScope scope, params IAtom[] args)
        {
            var dict = new DictionaryAtom(Array.Empty<(string, IAtom)>());

            foreach (var a in args)
            {
                if (a is IEnumerableAtom ea && ea.Atoms.Length == 2)
                {
                    var items = ea.Atoms.Select(x => x.Evaluate(scope)).ToArray();
                    dict[items[0].GetValue(scope).ToString()] = items[1];
                }
                else if (a.GetValue(scope) is System.Collections.Generic.KeyValuePair<string, object> kvp)
                {
                    dict[kvp.Key] = kvp.Value.Wrap();
                }
                else
                {
                    var v = a.Evaluate(scope);
                    dict[v.GetHashCode().ToString()] = v;
                }
            }

            return dict;
        }
    }
}
