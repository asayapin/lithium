﻿using asjp.lithium.Contracts.Atoms;
using asjp.lithium.Contracts.Extensibility;
using asjp.lithium.Contracts.Services;
using asjp.lithium.ObjectModel;
using asjp.lithium.ObjectModel.Extensions;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

// ReSharper disable UnusedMember.Local
// ReSharper disable once UnusedType.Global
namespace asjp.lithium.Defaults.StdLibParts
{
    /// <summary>
    /// List processing utilities
    /// </summary>
    [ExtensionPart]
    [SuppressMessage(
        "CodeQuality",
        "IDE0051:Remove unused private members",
        Justification = "methods are decorated so that they are resolved in runtime; see StdLib & ExtensionPartsCollectorBase")]
    internal class Lists : PartBase
    {
        /// <summary>
        /// Get first atom of sequence
        /// </summary>
        /// <param name="atom">Sequence to process</param>
        /// <returns>First atom or nil if atom is not enumerable</returns>
        /// <remarks>Usage:<br/>
        /// <code>(head seq)</code><br/>
        /// <code>(car seq)</code></remarks>
        [Names("car", "head", "first")]
        static IAtom Car([Eager] IAtom atom) => atom is IEnumerableAtom a ? a.Head() : ValueAtom.Nil;

        /// <summary>
        /// Return "tail" - either all atoms except first (lists) or second atom of tuple
        /// </summary>
        /// <param name="atom">Sequence to process</param>
        /// <returns>All atoms except first for list atom, second atom for tuple, nil otherwise</returns>
        /// <remarks>Usage:<br/>
        /// <code>(cdr seq)</code><br/>
        /// <code>(tail seq)</code></remarks>
        [Names("cdr", "tail", "rest")]
        static IAtom Cdr([Eager] IAtom atom)
        {
            if (atom is IEnumerableAtom a) return a.Tail();
            return ValueAtom.Nil;
        }

        /// <summary>
        /// Create list from set of arguments
        /// </summary>
        /// <param name="args">Items of sequence to create</param>
        /// <returns>New list atom with all provided arguments</returns>
        /// <remarks>Usage: <code>(list atom-1 atom-2)</code></remarks>
        [Names("list"), Eager]
        static IAtom List(params IAtom[] args) => new ListAtom(args, (0, 0, 0));
        /// <summary>
        /// Append atom to list
        /// </summary>
        /// <param name="l">Target list</param>
        /// <param name="a">Atom to append</param>
        /// <returns>List with appended atom or nil if l was not enumerable</returns>
        /// <remarks>Usage: <code>(append (list 1 2 3) 4)</code></remarks>
        [Names("append"), Eager]
        static IAtom Append(IAtom l, IAtom a)
        {
            if (l is IEnumerableAtom ia) return new ListAtom(ia.Atoms.Append(a), (0, 0, 0));
            return ValueAtom.Nil;
        }
        /// <summary>
        /// Concatenate two sequences
        /// </summary>
        /// <param name="l">First sequence</param>
        /// <param name="a">Second sequence</param>
        /// <returns>Concatenated sequence or nil if any of arguments was not enumerable</returns>
        /// <remarks>Usage: <code>(concat (list 1 2 3) (list 4 5 6))</code> -> (1 2 3 4 5 6)</remarks>
        [Names("cat", "concat"), Eager]
        static IAtom Concat(IAtom l, IAtom a)
        {
            if (l is IEnumerableAtom a1 && a is IEnumerableAtom a2)
                return new ListAtom(a1.Atoms.Concat(a2.Atoms), (0, 0, 0));
            return ValueAtom.Nil;
        }
        /// <summary>
        /// Get value at position - with wrapping around bounds
        /// </summary>
        /// <param name="l">Sequence to get from</param>
        /// <param name="index">Index of element. Use negative values to count from end (-1 == last)</param>
        /// <param name="scope">Scope to consider</param>
        /// <returns>Atom at specified position or nil if l is not enumerable or index is not integral</returns>
        /// <example>
        /// <code>
        /// (set a (list 0 1 2 3 4 5 6))
        /// (== (index a 3) 3)
        /// (== (index a 12) 5)
        /// (== (index a -3) 4)
        /// </code>
        /// </example>
        /// <remarks>Usage:<br/>
        /// <code>(@ seq position)</code><br/>
        /// <code>(ix seq pos)</code></remarks>
        [Names("@", "ix", "index"), Eager]
        static IAtom Index(IAtom l, IAtom index, IScope scope)
        {
            var ix = index.GetValue(scope);
            if (l is IEnumerableAtom a && ix is int i)
            {
                var ats = a.Atoms;
                return ats[(i % ats.Length + ats.Length) % ats.Length];
            }

            return ValueAtom.Nil;
        }
        /// <summary>
        /// Map items of sequence to new form (Linq: Select) 
        /// </summary>
        /// <param name="s">Selector</param>
        /// <param name="l">Sequence to map</param>
        /// <param name="scope">Scope to consider</param>
        /// <returns>Sequence of converted items or nil if any of arguments is not of required form</returns>
        /// <remarks>Usage: <code>(map (lambda (v) (* 2 v)) (list 1 2 3))</code> -> (2 4 6)</remarks>
        [Names("map", "->"), Eager]
        static IAtom Map(IAtom s, IAtom l, IScope scope)
        {
            if (s is IInvokableAtom ivk && l is IEnumerableAtom ie)
                return new ListAtom(ie.Atoms.Select(x => ivk.Invoke(new[] { x }, scope)), scope.GetPosition());
            return ValueAtom.Nil;
        }
        
        /// <summary>
        /// Map items of sequence to new form (Linq: Select), with index 
        /// </summary>
        /// <param name="s">Selector</param>
        /// <param name="l">Sequence to map</param>
        /// <param name="scope">Scope to consider</param>
        /// <returns>Sequence of converted items or nil if any of arguments is not of required form</returns>
        /// <remarks>Usage: <code>(map (lambda (v i) ($$ v i)) (list 1 2 3))</code> -> ("1 0" "2 1" "3 2")</remarks>
        [Names("mapi"), Eager]
        static IAtom MapI(IAtom s, IAtom l, IScope scope)
        {
            if (s is IInvokableAtom ivk && l is IEnumerableAtom ie)
                return new ListAtom(ie.Atoms.Select((x, i) => ivk.Invoke(new[] { x, i.Wrap() }, scope)), scope.GetPosition());
            return ValueAtom.Nil;
        }
        
        /// <summary>
        /// Filter out elements for which predicate does not hold (Linq: Where)
        /// </summary>
        /// <param name="s">Predicate</param>
        /// <param name="l">Sequence to work on</param>
        /// <param name="scope">Scope to consider</param>
        /// <returns>Sequence of elements for which predicate holds, or nil if any of arguments is not of required form</returns>
        /// <remarks>Usage: <code>(filter (> 4) (list 2 3 4 5 6))</code> -> (2 3)</remarks>
        [Names("filter", "?>"), Eager]
        static IAtom Filter(IAtom s, IAtom l, IScope scope)
        {
            if (s is IInvokableAtom ivk && l is IEnumerableAtom ie)
                return new ListAtom(ie.Atoms.Where(x => ivk.Invoke(new[] { x }, scope).IsT()), scope.GetPosition());
            return ValueAtom.Nil;
        }
        /// <summary>
        /// Reduce/fold sequence (Linq: Aggregate)
        /// </summary>
        /// <param name="s">Folding function of form (IAtom, IAtom, IScope?) -> IAtom</param>
        /// <param name="scope">Scope to consider</param>
        /// <param name="atoms">Either init value and list to process or just list to process</param>
        /// <returns>Folding result or nil if args of not required form</returns>
        /// <remarks>Usage:<br/>
        /// <code>(reduce + (list 1 2 3))</code> -> 6<br/>
        /// <code>(reduce * 1 (list 2 3 4))</code> -> 24</remarks>
        [Names("reduce", "<-"), Eager]
        static IAtom Reduce(IAtom s, IScope scope, params IAtom[] atoms)
        {
            if (s is IInvokableAtom ivk && atoms.Last() is IEnumerableAtom seq)
            {
                var init = atoms.Length > 1 ? atoms[0] : seq.Head();
                var a = seq.Atoms.Skip(2 - atoms.Length);
                return a.Aggregate(init, (acc, curr) => ivk.Invoke(new[] { acc, curr }, scope));
            }

            return ValueAtom.Nil;
        }
        /// <summary>
        /// Create a tuple
        /// </summary>
        /// <param name="a">Tuple head</param>
        /// <param name="b">Tuple tail</param>
        /// <returns>Created tuple</returns>
        /// <remarks>Usage:<br/>
        /// <code>(cons 1 2)</code> -> tuple (1 2)<br/>
        /// <code>(1 . 2)</code> -> tuple (1 2)</remarks>
        [Names("cons"), Eager]
        static IAtom Cons(IAtom a, IAtom b) => new TupleAtom(a, b, (0, 0, 0));
        
        /// <summary>
        /// check whether list is empty
        /// </summary>
        /// <param name="atom"></param>
        /// <returns></returns>
        [Names("empty?")]
        static bool Empty([Eager] IAtom atom)
        {
            if (atom is IEnumerableAtom ie) return ie.Atoms.Length == 0;
            return atom.Equals(ValueAtom.Nil);
        }

        /// <summary>
        /// check whether list is not empty
        /// </summary>
        /// <param name="atom"></param>
        /// <returns></returns>
        [Names("not-empty?")]
        static bool NotEmpty([Eager] IAtom atom)
        {
            if (atom is IEnumerableAtom ie) return ie.Atoms.Length != 0;
            return false;
        }

        /// <summary>
        /// check that all atoms satisfy predicate
        /// </summary>
        /// <param name="predicate">unary function - predicate</param>
        /// <param name="seq">sequence to check</param>
        /// <param name="scope"></param>
        /// <returns>true if validation passes and all atoms satisfy predicate, false otherwise</returns>
        [Names("seq:all"), Eager]
        static bool All(IAtom predicate, IAtom seq, IScope scope)
        {
            if (predicate is IInvokableAtom p && seq is IEnumerableAtom e)
                return e.Atoms.All(x => p.Invoke(new[] { x }, scope).IsT());

            return false;
        }
        /// <summary>
        /// check that at least one atom satisfy predicate
        /// </summary>
        /// <param name="predicate">unary function - predicate</param>
        /// <param name="seq">sequence to check</param>
        /// <param name="scope"></param>
        /// <returns>true if validation passes and there is an atom satisfying predicate, false otherwise</returns>
        [Names("seq:any"), Eager]
        static bool Any(IAtom predicate, IAtom seq, IScope scope)
        {
            if (predicate is IInvokableAtom p && seq is IEnumerableAtom e)
                return e.Atoms.Any(x => p.Invoke(new[] { x }, scope).IsT());

            return false;
        }

        /// <summary>
        /// Calculate length of value
        /// </summary>
        /// <param name="value">list to calculate length of</param>
        /// <returns>The length or nil if atom is not list</returns>
        [Names("length"), Eager]
        static IAtom Length(IAtom value) => 
            value is IEnumerableAtom ie ? ie.Atoms.Length.Wrap() : ValueAtom.Nil;
    }
}
