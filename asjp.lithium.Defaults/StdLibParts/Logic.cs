﻿using asjp.lithium.Contracts.Atoms;
using asjp.lithium.Contracts.Extensibility;
using asjp.lithium.ObjectModel;
using asjp.lithium.ObjectModel.Extensions;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

// ReSharper disable UnusedMember.Local
// ReSharper disable once UnusedType.Global
namespace asjp.lithium.Defaults.StdLibParts
{
    /// <summary>
    /// Logic utilities - functions returning T or NIL
    /// </summary>
    [ExtensionPart]
    [SuppressMessage(
        "CodeQuality",
        "IDE0051:Remove unused private members",
        Justification =
            "methods are decorated so that they are resolved in runtime; see StdLib & ExtensionPartsCollectorBase")]
    internal class Logic : PartBase
    {
        /// <summary>
        /// Conjunction - check that all atoms are truthy
        /// </summary>
        /// <param name="args">atoms to check</param>
        /// <returns>T if all atoms are truthy or none provided, nil otherwise</returns>
        /// <remarks>Usage:<br/>
        /// <code>(& t t)</code> -> t<br/>
        /// <code>(& ())</code> -> nil</remarks>
        [Names("&"), Eager]
        static IAtom And(params IAtom[] args) => args.All(x => x.IsT()).Wrap();

        /// <summary>
        /// Disjunction - check that any atom is truthy
        /// </summary>
        /// <param name="args">atoms to check</param>
        /// <returns>T if any of atoms is truthy or none provided, nil otherwise</returns>
        /// <remarks>Usage:<br/>
        /// <code>(| t nil)</code> -> t</remarks>
        [Names("|"), Eager]
        static IAtom Or(params IAtom[] args) => args.Any(x => x.IsT()).Wrap();

        /// <summary>
        /// Invert atom - convert truthy to nil, falsy to T
        /// </summary>
        /// <param name="a">atom to convert</param>
        /// <returns>Nil or T, depending on initial truthiness</returns>
        /// <remarks>Usage: <code>(~ t)</code> -> nil</remarks>
        [Names("~"), Eager]
        static IAtom Not(IAtom a) => a.IsT() ? ValueAtom.Nil : ValueAtom.T;

        /// <summary>
        /// Check whether atom is scalar
        /// </summary>
        /// <param name="a">Atom to check</param>
        /// <returns>T if atom represents a scalar - value, invocable, error, CLR wrapper, nil otherwise</returns>
        /// <remarks>Usage: <code>(atom a)</code> -> t | nil</remarks>
        [Names("atom"), Eager]
        static IAtom Atom(IAtom a)
        {
            switch (a)
            {
                case ValueAtom:
                case NetWrapperAtom:
                case IInvokableAtom:
                case ErrorAtom:
                    return ValueAtom.T;
                default: return ValueAtom.Nil;
            }
        }

        /// <summary>
        /// Check whether identifiers are loaded in scope
        /// </summary>
        /// <param name="args">Atoms to check</param>
        /// <returns>Sequence of t or nil per each provided atom, where T stands for atom is defined and resolvable, and nil for unresolvable ones</returns>
        /// <remarks>Usage: <code>(?? def)</code> -> (t)</remarks>
        [Names("is-def", "??", "isdef"), Eager]
        static IAtom IsDef(params IAtom[] args) => new ListAtom(args.Select(x => (x is not ErrorAtom).Wrap()), (0, 0, 0));
    }
}