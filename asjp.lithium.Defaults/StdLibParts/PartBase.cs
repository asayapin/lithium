﻿using asjp.lithium.Contracts.Atoms;
using System;
using System.Linq;

namespace asjp.lithium.Defaults.StdLibParts
{
    /// <summary>
    /// Utilities to simplify extensions creating
    /// </summary>
    public abstract class PartBase
    {
        /// <summary>
        /// Perform action on sequence of IArithmetic atoms, skipping non-compliant atoms
        /// </summary>
        /// <param name="atoms">Atoms to fold</param>
        /// <param name="aggr">Folding function</param>
        /// <returns>Folding result</returns>
        protected static IAtom InvokeOnIArithmetic(IAtom[] atoms, Func<IArithmetic, IAtom, IAtom> aggr)
        {
            if (atoms.Length == 1) return atoms[0];
            return atoms.Skip(1).Aggregate(atoms[0], (acc, curr) => acc is IArithmetic ia ? aggr(ia, curr) : acc);
        }
    }
}
