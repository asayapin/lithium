﻿using asjp.lithium.Contracts.Atoms;
using asjp.lithium.Contracts.Extensibility;
using asjp.lithium.Contracts.Services;
using asjp.lithium.ObjectModel.Extensions;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
// ReSharper disable UnusedMember.Local
// ReSharper disable once UnusedType.Global
namespace asjp.lithium.Defaults.StdLibParts
{
    /// <summary>
    /// Formatting utilities
    /// </summary>
    [ExtensionPart]
    [SuppressMessage(
        "CodeQuality",
        "IDE0051:Remove unused private members",
        Justification = "methods are decorated so that they are resolved in runtime; see StdLib & ExtensionPartsCollectorBase")]
    internal class Strings : PartBase
    {
        /// <summary>
        /// Wrapper for string.Format
        /// </summary>
        /// <param name="fmt">C# format string</param>
        /// <param name="scope">Scope to consider</param>
        /// <param name="items">Items to insert in format string</param>
        /// <returns>Formatted string</returns>
        /// <remarks>Usage: <code>($ "{0} {1} {2}" "these" "are" "arguments")</code> -> "these are arguments"</remarks>
        [Names("format", "$", "fmt"), Eager]
        static IAtom Format(IAtom fmt, IScope scope, params IAtom[] items)
        {
            var format = fmt.GetValue(scope).ToString();
            return string.Format(format, items.Select(x => x.GetValue(scope)).ToArray()).Wrap();
        }
        /// <summary>
        /// Wrapper for string.Join(" ", ...)
        /// </summary>
        /// <param name="args">Args to join</param>
        /// <returns>String of arguments separated by space</returns>
        /// <remarks>Usage: <code>($$ 1 2 3)</code> -> "1 2 3"</remarks>
        [Names("$$", "fmt-il", "inline"), Eager]
        static IAtom InlineFormat(params object[] args)
        {
            return string.Join(" ", args.Select(x => x.ToString())).Wrap();
        }
    }
}
