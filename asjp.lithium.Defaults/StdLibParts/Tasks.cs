﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using asjp.lithium.Contracts.Atoms;
using asjp.lithium.Contracts.Extensibility;
using asjp.lithium.Contracts.Services;
using asjp.lithium.ObjectModel;
using asjp.lithium.ObjectModel.Extensions;

// ReSharper disable UnusedMember.Local
// ReSharper disable once UnusedType.Global
namespace asjp.lithium.Defaults.StdLibParts
{
    /// <summary>
    /// Task utilities
    /// </summary>
    [ExtensionPart]
    [SuppressMessage(
        "CodeQuality",
        "IDE0051:Remove unused private members",
        Justification =
            "methods are decorated so that they are resolved in runtime; see StdLib & ExtensionPartsCollectorBase")]
    internal class Tasks : PartBase
    {
        /// <summary>
        /// Create task from delegate
        /// </summary>
        /// <param name="func">Invocable to wrap in task</param>
        /// <param name="scope">Scope to consider</param>
        /// <returns>Task wrapped in atom or nil if any requirement is violated</returns>
        /// <remarks>Usage: <code>(task (λ () 123))</code></remarks>
        [Names("new-task", "task")]
        static IAtom CreateTask([Eager] IAtom func, IScope scope)
        {
            if (func is IInvokableAtom ia && ia.GetValue(scope) is Delegate d)
                return new AwaitableAtom(d);
            return ValueAtom.Nil;
        }

        /// <summary>
        /// Synchronously run all provided tasks
        /// </summary>
        /// <param name="scope">Scope to consider</param>
        /// <param name="args">Tasks to wait for</param>
        /// <returns>Sequence of tasks' results</returns>
        /// <remarks>Usage: <code>(wait task-1 task-2 task-3)</code></remarks>
        [Names("wait"), Eager]
        static IAtom Wait(IScope scope, params IAtom[] args)
        {
            var tasks = args.OfType<AwaitableAtom>().Select(x => x.GetValue(scope))
                .OfType<Task>().ToArray();

            // ReSharper disable once CoVariantArrayConversion
            Task.WaitAll(tasks);

            return tasks.Length == 1 ? AwaitableAtom.Get(tasks[0]) : new ListAtom(tasks.Select(AwaitableAtom.Get),
                scope.GetPosition());
        }

        /// <summary>
        /// Bind sequence of continuations to single task
        /// </summary>
        /// <param name="task">Task to bind to</param>
        /// <param name="scope">Scope to consider</param>
        /// <param name="fs">Continuations to bind</param>
        /// <returns>Task with bindings or error is task parameter is not awaitable</returns>
        /// <remarks>All continuations are bound sequentially, creating a "chain" with single root node (task parameter)
        /// and set of leaves each corresponding to provided continuation<br/>
        /// Actually, this one is erroneous, use <code>chain</code><br/>
        /// Usage: <code>(bind root-task func-1 func-2)</code>
        /// </remarks>
        [Names("bind"), Eager]
        static IAtom Bind(IAtom task, IScope scope, params IAtom[] fs)
        {
            return task is AwaitableAtom aw
                ? (IAtom) aw.ContinueWith(new ListAtom(fs, scope.GetPosition()), scope)
                : new ErrorAtom("failed to bind continuation", task, scope);
        }

        /// <summary>
        /// Chain continuations to provided task
        /// </summary>
        /// <param name="task">First task to bind to</param>
        /// <param name="scope">Scope to consider</param>
        /// <param name="fs">Continuations - unary functions</param>
        /// <returns>Task holding continuation chain</returns>
        /// <remarks>Usage: <code>(chain root-task func-1 func-2)</code></remarks>
        [Names("chain"), Eager]
        static IAtom Chain(IAtom task, IScope scope, params IAtom[] fs)
        {
            if (!(task is AwaitableAtom aw)) return new ErrorAtom("failed to chain continuations", task, scope);

            return fs.Aggregate(aw, (t, a) => t.ContinueWith(a, scope));
        }
        
        /// <summary>
        /// create a task fulfilled when all child tasks are resolved
        /// </summary>
        /// <param name="tasks">tasks to wait on</param>
        /// <returns>task</returns>
        [Names("tasks:all"), Eager]
        static IAtom All(params IAtom[] tasks)
        {
            var task = Task.WhenAll(tasks.OfType<AwaitableAtom>().Select(x => x.Task))
                .ContinueWith(t =>
                    t.Status == TaskStatus.RanToCompletion
                        ? t.Result.Wrap()
                        : new ErrorAtom("at least one of tasks failed", ValueAtom.Nil, null, t.Exception));

            return new AwaitableAtom(task);
        }
        
        /// <summary>
        /// create a task fulfilled when any of child task is resolved
        /// </summary>
        /// <param name="tasks">tasks to wait on</param>
        /// <returns>task</returns>
        [Names("tasks:any"), Eager]
        static IAtom Any(params IAtom[] tasks)
        {
            var task = Task.WhenAny(tasks.OfType<AwaitableAtom>().Select(x => x.Task))
                .ContinueWith(t =>
                    t.Status == TaskStatus.RanToCompletion
                        ? t.Result.Status == TaskStatus.RanToCompletion
                            ? t.Result.Result
                            : new ErrorAtom("task failed", ValueAtom.Nil, null, t.Result.Exception)
                        : new ErrorAtom("all tasks failed", ValueAtom.Nil, null, t.Exception));

            return new AwaitableAtom(task);
        }
    }
}