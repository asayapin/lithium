﻿using System;
using System.Collections.Generic;
using System.Linq;
using asjp.lithium.Contracts.Atoms;
using asjp.lithium.Contracts.Extensibility;
using asjp.lithium.Contracts.Services;
using asjp.lithium.ObjectModel;
using asjp.lithium.ObjectModel.Extensions;

namespace asjp.lithium.Defaults.StdLibParts
{
    /// <summary>
    /// A little set of functions to create types in Lithium
    /// </summary>
    [ExtensionPart]
    public class TypeCreation
    {
        const string typeContextName = "__type-context";
        static RecordAtom Context(IScope scope)
        {
            if (scope.TryGetData(typeContextName, out var atom) && atom is RecordAtom ta) return ta;

            scope.SetData(typeContextName, ta = new());
            return ta;
        }

        static void ClearContext(IScope scope) => scope.SetData(typeContextName, ValueAtom.Nil);

        /// <summary>
        /// create enum type
        /// </summary>
        /// <param name="name">type name</param>
        /// <param name="scope"></param>
        /// <param name="items">type items</param>
        /// <returns>true</returns>
        [Names("c:enum")]
        static bool CreateEnum(string name, IScope scope, params object[] items) {
            EnumAtom type;
            if (!scope.TryGetData(name, out var atom)) {
                scope.SetData(name, type = new());
            }
            else type = atom as EnumAtom;

            var max = type.Values.Append(-1).Max();
            var names = items.Select(x => x?.ToString()).Where(x => !(x is null)).Except(type.Names).ToArray();

            for (int i = 0; i < names.Length; i++)
            {
                string n = names[i];
                var value = i + max + 1;
                type.Add(n, value);
                scope.SetData($"{name}:{n}", new ValueAtom(n, scope.GetPosition()));
                scope.SetData($"{name}:{n}:val", new ValueAtom(value, scope.GetPosition()));
            }

            return true;
        }

        /// <summary>
        /// create a record type
        /// </summary>
        /// <param name="name">type name</param>
        /// <param name="scope"></param>
        /// <param name="children">type items</param>
        /// <returns>true</returns>
        [Names("c:type")]
        static bool CreateRecordType(string name, IScope scope, params IAtom[] children)
        {
            var type = Context(scope);
            scope.SetData(name, type);
            type.Prefix = name;

            foreach (var atom in children) 
                atom.Evaluate(scope);

            ClearContext(scope);

            return true;
        }

        /// <summary>
        /// create a property for a record type
        /// </summary>
        /// <param name="name">property name</param>
        /// <param name="typeAlias">property type</param>
        /// <param name="scope"></param>
        /// <remarks>allows to specify array types with [] after element type; creates a set of accessors in current scope</remarks>
        [Names("c:prop")]
        static void Property(string name, string typeAlias, IScope scope)
        {
            var record = Context(scope);
            record.AddProperty(name, typeAlias);

            var propPrefix = $"{record.Prefix}:{name}";

            if (typeAlias.EndsWith("[]"))
                scope.SetData(
                    $"{propPrefix}:add",
                    new MethodWrapperAtom(new Func<IAtom, IAtom, IScope, IAtom>(
                        (target, item, s) =>
                        {
                            var dict = GetArgumentValue(target, s);
                            if (dict is null) return ValueAtom.Nil;
                            return dict[name] = (dict[name] as ListAtom).Add(item.Evaluate(s));
                        })));

            scope.SetData($"{propPrefix}:set",
                new MethodWrapperAtom(new Func<IAtom, IAtom, IScope, IAtom>(
                    (target, item, s) =>
                    {
                        var val = item.Evaluate(s);
                        var arg = GetArgumentValue(target, s);
                        return arg is null ? ValueAtom.Nil : (arg[name] = val);
                    })));
            scope.SetData($"{propPrefix}:get",
                new MethodWrapperAtom(new Func<IAtom, IScope, IAtom>(
                    (target, s) =>
                    {
                        var arg = GetArgumentValue(target, s, @readonly: true);
                        return arg is null ? ValueAtom.Nil : arg[name];
                    })));
        }

        private static DictionaryAtom GetArgumentValue(IAtom target, IScope scope, bool @readonly = false, HashSet<string> visitedIds = null)
        {
            switch (target.Evaluate(scope))
            {
                case DictionaryAtom d: return d;
                case IdentifierAtom i:
                    if (visitedIds is null) visitedIds = new();
                    if (!visitedIds.Add(i.Identifier)) return null;
                    return GetArgumentValue(i, scope, @readonly, visitedIds);
                case IEnumerableAtom e when @readonly:
                    return General.Dict(scope, e.Atoms);
                default: return null;
            }
        }

        /// <summary>
        /// set prefix for type accessors; should be the first command in type creation (if needed at all)
        /// </summary>
        /// <param name="p">prefix</param>
        /// <param name="scope"></param>
        /// <returns></returns>
        /// <remarks>used to shorten accessor names - default prefix is type name</remarks>
        [Names("c:prefix")]
        static string Prefix(string p, IScope scope) => Context(scope).Prefix = p;

        /// <summary>
        /// create an instance of record type
        /// </summary>
        /// <param name="name">type name</param>
        /// <param name="scope"></param>
        /// <param name="args">constructor arguments</param>
        /// <returns></returns>
        [Names("c:new")]
        static IAtom New(string name, IScope scope, params IAtom[] args)
        {
            if (scope.TryGetData(name, out var atom) && atom is TypeAtom ta)
                return ta.CreateInstance(scope, args);

            return ValueAtom.Nil;
        }
    }
}