﻿using System;
using System.Threading.Tasks;
using asjp.lithium.Contracts.Atoms;
using asjp.lithium.Contracts.Services;

namespace asjp.lithium.ObjectModel;

public abstract class AtomBase : IAtom
{
    protected AtomBase() : this(0, 0, 0) { }
    protected AtomBase(int line, int column, int offset) : this((line, column, offset)) { }
    protected AtomBase((int line, int column, int offset) pos)
    {
        Position = pos;
    }
    public abstract bool Equals(IAtom other);

    public IAtom Evaluate(IScope scope)
    {
        EvaluationStarted?.Invoke(this)?.GetAwaiter().GetResult();
        return PerformEvaluation(scope);
    }
    protected abstract IAtom PerformEvaluation(IScope scope); 
    public abstract object GetValue(IScope scope);
    public (int line, int col, int offset) Position { get; }
    public event Func<IAtom, Task>? EvaluationStarted;
}