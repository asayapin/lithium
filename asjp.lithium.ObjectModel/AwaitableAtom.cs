﻿using System;
using System.Threading.Tasks;
using asjp.lithium.Contracts.Atoms;
using asjp.lithium.Contracts.Services;
using asjp.lithium.ObjectModel.Extensions;

namespace asjp.lithium.ObjectModel
{
    /// <summary>
    /// Task wrapper atom
    /// </summary>
    public class AwaitableAtom : AtomBase
    {
        /// <summary>
        /// underlying task
        /// </summary>
        public Task<IAtom> Task { get; private set; }

        /// <summary>
        /// Creates new atom from delegate to run in separate thread
        /// </summary>
        /// <param name="func">Delegate to run</param>
        /// <exception cref="NotSupportedException">if delegate is not <see cref="Func{IAtom}"/></exception>
        public AwaitableAtom(Delegate func)
        {
            if (func is Func<IAtom> f)
                Task = System.Threading.Tasks.Task.Run(f);
            else if (func.Method.GetParameters().Length == 0)
                Task = System.Threading.Tasks.Task.Run((Func<object>)Delegate.CreateDelegate(typeof(Func<object>), func.Target, func.Method))
                .ContinueWith(t => t.Result.Wrap());
            else throw new NotSupportedException($"failed to create task from {func}");
        }

        /// <summary>
        /// Create task atom from existing task
        /// </summary>
        /// <param name="task"></param>
        public AwaitableAtom(Task task)
        {
            if (task is Task<IAtom> t)
                Task = t;
            else if (task is Task<object> to)
                Task = to.ContinueWith(x => x.Result.Wrap());
            else Task = task.ContinueWith(_ => ValueAtom.T);
        }

        /// <summary>
        /// always returns false
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public override bool Equals(IAtom other) => false;

        /// <summary>
        /// always returns this
        /// </summary>
        /// <param name="scope"></param>
        /// <returns></returns>
        protected override IAtom PerformEvaluation(IScope scope) => this;

        /// <summary>
        /// always returns underlying task
        /// </summary>
        /// <param name="scope">not used</param>
        /// <returns></returns>
        public override object GetValue(IScope scope) => Task;

        /// <summary>
        /// bind continuation to underlying task
        /// </summary>
        /// <param name="a">continuation to bind</param>
        /// <param name="scope">scope to consider</param>
        /// <returns>this</returns>
        public AwaitableAtom ContinueWith(IAtom a, IScope scope)
        {
            switch (a)
            {
                case IInvokableAtom ia when ia.GetValue(scope) is Delegate d && d is Func<object, IAtom> f:
                    Task = Task.ContinueWith(t => f(Get(t)));
                    break;
                case IEnumerableAtom ea:
                    foreach (var atom in ea.Atoms) ContinueWith(atom, scope);
                    return this;
                default:
                    Task = Task.ContinueWith(t =>
                    {
                        var closure = scope.CreateChild();
                        closure.SetData("task-result", Get(t));
                        return a.Evaluate(closure);
                    });
                    break;
            }

            return this;
        }

        /// <summary>
        /// Get value from provided task
        /// </summary>
        /// <param name="task"></param>
        /// <returns>Wrapped value of error atom</returns>
        public static IAtom Get(Task<IAtom> task) => task.Status == TaskStatus.RanToCompletion
            ? task.Result
            : new ErrorAtom(task.Exception?.Message, ValueAtom.Nil, null, task.Exception);

        public static IAtom Get(Task task) {
            return task switch {
                Task<IAtom> ta => Get(ta),
                Task<object> to => Get(to.ContinueWith(x => x.Result.Wrap())),
                _ => ValueAtom.Nil
            };
        }
    }
}