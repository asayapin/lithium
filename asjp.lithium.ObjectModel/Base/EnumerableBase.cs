﻿using asjp.lithium.Contracts.Atoms;
using asjp.lithium.Contracts.Services;
using System.Linq;

namespace asjp.lithium.ObjectModel.Base
{
    /// <summary>
    /// Base class for enumerable atoms
    /// </summary>
    public abstract class EnumerableBase : AtomBase, IEnumerableAtom
    {
        protected EnumerableBase((int l, int c, int o) pos) : base(pos) { }
        /// <inheritdoc cref="IEnumerableAtom.Atoms"/>
        public abstract IAtom[] Atoms { get; }
        /// <inheritdoc cref="IEnumerableAtom.Head"/>
        public abstract IAtom Head();
        /// <inheritdoc cref="IEnumerableAtom.Tail"/>
        public abstract IAtom Tail();
        /// <inheritdoc cref="IAtom.Evaluate"/>
        protected override IAtom PerformEvaluation(IScope scope)
        {
            var atoms = Atoms;
            if (scope is null) return this;
            if (atoms.Length == 0) return ValueAtom.Nil;

            scope.Trace.Push(new StackFrame(this));
            var func = atoms[0].Evaluate(scope);
            var result = func is IInvokableAtom ia
                ? ia.Invoke(atoms.Skip(1), scope)
                : new ErrorAtom($"[lithium] failed evaluate term <{func}>", this, scope);
            scope.Trace.Pop();
            return result;
        }
    }
}
