﻿using System;
using System.Collections.Generic;
using System.Linq;
using asjp.lithium.Contracts.Atoms;
using asjp.lithium.Contracts.Services;
using asjp.lithium.ObjectModel.Extensions;

namespace asjp.lithium.ObjectModel.Base
{
    /// <summary>
    /// Base for invocable atoms
    /// </summary>
    public abstract class InvokableBase : AtomBase, IInvokableAtom
    {
        readonly bool _bindsScope;
        protected readonly string[] Args;

        /// <summary>
        /// Create instance writing required arguments and info on scope binding
        /// </summary>
        /// <param name="args">List of names of required arguments. Add "..." in front of last arg to allow variadics</param>
        /// <param name="bindsScope">True is IScope should be added as last positional argument on binding</param>
        protected InvokableBase(IEnumerable<string> args, (int l, int c, int o) pos, bool bindsScope = false) : base(pos)
        {
            _bindsScope = bindsScope;
            Args = args.ToArray();
        }

        /// <summary>
        /// Number of required arguments, not taking in account scope and variadics
        /// </summary>
        int ArgsCount => Args.Length;

        /// <summary>
        /// Referential equality
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public override bool Equals(IAtom other) => ReferenceEquals(this, other);

        /// <summary>
        /// Invoke if no args required, otherwise returns this 
        /// </summary>
        /// <param name="scope"></param>
        /// <returns></returns>
        protected override IAtom PerformEvaluation(IScope scope) =>
            ArgsCount == 0 ? Invoke(Array.Empty<IAtom>(), scope) : this;

        /// <summary>
        /// Bind args and either curry or invoke underlying method
        /// </summary>
        /// <param name="args">Args to bind</param>
        /// <param name="scope">Scope to consider</param>
        /// <returns>Either curried invocable or invocation result</returns>
        public IAtom Invoke(IEnumerable<IAtom> args, IScope scope)
        {
            var (argsArr, variadic) = BindArgs(args, scope);

            var containsVariadic = Args.LastOrDefault()?.StartsWith("...") == true;
            var totalBoundCount = argsArr.Length +
                                  (containsVariadic && argsArr.Length == ArgsCount - 1
                                      ? 1
                                      : 0);

            if (ArgsCount > 0 && totalBoundCount == 0) return this;
            
            scope.Trace.Push(new(this));

            var retVal = totalBoundCount >= ArgsCount
                ? Call(argsArr.ToArray(), variadic, scope)
                : Curry(Args.Skip(argsArr.Length), argsArr, scope);

            scope.Trace.Pop();
            return retVal;
        }

        /// <summary>
        /// Curry method - convert to new invocable of diminished arity, routing to original method with some args already bound
        /// </summary>
        /// <param name="leftArgs">Args left to bind</param>
        /// <param name="boundArgs">Args already bound</param>
        /// <param name="parentScope">Scope to add as parent on evaluation</param>
        /// <returns>Curried invocable</returns>
        protected abstract IAtom Curry(IEnumerable<string> leftArgs, IEnumerable<IAtom> boundArgs, IScope parentScope);
        /// <summary>
        /// Perform invocation of underlying method
        /// </summary>
        /// <param name="args">Args to pass to method</param>
        /// <param name="v">Variadic arg</param>
        /// <param name="scope">Scope to invoke in</param>
        /// <returns>Wrapped invocation result</returns>
        protected abstract IAtom Call(IAtom[] args, IAtom[] v, IScope scope);

        /// <summary>
        /// Bind raw sequence of args to set of positional args and variadic
        /// </summary>
        /// <param name="args">Args to split to positional and variadic</param>
        /// <param name="scope">Scope to add if <see cref="_bindsScope"/> is set</param>
        /// <returns>Pair of positional and variadic args</returns>
        (IAtom[], IAtom[]) BindArgs(IEnumerable<IAtom> args, IScope scope)
        {
            var tmp = args.ToArray();
            var isVariadic = (tmp.Length + (_bindsScope ? 1 : 0)) >= ArgsCount - 1 && (Args.LastOrDefault()?.StartsWith("...") == true);
            var argsCount = ArgsCount - (isVariadic ? 1 : 0);
            var prep = tmp.Take(argsCount).ToList();
            if (_bindsScope && prep.Count == argsCount) prep.Add(new QuotedAtom(new NetWrapperAtom(scope), scope.GetPosition()));
            
            return (prep.ToArray(),
                    isVariadic ? tmp.Skip(ArgsCount - 1).ToArray() : Array.Empty<IAtom>());
        }
    }
}