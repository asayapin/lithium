﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using asjp.lithium.Contracts.Atoms;
using asjp.lithium.Contracts.Services;
using asjp.lithium.ObjectModel.Extensions;

namespace asjp.lithium.ObjectModel
{
    public class DictionaryAtom :
        AtomBase,
        IDictionary<string, IAtom>,
        IEnumerableAtom
    {
        public DictionaryAtom(IEnumerable<(string, IAtom)>? values = null):this()
        {
            foreach (var (k, v) in values ?? Array.Empty<(string, IAtom)>())
                Add(k, v);
        }
        public DictionaryAtom(IEnumerable<KeyValuePair<string, IAtom>>? values = null):this()
        {
            foreach (var v in values ?? Array.Empty<KeyValuePair<string, IAtom>>())
                Add(v);
        }
        public DictionaryAtom()
        {
            storage = new();
        }

        readonly Dictionary<string, IAtom> storage;

        public IAtom this[string key]
        {
            get => storage.TryGetValue(key, out var v) ? v : ValueAtom.Nil;
            set => storage[key] = value;
        }

        public ICollection<string> Keys => storage.Keys;
        public ICollection<IAtom> Values => storage.Values;
        public int Count => storage.Count;
        public bool IsReadOnly => false;

        public IAtom[] Atoms => storage.Select(x =>
                new TupleAtom(AtomExtensions.Wrap((object)x.Key), x.Value, Position))
            .ToArray();

        public void Add(string key, IAtom value) => storage[key] = value;

        public void Add(KeyValuePair<string, IAtom> item) => Add(item.Key, item.Value);

        public void Clear() => storage.Clear();

        public bool Contains(KeyValuePair<string, IAtom> item) => ContainsKey(item.Key);

        public bool ContainsKey(string key) => storage.ContainsKey(key);

        public void CopyTo(KeyValuePair<string, IAtom>[] array, int arrayIndex) => 
            ((IDictionary<string, IAtom>)storage).CopyTo(array, arrayIndex);

        public override bool Equals(IAtom other) =>
            other is DictionaryAtom da
            && da.storage.SequenceEqual(storage);

        protected override IAtom PerformEvaluation(IScope scope) => this;

        public IEnumerator<KeyValuePair<string, IAtom>> GetEnumerator() => storage.GetEnumerator();

        public override object GetValue(IScope scope) => 
            storage.ToDictionary(
                kvp => kvp.Key,
                kvp => kvp.Value.GetValue(scope)
            );

        public IAtom Head()
        {
            if (storage.Count == 0) return ValueAtom.Nil;

            var item = storage.First();

            return new TupleAtom(item.Key.Wrap(), item.Value, Position);
        }

        public bool Remove(string key) => storage.Remove(key);

        public bool Remove(KeyValuePair<string, IAtom> item) => storage.Remove(item.Key);

        public IAtom Tail() => new DictionaryAtom(storage.Skip(1));

        public bool TryGetValue(string key, out IAtom value) => storage.TryGetValue(key, out value);

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}