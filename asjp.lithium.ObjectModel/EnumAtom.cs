﻿using System.Collections.Generic;
using System.Linq;
using asjp.lithium.Contracts.Atoms;
using asjp.lithium.Contracts.Services;
using asjp.lithium.ObjectModel.Extensions;

namespace asjp.lithium.ObjectModel
{
    public class EnumAtom : AtomBase, TypeAtom
    {
        readonly Dictionary<string, int> values = new();

        public IEnumerable<string> Names => values.Keys;
        public IEnumerable<int> Values => values.Values;

        public void Add(string name, int value) => values[name] = value;

        public IAtom this[int val]
        {
            get => values.FirstOrDefault(kvp => kvp.Value == val).Key?.Wrap() ?? ValueAtom.Nil;
            set => values[value.GetValue(null).ToString()] = val;
        }

        public IAtom CreateInstance(IScope _, IAtom[] __) =>
            values.Aggregate((acc, next) => acc.Value < next.Value ? acc : next).Key.Wrap();

        public override bool Equals(IAtom other) => ReferenceEquals(this, other);

        protected override IAtom PerformEvaluation(IScope scope) => this;

        public override object GetValue(IScope scope) => null;
    }
}