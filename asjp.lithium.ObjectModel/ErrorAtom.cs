﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using asjp.lithium.Contracts.Atoms;
using asjp.lithium.Contracts.Services;

namespace asjp.lithium.ObjectModel
{
    /// <summary>
    /// Exception wrapper usable in engine
    /// </summary>
    public class ErrorAtom : Exception, IAtom
    {
        private readonly IAtom _source;
        public Stack<StackFrame> Frames { get; }

        public ErrorAtom(string message, IAtom source, IScope? scope, Exception? ex = null) : base(message, ex)
        {
            _source = source;
            Frames = new(scope?.Trace?.AsEnumerable() ?? Array.Empty<StackFrame>());
        }

        /// <summary>
        /// always returns this
        /// </summary>
        /// <param name="scope"></param>
        /// <returns></returns>
        public IAtom Evaluate(IScope scope)
        {
            EvaluationStarted?.Invoke(this)?.GetAwaiter().GetResult();
            return this;
        }

        /// <summary>
        /// always returns this
        /// </summary>
        /// <param name="scope"></param>
        /// <returns></returns>
        public object GetValue(IScope scope) => this;

        public (int line, int col, int offset) Position => _source.Position;
        public event Func<IAtom, Task>? EvaluationStarted;

        /// <summary>
        /// referential equality
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(IAtom other) => ReferenceEquals(this, other);
        public override string ToString()
        {
            return $"[lithium] {Message}\n{string.Join("\n", Frames.Select(f => $"at {f.Target}:{f.Target.Position.line}:{f.Target.Position.col}"))}";
        }
    }
}