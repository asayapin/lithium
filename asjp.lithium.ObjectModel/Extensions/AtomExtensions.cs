﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using asjp.lithium.Contracts.Atoms;

namespace asjp.lithium.ObjectModel.Extensions
{
    /// <summary>
    /// Atom helpers
    /// </summary>
    public static class AtomExtensions
    {
        /// <summary>
        /// Check whether atom is falsy - equals to (), empty list, or error atom
        /// </summary>
        /// <param name="atom">atom to check</param>
        /// <returns>true if atom is falsy, false otherwise</returns>
        public static bool IsNil(this IAtom atom) =>
            ReferenceEquals(atom, ValueAtom.Nil) 
            || atom is ListAtom { Count: 0 } 
            || atom is ValueAtom v && v.GetValue(null) is null
            || atom is ErrorAtom;

        /// <summary>
        /// Inverse of <see cref="IsNil"/>
        /// </summary>
        /// <param name="atom"></param>
        /// <returns></returns>
        public static bool IsT(this IAtom atom) => !IsNil(atom);

        /// <summary>
        /// Convert CLR object to atom - considering tasks, enumerables and tuples, method groups and delegates, and scalars
        /// </summary>
        /// <param name="o">object to convert</param>
        /// <returns>Wrapper atom</returns>
        public static IAtom Wrap(this object o)
        {
            if (o is null) return ValueAtom.Nil;
            var t = o.GetType();
            if (t.IsGenericType && t.GetGenericTypeDefinition().Name.Contains("Tuple`"))
            {
                var values = t.GetFields().Where(x => x.Name.StartsWith("Item")).Select(x => x.GetValue(o)).ToArray();
                if (values.Length <= 2)
                    return new TupleAtom(values.ElementAtOrDefault(0).Wrap(), values.ElementAtOrDefault(1).Wrap(),
                        (0, 0, 0));
                return new ListAtom(values.Select(Wrap), (0, 0, 0));
            }

            switch (o)
            {
                case IAtom ia:
                    return ia;
                case string s:
                    return new ValueAtom(s, (0, 0, 0));
                case IEnumerable ie:
                    return Wrap(ie.OfType<object>());
                case bool b:
                    return b ? ValueAtom.T : ValueAtom.Nil;
                case Delegate d:
                    return new MethodWrapperAtom(d.Method, d.Target);
                case MethodInfo mi:
                    return new MethodWrapperAtom(mi);
                case ValueType vt:
                    return new ValueAtom(vt, (0, 0, 0));
                case Task<IAtom> ts:
                    return new AwaitableAtom(ts);
                default:
                    return new NetWrapperAtom(o);
            }
        }

        /// <summary>
        /// Wrap sequence of objects
        /// </summary>
        /// <param name="ie"></param>
        /// <returns></returns>
        public static IAtom Wrap(this IEnumerable<object> ie) => new ListAtom(ie.Select(Wrap), (0, 0, 0));

        /// <summary>
        /// Invert atom - return nil if atom is truthy, and t otherwise
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public static IAtom Inv(this IAtom a)
        {
            if (IsNil(a))
                return ValueAtom.T;
            return IsT(a) ? ValueAtom.Nil : a;
        }

        /// <summary>
        /// Method to allow <code>await</code> syntactic sugar over atoms
        /// </summary>
        /// <param name="atom">atom to await</param>
        /// <returns>Awaiter of either underlying task or <code>Task.FromResult</code></returns>
        public static TaskAwaiter<IAtom> GetAwaiter(this IAtom atom) =>
            (atom is AwaitableAtom a ? a.Task : Task.FromResult(atom)).GetAwaiter();
    }
}