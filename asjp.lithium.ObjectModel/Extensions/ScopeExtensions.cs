﻿using System.Linq;
using asjp.lithium.Contracts.Services;

namespace asjp.lithium.ObjectModel.Extensions;

public static class ScopeExtensions
{
    public static (int line, int col, int offset) GetPosition(this IScope scope)
    {
        if (scope?.Trace?.Any() == true)
            return scope.Trace.Peek().Target.Position;
        return (0, 0, 0);
    }
}