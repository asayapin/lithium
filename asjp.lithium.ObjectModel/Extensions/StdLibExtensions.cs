﻿using System;
using System.Reflection;

namespace asjp.lithium.ObjectModel.Extensions
{
    /// <summary>
    /// Internal extensions for standard library
    /// </summary>
    public static class StdLibExtensions
    {
        /// <summary>
        /// Convert member to delegate
        /// </summary>
        /// <param name="mi">Member to convert</param>
        /// <returns>Invoke wrapper for method, getter for prop/field, null otherwise</returns>
        public static Func<object, object[], object> AsDelegate(this MemberInfo mi)
        {
            switch (mi)
            {
                case MethodInfo m: return (target, args) => m.Invoke(target, args);
                case FieldInfo f: return (t, _) => f.GetValue(t);
                case PropertyInfo p: return p.GetValue;
                default: return null;
            }
        }

        public static Action<object, object[], object> AsSetter(this MemberInfo mi)
        {
            switch (mi)
            {
                case FieldInfo f: return (t, _, v) => f.SetValue(t, v);
                case PropertyInfo p: return (t, ix, v) => p.SetValue(t, v, ix);
                default: return null;
            }
        }
    }
}
