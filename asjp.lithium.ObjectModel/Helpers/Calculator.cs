﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace asjp.lithium.ObjectModel.Helpers
{
    /// <summary>
    /// Utility for arithmetic operations over atoms
    /// </summary>
    internal static class Calculator
    {
        /// <summary>
        /// types allowed for calculations
        /// </summary>
        static readonly List<Type> NumericTypes = new List<Type> {
            typeof(sbyte),
            typeof(byte),
            typeof(short),
            typeof(ushort),
            typeof(int),
            typeof(uint),
            typeof(long),
            typeof(ulong),
            typeof(float),
            typeof(double),
            typeof(decimal)
        };

        /// <summary>
        /// allowed upcasts
        /// </summary>
        static readonly Dictionary<Type, Type> Upcasts = new Dictionary<Type, Type>
        {
            [typeof(byte)] = typeof(short),
            [typeof(sbyte)] = typeof(short),
            [typeof(short)] = typeof(int),
            [typeof(ushort)] = typeof(int),
            [typeof(int)] = typeof(long),
            [typeof(uint)] = typeof(long),
            [typeof(long)] = typeof(ulong),
            [typeof(ulong)] = typeof(double),
            [typeof(float)] = typeof(double),
            [typeof(double)] = typeof(decimal)
        };

        /// <summary>
        /// allowed binary functions
        /// </summary>
        static readonly (string, Func<Expression, Expression, Expression>)[] Binaries = {
            ("add", Expression.Add),
            ("sub", Expression.Subtract),
            ("mul", Expression.Multiply),
            ("div", Expression.Divide),
            ("mod", Expression.Modulo),
            // ReSharper disable once AssignNullToNotNullAttribute
            ("pow", (l, r) => Expression.Call(typeof(Math).GetMethod("Pow"), Expression.Convert(l, typeof(double)), Expression.Convert(r, typeof(double)))),
        };

        /// <summary>
        /// allowed unary functions
        /// </summary>
        static readonly (string, Func<Expression, Expression>)[] Unaries = {
            //("neg", Expression.Negate),
            ("inc", Expression.Increment),
            ("dec", Expression.Decrement)
        };

        /// <summary>
        /// cache for delegates to invoke
        /// </summary>
        static readonly Dictionary<(Type, Type, string), Delegate> Delegates = new Dictionary<(Type, Type, string), Delegate>();

        /// <summary>
        /// initialize cache
        /// </summary>
        static Calculator()
        {
            var ops = from l in NumericTypes
                      from r in NumericTypes
                      from b in Binaries
                      select (l, r, b.Item1, CreateDelegate(b.Item2, l, r));

            foreach (var (tl, tr, name, dlg) in ops)
                Delegates[(tl, tr, name)] = dlg;

            foreach (var t in NumericTypes)
            {
                var (ts, st) = CreateAddToString(t);
                Delegates[(t, typeof(string), "add")] = ts;
                Delegates[(typeof(string), t, "add")] = st;
            }

            foreach (var (t, name, op) in from t in NumericTypes from o in Unaries select (t, o.Item1, o.Item2))
                Delegates[(t, typeof(void), name)] = CreateDelegate(op, t);

            foreach (var type in NumericTypes)
            {
                Delegates[(type, typeof(void), "neg")] = CreateNegation(type);
            }
        }

        /// <summary>
        /// create delegate negating argument
        /// </summary>
        /// <param name="t">target type</param>
        /// <returns>delegate wrapping unary operator-</returns>
        static Delegate CreateNegation(Type t)
        {
            var param = Expression.Parameter(t);
            if (t == typeof(ushort) || t == typeof(uint) || t == typeof(ulong) || t == typeof(sbyte) || t == typeof(byte))
                t = Upcasts[t];
            var cvt = Expression.Convert(param, t);
            var body = Expression.Negate(cvt);
            return Expression.Lambda(body, param).Compile();
        }

        /// <summary>
        /// create delegate wrapping arithmetic operation for provided type
        /// </summary>
        /// <param name="op">operation to process</param>
        /// <param name="t">target type</param>
        /// <returns></returns>
        static Delegate CreateDelegate(Func<Expression, Expression> op, Type t)
        {
            var lct = GetLeastCommonType(t, t);
            var paraml = Expression.Parameter(t, "l");
            var cvt = Expression.Convert(paraml, lct);
            var body = op(cvt);
            var func = Expression.Lambda(body, paraml);
            return func.Compile();
        }

        /// <summary>
        /// create pair of delegates for concatenation with string
        /// </summary>
        /// <param name="t">target type</param>
        /// <returns>(t, string) -> string and (string, t) -> string delegates</returns>
        static (Delegate, Delegate) CreateAddToString(Type t)
        {
            var p1 = Expression.Parameter(t);
            var p2 = Expression.Parameter(typeof(string));

            var cvt = Expression.Call(p1, "ToString", new Type[0]);
            var body1 = Expression.Add(cvt, p2, typeof(string).GetMethod("Concat", new[] { typeof(string), typeof(string) }));
            var body2 = Expression.Add(p2, cvt, typeof(string).GetMethod("Concat", new[] { typeof(string), typeof(string) }));

            return (
                Expression.Lambda(body1, p1, p2).Compile(),
                Expression.Lambda(body2, p2, p1).Compile()
                );
        }

        /// <summary>
        /// create delegate for binary function
        /// </summary>
        /// <param name="binary">target operation</param>
        /// <param name="left">type of left argument</param>
        /// <param name="right">type of right argument</param>
        /// <returns>delegate performing selected operation</returns>
        static Delegate CreateDelegate(Func<Expression, Expression, Expression> binary, Type left, Type right)
        {
            var lct = GetLeastCommonType(left, right);
            var paraml = Expression.Parameter(left, "l");
            var paramr = Expression.Parameter(right, "r");
            var cvtl = Expression.Convert(paraml, lct);
            var cvtr = Expression.Convert(paramr, lct);
            var body = binary(cvtl, cvtr);
            var func = Expression.Lambda(body, paraml, paramr);
            return func.Compile();
        }

        /// <summary>
        /// get type that can be target of conversion for both provided types
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        static Type GetLeastCommonType(Type a, Type b)
        {
            if (a == b)
            {
                if (a == typeof(sbyte) || a == typeof(byte))
                    return GetLeastCommonType(Upcasts[a], Upcasts[b]);
                return a;
            }

            var ixa = NumericTypes.IndexOf(a);
            var ixb = NumericTypes.IndexOf(b);
            if (ixb < ixa) return GetLeastCommonType(b, a);
            return GetLeastCommonType(Upcasts[a], b);
        }

        /// <summary>
        /// safely invoke delegate
        /// </summary>
        /// <param name="result">result of invocation</param>
        /// <param name="method">method to invoke (fetched from cache depending on types and name)</param>
        /// <param name="l"></param>
        /// <param name="r"></param>
        /// <returns>true on success, false otherwise</returns>
        internal static bool TryInvoke(out object? result, string method, object l, object? r = null)
        {
            var t1 = l.GetType();
            var t2 = r?.GetType() ?? typeof(void);

            if (Delegates.TryGetValue((t1, t2, method), out var d))
            {
                result = r is null ? d.DynamicInvoke(l) : d.DynamicInvoke(l, r);
                return true;
            }

            result = null;
            return false;

        }
    }
}
