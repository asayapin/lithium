﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using asjp.lithium.Contracts.Atoms;
using asjp.lithium.Contracts.Services;
using asjp.lithium.ObjectModel.Extensions;

namespace asjp.lithium.ObjectModel
{
    /// <summary>
    /// Atom holding identifier of other atom
    /// </summary>
    public class IdentifierAtom : AtomBase, IComparable<IAtom>, ISettable
    {
        public IdentifierAtom(string identifier, (int l, int c, int o) pos) : base(pos)
        {
            Identifier = identifier;
        }

        static readonly Regex Cadr = new Regex("^c[ad]+r$");

        /// <summary>
        /// Wrapped identifier
        /// </summary>
        public string Identifier { get; }

        /// <summary>
        /// When scope provided, resolve symbol and return 
        /// </summary>
        /// <param name="scope">Scope to consider</param>
        /// <returns>Invocable if identifier points to method or c[ad]+r invocation; data atom if found in scope data; this is scope is null; error otherwise</returns>
        protected override IAtom PerformEvaluation(IScope scope)
        {
            if (scope is null) return this;
            if (Cadr.IsMatch(Identifier))
            {
                var seq = Identifier.ToLower().Trim('c', 'r').Reverse().ToList();
                return new MethodWrapperAtom(
                    new Func<IAtom, IScope, IAtom>((a, sc) =>
                        {
                            sc.AddParent(scope);
                            var first = new MethodWrapperAtom(SelectCadrMethod(seq[0], sc));
                            var init = first.Invoke(new[] { a }, sc);
                            return seq.Skip(1).Aggregate(init,
                                (acc, curr) =>
                                    new MethodWrapperAtom(SelectCadrMethod(curr, sc))
                                        .Invoke(new IAtom[] { new QuotedAtom(acc, scope.GetPosition()) }, sc));
                        }
                    ));
            }

            if (scope.Resolve(Identifier) is { } m) return new MethodWrapperAtom(m);
            return scope.TryGetData(Identifier, out var atom)
                ? atom
                : new ErrorAtom($"failed to resolve symbol <{Identifier}>", this, scope);
        }

        /// <summary>
        /// resolve appropriate method for list navigation
        /// </summary>
        /// <param name="a"></param>
        /// <param name="s"></param>
        /// <returns></returns>
        static Delegate SelectCadrMethod(char a, IScope s) => s.Resolve($"c{a}r");

        /// <summary>
        /// get identifier value
        /// </summary>
        /// <param name="scope"></param>
        /// <returns></returns>
        public override object GetValue(IScope scope) => Identifier;

        /// <summary>
        /// set value in scope based on name (overwrite if needed)
        /// </summary>
        /// <param name="value"></param>
        /// <param name="scope"></param>
        /// <returns>always true</returns>
        public bool TrySet(IAtom value, IScope scope)
        {
            scope.SetData(Identifier, value);
            return true;
        }

        public override bool Equals(IAtom other) => other is IdentifierAtom ia && ia.Identifier.Equals(Identifier);

        public int CompareTo(IAtom other)
        {
            if (other is IdentifierAtom id)
                // ReSharper disable once StringCompareToIsCultureSpecific
                return Identifier.CompareTo(id.Identifier);
            return 0;
        }

        public override string ToString() => Identifier;
    }
}