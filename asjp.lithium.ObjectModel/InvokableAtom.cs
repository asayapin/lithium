﻿using System;
using System.Collections.Generic;
using System.Linq;
using asjp.lithium.Contracts.Atoms;
using asjp.lithium.Contracts.Services;
using asjp.lithium.ObjectModel.Base;
using asjp.lithium.ObjectModel.Extensions;

namespace asjp.lithium.ObjectModel
{
    /// <summary>
    /// Atom to invoke atoms
    /// </summary>
    public class InvokableAtom : InvokableBase
    {
        readonly IAtom _target;
        readonly IScope _scope;

        /// <summary>
        /// Create instance
        /// </summary>
        /// <param name="args">args to bind</param>
        /// <param name="target">atom to evaluate on call</param>
        /// <param name="scope">scope to bind</param>
        private InvokableAtom(IEnumerable<string> args, IAtom target, IScope scope) 
            : base(args, target.Position)
        {
            _target = target;
            _scope = scope;
        }

        /// <summary>
        /// Create instance
        /// </summary>
        /// <param name="args">args to bind</param>
        /// <param name="target">atom to evaluate on call</param>
        /// <param name="scope">scope to bind</param>
        public InvokableAtom(IEnumerable<string> args, IAtom target) : this(args, target, null){

        }

        static IAtom[] Wrap(params object[] xs) => xs.Select(x => x.Wrap()).ToArray();

        static readonly Func<InvokableAtom, IScope, Delegate>[] Delegates =
        {
            (ia, s) => new Func<IAtom>(() => ia.Call(Array.Empty<IAtom>(), Array.Empty<IAtom>(), s)),
            (ia, s) => new Func<object, IAtom>((a) => ia.Call(Wrap(a), Array.Empty<IAtom>(), s)),
            (ia, s) => new Func<object, object, IAtom>((a, b) => ia.Call(Wrap(a, b), Array.Empty<IAtom>(), s)),
            (ia, s) => new Func<object, object, object, IAtom>((a, b, c) => ia.Call(Wrap(a, b, c), Array.Empty<IAtom>(), s)),
            (ia, s) => new Func<object, object, object, object, IAtom>((a, b, c, d) => ia.Call(Wrap(a, b, c, d), Array.Empty<IAtom>(), s)),
            (ia, s) => new Func<object, object, object, object, object, IAtom>((a, b, c, d, e) => ia.Call(Wrap(a, b, c, d, e), Array.Empty<IAtom>(), s)),
            (ia, s) => new Func<object, object, object, object, object, object, IAtom>((a, b, c, d, e, f) => ia.Call(Wrap(a, b, c, d, e, f), Array.Empty<IAtom>(), s)),
            (ia, s) => new Func<object, object, object, object, object, object, object, IAtom>((a, b, c, d, e, f, g) => ia.Call(Wrap(a, b, c, d, e, f, g), Array.Empty<IAtom>(), s)),
        };

        /// <summary>
        /// Get delegate corresponding to wrapped atom
        /// </summary>
        /// <param name="scope"></param>
        /// <returns>Atom wrapped in delegate</returns>
        public override object GetValue(IScope scope)
        {
            if (Args.Length < Delegates.Length - 1)
                return Delegates[Args.Length](this, scope);
            return this;
        }

        /// <summary>
        /// Invoke atom
        /// </summary>
        /// <param name="args">provided arguments</param>
        /// <param name="variadic">provided variadic arguments</param>
        /// <param name="s">scope to consider (added as child to saved one)</param>
        /// <returns>invocation result</returns>
        protected override IAtom Call(IAtom[] args, IAtom[] variadic, IScope s)
        {
            var closure = s.CreateChild(parent: _scope);
            BindArgs(args, variadic, closure);
            return _target.Evaluate(closure);
        }

        /// <summary>
        /// bind args to scope
        /// </summary>
        /// <param name="args">args to bind</param>
        /// <param name="closure">scope to use for binding</param>
        void BindArgs(IAtom[] args, IAtom[] variadic, IScope closure)
        {
            const string eagerSuffix = "::eager";
            const string valueSuffix = "::value";

            foreach (var (name, atom) in Args.Zip(args, (l, v) => (l, v)))
            {
                var n = name;
                var a = atom;
                if (name.ToLower().EndsWith(eagerSuffix))
                {
                    n = n.Substring(0, n.Length - eagerSuffix.Length);
                    a = a.Evaluate(closure);
                }
                if (name.ToLower().EndsWith(valueSuffix))
                {
                    n = n.Substring(0, n.Length - valueSuffix.Length);
                    a = a.Evaluate(closure).GetValue(closure).Wrap();
                }

                closure.SetData(n, a);
            }

            var possibleVariadic = Args.LastOrDefault() ?? "";
            if (possibleVariadic.StartsWith("..."))
            {
                
                var n = possibleVariadic.Substring(3);
                if (n.ToLower().EndsWith(eagerSuffix))
                {
                    n = n.Substring(0, n.Length - eagerSuffix.Length);
                    variadic = variadic.Select(x => x.Evaluate(closure)).ToArray();
                }
                if (n.ToLower().EndsWith(valueSuffix))
                {
                    n = n.Substring(0, n.Length - valueSuffix.Length);
                    variadic = variadic.Select(x => x.Evaluate(closure).GetValue(closure).Wrap()).ToArray();
                }

                closure.SetData(n, new ListAtom(variadic, (0, 0, 0)));
            }
        }

        public override string ToString() => $"{{{_target}}}";

        /// <summary>
        /// Create new curried invocable
        /// </summary>
        /// <param name="leftArgs">args left to bind</param>
        /// <param name="boundArgs">args already bound</param>
        /// <param name="parentScope">scope to branch from</param>
        /// <returns>curried invocable</returns>
        protected override IAtom Curry(IEnumerable<string> leftArgs, IEnumerable<IAtom> boundArgs, IScope parentScope)
        {
            var boundScope = parentScope.CreateChild(parent: _scope);
            BindArgs(boundArgs.ToArray(), Array.Empty<IAtom>(), boundScope);
            return new InvokableAtom(leftArgs, _target, boundScope);
        }
    }
}