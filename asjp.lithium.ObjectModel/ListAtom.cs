﻿using System.Collections.Generic;
using System.Linq;
using asjp.lithium.Contracts.Atoms;
using asjp.lithium.Contracts.Services;
using asjp.lithium.ObjectModel.Base;

namespace asjp.lithium.ObjectModel
{
    /// <summary>
    /// Enumerable atom holding arbitrary number of elements
    /// </summary>
    public class ListAtom : EnumerableBase, IAddable
    {
        readonly List<IAtom> _atoms;

        public ListAtom(IEnumerable<IAtom> atoms, (int l, int c, int o) pos) : base(pos) => _atoms = new List<IAtom>(atoms);

        /// <summary>
        /// Count of items wrapped
        /// </summary>
        public int Count => _atoms.Count;
        /// <summary>
        /// first atom
        /// </summary>
        /// <returns></returns>
        public override IAtom Head() => _atoms.Count > 0 ? _atoms[0] : ValueAtom.Nil;

        /// <summary>
        /// atoms except first, wrapped in new list atom
        /// </summary>
        /// <returns></returns>
        public override IAtom Tail() => _atoms.Count > 1
            ? new ListAtom(_atoms.Skip(1), Position)
            : ValueAtom.Nil;
        /// <summary>
        /// view over wrapped atoms
        /// </summary>
        public override IAtom[] Atoms => _atoms.ToArray();
        /// <summary>
        /// convert atoms to array of underlying objects
        /// </summary>
        /// <param name="scope"></param>
        /// <returns></returns>
        public override object GetValue(IScope scope) => _atoms.Select(x => x.GetValue(scope)).ToArray();
        public override bool Equals(IAtom other) => other is ListAtom la && _atoms.SequenceEqual(la._atoms);

        /// <summary>
        /// Create new atom with appended item
        /// </summary>
        /// <param name="right"></param>
        /// <returns></returns>
        public IAtom Add(IAtom right) => new ListAtom(_atoms.Append(right), Position);

        public override string ToString() => $"({string.Join(" ", _atoms.Select(x => x.ToString()))})";
    }
}