﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using asjp.lithium.Contracts.Atoms;
using asjp.lithium.Contracts.Services;
using asjp.lithium.ObjectModel.Extensions;

namespace asjp.lithium.ObjectModel
{
    /// <summary>
    /// Wrapper for CLR method group
    /// </summary>
    public class MethodGroupAtom : AtomBase, IInvokableAtom
    {
        readonly IEnumerable<MethodInfo> _methods;
        readonly object _target;

        public MethodGroupAtom(IEnumerable<MethodInfo> methods, object target)
        {
            _methods = methods;
            _target = target;
        }

        /// <summary>
        /// Get predicate to check whether method matches provided args 
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        static Func<MethodInfo, bool> Matches(object[] args)
        {
            return mi =>
            {
                var ps = mi.GetParameters();
                if (args.Length != ps.Length) return false;
                if (args.Length == 0) return true;
                return MatchesTypes(args)(mi);
            };
        }

        /// <summary>
        /// Create predicate to check whether types of arguments are passable to method
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        static Func<MethodInfo, bool> MatchesTypes(object[] args)
        {
            return mi =>
            {
                var ps = mi.GetParameters();
                return ps.Zip(args, (l, r) => (l.ParameterType, argType: r.GetType()))
                    .All(x => x.ParameterType.IsAssignableFrom(x.argType));
            };
        }

        /// <summary>
        /// referential equality
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public override bool Equals(IAtom other) => ReferenceEquals(this, other);

        /// <summary>
        /// Find matching method of group (for no args) and try to invoke it
        /// </summary>
        /// <param name="scope"></param>
        /// <returns>this or result of invocation if no-arg overload is present</returns>
        protected override IAtom PerformEvaluation(IScope scope)
        {
            // ReSharper disable once CoVariantArrayConversion
            var m = _methods.FirstOrDefault(Matches(new IAtom[0]));
            return m is null ? this : m.Invoke(_target, new object[0]).Wrap();
        }

        /// <summary>
        /// Get methods wrapped in atom
        /// </summary>
        /// <param name="scope"></param>
        /// <returns></returns>
        public override object GetValue(IScope scope) => _methods;

        /// <summary>
        /// Try to invoke one method suitable for provided arguments; does not curry
        /// </summary>
        /// <param name="args">args to invoke with</param>
        /// <param name="scope">scope to consider</param>
        /// <returns>result of invocation or error atom</returns>
        public IAtom Invoke(IEnumerable<IAtom> args, IScope scope)
        {
            var values = args.Select(x => x.Evaluate(scope).GetValue(scope)).ToArray();
            var m = _methods.Where(Matches(values)).ToArray();
            if (m.Any())
                return m.First().Invoke(_target, values).Wrap();

            m = _methods.Where(MatchesTypes(values)).ToArray();
            if (m.Length == 0) return new ErrorAtom("[lithium] no method can be selected for provided arguments", this, scope);
            return new MethodGroupAtom(m, _target);
        }
    }
}