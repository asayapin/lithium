﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using asjp.lithium.Contracts.Atoms;
using asjp.lithium.Contracts.Extensibility;
using asjp.lithium.Contracts.Services;
using asjp.lithium.ObjectModel.Base;
using asjp.lithium.ObjectModel.Extensions;

namespace asjp.lithium.ObjectModel
{
    /// <summary>
    /// single delegate wrapper
    /// </summary>
    public class MethodWrapperAtom : InvokableBase
    {
        readonly Delegate _method;

        /// <summary>
        /// Create instance from delegate
        /// </summary>
        /// <param name="d"></param>
        public MethodWrapperAtom(Delegate d)
            : base(
                d.Method.GetParameters().Where(x => x.ParameterType != typeof(IScope) && !x.ParameterType.FullName.Contains("Closure")).Select(x =>
                    (x.GetCustomAttribute<ParamArrayAttribute>() is null ? "" : "...") + x.Name),
                (0, 0, 0),
                d.Method.GetParameters().Any(x => x.ParameterType == typeof(IScope)))
            => _method = d;

        /// <summary>
        /// Create instance from method info
        /// </summary>
        /// <param name="mi"></param>
        /// <param name="target"></param>
        public MethodWrapperAtom(MethodInfo mi, object? target = null)
            : this(MethodInfoToDelegate(mi, target))
        {
        }

        static Delegate MethodInfoToDelegate(MethodInfo mi, object? target = null)
        {
            return mi.CreateDelegate(
                Expression.GetDelegateType(mi
                    .GetParameters()
                    .Select(x => x.ParameterType)
                    .Append(mi.ReturnType)
                    .ToArray())
                , target);
        }

        /// <summary>
        /// return wrapped delegate
        /// </summary>
        /// <param name="scope"></param>
        /// <returns></returns>
        public override object GetValue(IScope scope) => _method;
        public override string ToString() => $"{_method.Method.Name}";

        /// <summary>
        /// perform required evaluations over args and invoke underlying method
        /// </summary>
        /// <param name="args">positional args</param>
        /// <param name="variadic">variadic args</param>
        /// <param name="scope">scope to consider</param>
        /// <returns>invocation result wrapped in atom</returns>
        protected override IAtom Call(IAtom[] args, IAtom[] variadic, IScope scope)
        {
            var parameters = _method.Method.GetParameters().Where(x => !x.ParameterType.FullName.Contains("Closure")).ToArray();
            var eager = _method.Method.GetCustomAttribute<EagerAttribute>() is not null;

            var result = new List<object>();
            for (var i = 0; i < parameters.Length; i++)
            {
                var parameter = parameters[i];
                if (!typeof(IAtom).IsAssignableFrom(parameter.ParameterType))
                {
                    if (parameter.GetCustomAttribute<ParamArrayAttribute>() is not null)
                    {
                        object[] add;
                        if (IsEager(eager, parameter) && parameter.ParameterType == typeof(IAtom[]))
                            add = variadic.Select(x => x.Evaluate(scope)).ToArray();
                        else if (parameter.ParameterType == typeof(object[]))
                            add = variadic.Select(x => x.Evaluate(scope).GetValue(scope)).ToArray();
                        else add = variadic;
                        result.Add(add);
                    }
                    else result.Add(args[i].Evaluate(scope).GetValue(scope));
                }
                else if (IsEager(eager, parameter))
                    result.Add(args[i].Evaluate(scope));
                else result.Add(args[i]);
            }

            return _method.DynamicInvoke(result.ToArray()).Wrap();
        }

        private static bool IsEager(bool eager, ParameterInfo parameter)
        {
            return eager && parameter.GetCustomAttribute<LazyAttribute>() is null
                   || parameter.GetCustomAttribute<EagerAttribute>() is not null;
        }

        /// <inheritdoc cref="InvokableBase.Curry"/>
        protected override IAtom Curry(IEnumerable<string> leftArgs, IEnumerable<IAtom> boundArgs, IScope parentScope)
        {
            var noTypes = Array.Empty<Type>();
            var mi = typeof(Enumerable).GetMethod(nameof(Enumerable.Concat)).MakeGenericMethod(typeof(IAtom));
            var args = leftArgs.Select(x => Expression.Parameter(typeof(IAtom), x)).ToArray();
            var scope = Expression.Parameter(typeof(IScope), "scope");
            var parent = Expression.Parameter(typeof(IScope), "parentScope");
            var self = Expression.Parameter(typeof(IInvokableAtom), "self");
            var bound = Expression.Parameter(typeof(IEnumerable<IAtom>), "bound");
            var addParent = Expression.Call(scope, nameof(IScope.AddParent), noTypes, parent);

            var newArr = Expression.NewArrayInit(typeof(IAtom), args);
            var concat = Expression.Call(null, mi, bound, newArr);
            var invoke = Expression.Call(self, nameof(IInvokableAtom.Invoke), noTypes, concat, addParent);
            var lambda = Expression.Lambda(
                Expression.GetDelegateType(args.Append(scope).Select(x => x.Type).Append(typeof(IAtom)).ToArray()),
                invoke, 
                args.Append(scope).ToArray());

            var result = Expression.Lambda<Func<IInvokableAtom, IEnumerable<IAtom>, IScope, Delegate>>(lambda, self, bound, parent);
            
            return new MethodWrapperAtom(
                result.Compile()(this, boundArgs, parentScope)
            );
        }
    }
}