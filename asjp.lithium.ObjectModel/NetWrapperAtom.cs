﻿using asjp.lithium.Contracts.Atoms;
using asjp.lithium.Contracts.Services;
using System;
using System.Reflection;
using asjp.lithium.ObjectModel.Extensions;

namespace asjp.lithium.ObjectModel
{
    /// <summary>
    /// Wrapper for CLR objects not falling to primitives, delegates or enumerables category
    /// </summary>
    public class NetWrapperAtom : AtomBase, ISettable
    {
        readonly object _value;
        readonly MemberInfo member;
        readonly object[] indexerArgs;
        readonly bool isMemberWrapper;

        public NetWrapperAtom(object o)
        {
            _value = o;
            isMemberWrapper = false;
        }

        public NetWrapperAtom(object target, MemberInfo member, params object[] indexerArgs)
        {
            isMemberWrapper = true;
            _value = target;
            this.member = member;
            this.indexerArgs = indexerArgs ?? Array.Empty<object>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public override bool Equals(IAtom other)
        {
            if (other is NetWrapperAtom nwa)
                return Equals(nwa.GetValue(null), GetValue(null));
            return other?.Equals(Evaluate(null)) ?? false;
        }

        /// <summary>
        /// returns wrapped value as plain atom
        /// </summary>
        /// <param name="scope"></param>
        /// <returns></returns>
        protected override IAtom PerformEvaluation(IScope scope) => GetValue(scope).Wrap();

        /// <summary>
        /// return wrapped value
        /// </summary>
        /// <param name="scope">not used</param>
        /// <returns></returns>
        public override object GetValue(IScope scope) => isMemberWrapper ? member.AsDelegate()(_value, indexerArgs) : _value;

        public bool TrySet(IAtom value, IScope scope)
        {
            if (!isMemberWrapper) return false;
            try
            {
                member.AsSetter()(_value, indexerArgs, value.GetValue(scope));
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
