﻿using asjp.lithium.Contracts.Atoms;
using asjp.lithium.Contracts.Services;

namespace asjp.lithium.ObjectModel
{
    /// <summary>
    /// Wrapper atom to prevent premature evaluation
    /// </summary>
    public class QuotedAtom : AtomBase
    {
        public IAtom WrappedAtom { get; }

        public QuotedAtom(IAtom wrappedAtom, (int l, int c, int o) pos) : base(pos) => WrappedAtom = wrappedAtom;

        /// <summary>
        /// returns underlying atom
        /// </summary>
        /// <param name="scope">not used</param>
        /// <returns></returns>
        protected override IAtom PerformEvaluation(IScope scope) => WrappedAtom;

        /// <summary>
        /// get value of underlying atom wrapped to array
        /// </summary>
        /// <param name="scope"></param>
        /// <returns></returns>
        public override object GetValue(IScope scope) => new[] {WrappedAtom.GetValue(scope)};

        /// <summary>
        /// routes check to wrapped atoms
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        bool Equals(QuotedAtom other)
        {
            return Equals(WrappedAtom, other.WrappedAtom);
        }

        public override bool Equals(IAtom other) => other is QuotedAtom qa && Equals(qa);

        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((QuotedAtom) obj);
        }

        public override int GetHashCode() => (WrappedAtom != null ? WrappedAtom.GetHashCode() : 0);

        public override string ToString() => $"'{WrappedAtom}";
    }
}