﻿using System;
using System.Collections.Generic;
using System.Linq;
using asjp.lithium.Contracts.Atoms;
using asjp.lithium.Contracts.Services;
using asjp.lithium.ObjectModel.Extensions;

namespace asjp.lithium.ObjectModel
{
    public class RecordAtom : AtomBase, TypeAtom
    {
        readonly Dictionary<string, string> typeAliases = new()
        {
            ["string"] = typeof(string).FullName,
            ["byte"] = typeof(byte).FullName,
            ["sbyte"] = typeof(sbyte).FullName,
            ["short"] = typeof(short).FullName,
            ["ushort"] = typeof(ushort).FullName,
            ["int"] = typeof(int).FullName,
            ["uint"] = typeof(uint).FullName,
            ["long"] = typeof(long).FullName,
            ["ulong"] = typeof(ulong).FullName,
            ["float"] = typeof(float).FullName,
            ["double"] = typeof(double).FullName,
            ["decimal"] = typeof(decimal).FullName,
            ["guid"] = typeof(Guid).FullName,
            ["dict"] = typeof(DictionaryAtom).FullName,
            ["obj"] = typeof(object).FullName
        };

        readonly List<(string, string)> properties = new();

        public string Prefix { get; set; }

        public void AddProperty(string name, string typeAlias)
        {
            var ix = properties.FindIndex(x => x.Item1 == name);
            var item = (name, typeAlias);
            if (ix < 0)
                properties.Add(item);
            else
                properties[ix] = item;
        }

        public IAtom CreateInstance(IScope scope, IAtom[] args)
        {
            if (properties.Count == 0) return ValueAtom.Nil;

            var result = new DictionaryAtom(Array.Empty<(string, IAtom)>());

            var argPos = 0;
            foreach (var (prop, type) in properties)
            {
                IAtom value;
                try
                {
                    if (args.Length > argPos)
                    {
                        value = args[argPos++].Evaluate(scope);
                        if (value is NetWrapperAtom nwa)
                            value = nwa.Evaluate(scope);
                        if (type.Contains("[]") && value.IsNil())
                            value = new ListAtom(Array.Empty<IAtom>(), scope.GetPosition());
                    }
                    else if (type.Contains("[]"))
                    {
                        value = new ListAtom(Array.Empty<IAtom>(), scope.GetPosition());
                    }
                    else if (scope.TryGetData(type, out var atom)
                             && atom is TypeAtom ta)
                    {
                        value = ta.CreateInstance(scope, Array.Empty<IAtom>());
                    }
                    else
                    {
                        var clrName = typeAliases.Aggregate(type, (acc, nextKvp) => acc.Replace(nextKvp.Key, nextKvp.Value));
                        value = Activator.CreateInstance(Type.GetType(clrName)).Wrap();
                    }
                }
                catch
                {
                    value = ValueAtom.Nil;
                }
                result[prop] = value;
            }

            return result;
        }

        public override bool Equals(IAtom other) => ReferenceEquals(this, other);

        protected override IAtom PerformEvaluation(IScope scope) => this;

        public override object GetValue(IScope scope) => null;
    }
}