﻿using asjp.lithium.Contracts.Atoms;
using asjp.lithium.Contracts.Services;
using asjp.lithium.ObjectModel.Base;

namespace asjp.lithium.ObjectModel
{
    /// <summary>
    /// Special case for pairs
    /// </summary>
    public class TupleAtom : EnumerableBase
    {
        readonly IAtom _head;
        readonly IAtom _tail;

        public TupleAtom(IAtom head, IAtom tail, (int l, int c, int o) pos) : base(pos)
        {
            _head = head;
            _tail = tail;
        }

        /// <summary>
        /// first (left) atom
        /// </summary>
        /// <returns></returns>
        public override IAtom Head() => _head;
        /// <summary>
        /// second (right) atom
        /// </summary>
        /// <returns></returns>
        public override IAtom Tail() => _tail;
        /// <summary>
        /// array holding both atoms
        /// </summary>
        public override IAtom[] Atoms => new[] {_head, _tail};

        /// <summary>
        /// returns CLR value tuple
        /// </summary>
        /// <param name="scope"></param>
        /// <returns></returns>
        public override object GetValue(IScope scope) => (_head.GetValue(scope), _tail.GetValue(scope));

        protected bool Equals(TupleAtom other) => Equals(_head, other._head) && Equals(_tail, other._tail);

        public override bool Equals(IAtom other) => other is TupleAtom ta && Equals(ta);

        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((TupleAtom) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((_head != null ? _head.GetHashCode() : 0) * 397) ^ (_tail != null ? _tail.GetHashCode() : 0);
            }
        }

        public override string ToString() => $"({_head} . {_tail})";
    }
}