﻿using System;
using asjp.lithium.Contracts.Atoms;
using asjp.lithium.ObjectModel.Helpers;
using asjp.lithium.Contracts.Services;
using asjp.lithium.ObjectModel.Extensions;

namespace asjp.lithium.ObjectModel
{
    /// <summary>
    /// Atom holding primitive value (string, numeric, bool)
    /// </summary>
    public class ValueAtom : AtomBase, IEquatable<ValueAtom>, IComparable<IAtom>, IAddable, IArithmetic
    {
        readonly object? _value;
        ValueAtom(object? o, (int l, int c, int off) pos) : base(pos) => _value = o;
        public ValueAtom(string s, (int l, int c, int o) pos) : base(pos) => _value = s;
        public ValueAtom(ValueType vt, (int l, int c, int o) pos) : base(pos) => _value = vt;

        /// <summary>
        /// primitive true value
        /// </summary>
        public static readonly IAtom T = new ValueAtom(true, (0, 0, 0));

        /// <summary>
        /// primitive false value
        /// </summary>
        public static readonly IAtom Nil = new ValueAtom((object?)null, (0, 0, 0));
        /// <summary>
        /// returns this
        /// </summary>
        /// <param name="scope">not used</param>
        /// <returns></returns>
        protected override IAtom PerformEvaluation(IScope scope) => this;

        /// <summary>
        /// returns underlying value
        /// </summary>
        /// <param name="scope">not used</param>
        /// <returns></returns>
        public override object GetValue(IScope scope) => _value!;

        public override bool Equals(IAtom other) => Equals(other as ValueAtom);

        public int CompareTo(IAtom other)
        {
            if (other is ValueAtom va)
            {
                if (_value is IComparable ic1 && va._value is IComparable ic2)
                    return ic1.CompareTo(ic2);
            }
            return 0;
        }

        public override bool Equals(object? obj)
        {
            if (obj is null) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj is ValueAtom va) return Equals(va);
            return false;
        }

        public bool Equals(ValueAtom? other)
        {
            if (other is null) return false;
            return ReferenceEquals(this, other) || (_value is null && other._value is null) || Equals(_value, other._value);
        }

        public override int GetHashCode() => _value != null ? _value.GetHashCode() : 0;

        /// <summary>
        /// attempts to invoke binary arithmetic function on this and second arg
        /// </summary>
        /// <param name="s">function to invoke</param>
        /// <param name="right">second arg</param>
        /// <returns>result or nil</returns>
        IAtom InvokeBinary(string s, IAtom right)
        {
            var r = right.GetValue(null);
            return Calculator.TryInvoke(out var res, s, _value, r) ? res.Wrap() : Nil;
        }

        /// <summary>
        /// attempt to invoke unary function on underlying value
        /// </summary>
        /// <param name="s">operation to invoke</param>
        /// <returns>result or nil</returns>
        IAtom InvokeUnary(string s) => Calculator.TryInvoke(out var res, s, _value) ? res.Wrap() : Nil;

        /// <summary>
        /// try perform addition
        /// </summary>
        /// <param name="right"></param>
        /// <returns></returns>
        public IAtom Add(IAtom right) => InvokeBinary("add", right);

        /// <summary>
        /// try perform subtraction
        /// </summary>
        /// <param name="right"></param>
        /// <returns></returns>
        public IAtom Sub(IAtom right) => InvokeBinary("sub", right);

        /// <summary>
        /// try multiply
        /// </summary>
        /// <param name="right"></param>
        /// <returns></returns>
        public IAtom Mul(IAtom right) => InvokeBinary("mul", right);

        /// <summary>
        /// try divide
        /// </summary>
        /// <param name="right"></param>
        /// <returns></returns>
        public IAtom Div(IAtom right) => InvokeBinary("div", right);

        /// <summary>
        /// try get modulo
        /// </summary>
        /// <param name="right"></param>
        /// <returns></returns>
        public IAtom Mod(IAtom right) => InvokeBinary("mod", right);

        /// <summary>
        /// try raise to power
        /// </summary>
        /// <param name="right"></param>
        /// <returns></returns>
        public IAtom Pow(IAtom right) => InvokeBinary("pow", right);

        /// <summary>
        /// try negate underlying value (pure)
        /// </summary>
        /// <returns></returns>
        public IAtom Neg() => InvokeUnary("neg");

        /// <summary>
        /// try increment underlying value (pure)
        /// </summary>
        /// <returns></returns>
        public IAtom Inc() => InvokeUnary("inc");

        /// <summary>
        /// try decrement underlying value (pure)
        /// </summary>
        /// <returns></returns>
        public IAtom Dec() => InvokeUnary("dec");

        public override string ToString() => _value?.ToString() ?? "nil";
    }
}