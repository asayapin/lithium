﻿using System;
using System.Linq;
using asjp.lithium.Contracts.Atoms;
using asjp.lithium.Contracts.Extensibility;
using asjp.lithium.Contracts.Services;
using asjp.lithium.Defaults.StdLibParts;
using asjp.lithium.ObjectModel;
using asjp.lithium.ObjectModel.Extensions;
using DeepEqual.Syntax;
using FluentAssertions;
using NUnit.Framework;

namespace asjp.lithium.Testing.Bindings
{
    [ExtensionPart]
    public class Assertions : PartBase
    {
        [Names("!", "assert:all"), Eager]
        public static IAtom All(params IAtom[] args)
        {
            Assert.IsTrue(args.All(x => x.IsT()));
            return ValueAtom.T;
        }

        [Names("!~", "assert:nil"), Eager]
        public static IAtom Nil(IAtom arg)
        {
            arg.IsNil().Should().BeTrue();
            return ValueAtom.T;
        }

        [Names("!!", "assert:eq")]
        public static IAtom Eq(object left, object right)
        {
            right.Should().Be(left);
            return ValueAtom.T;
        }

        [Names("!!!", "assert:deep-eq")]
        public static IAtom DeepEq(object left, object right)
        {
            right.ShouldDeepEqual(left);
            return ValueAtom.T;
        }
        
        [Names("!?", "assert:any"), Eager]
        public static IAtom Any(params IAtom[] args)
        {
            Assert.IsTrue(args.Any(x => x.IsT()));
            return ValueAtom.T;
        }
        [Names("assert:in")]
        public static IAtom In(object target, params object[] args)
        {
            CollectionAssert.Contains(args, target);
            return ValueAtom.T;
        }

        [Names("assert:throws")]
        public static IAtom Throws(IAtom atom, IScope scope)
        {
            Assert.Catch<Exception>(() => atom.Evaluate(scope));
            return ValueAtom.T;
        }

        [Names("assert:of-type")]
        public static IAtom OfType(object atom, string type)
        {
            atom.GetType().FullName.Should().Be(type);
            return ValueAtom.T;
        }

        [Names("assert:result")]
        public static IAtom Result([Eager] IAtom atom, object expected, IScope scope)
        {
            var a = atom as AwaitableAtom;
            a.Should().NotBeNull();
            a!.Task.Result.Evaluate(scope).GetValue(scope).Should().Be(expected);
            
            return ValueAtom.Nil;
        }
    }
}