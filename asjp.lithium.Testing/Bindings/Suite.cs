﻿using System;
using System.Collections.Generic;
using System.Linq;
using asjp.lithium.Contracts.Atoms;
using asjp.lithium.Contracts.Extensibility;
using asjp.lithium.Contracts.Services;
using asjp.lithium.ObjectModel;
using asjp.lithium.ObjectModel.Extensions;

namespace asjp.lithium.Testing.Bindings
{
    [ExtensionPart]
    public static class Suite
    {
        const string casesStore = "__cases",
            suiteStore = "__suite";

        public static T Get<T>(this IScope scope, string name, Func<T> init, Func<T, IAtom>? wrap = null)
        {
            if (scope.TryGetData(name, out var atom)
                && atom.GetValue(scope) is T t)
                return t;

            t = init();
            wrap ??= x => new NetWrapperAtom(x);
            scope.SetData(name, wrap(t));
            return t;
        }

        public static string SuiteName(this IScope scope) => 
            scope.Get<string>(suiteStore, () => "", x => x.Wrap());

        public static List<(string name, IAtom content)> Cases(this IScope scope) =>
            scope.Get<List<(string, IAtom)>>(casesStore, () => new());

        [Names("lt:suite"), Eager]
        static IAtom AddSuite(string name, IScope scope, params IAtom[] _)
        {
            scope.SetData(suiteStore, name.Wrap());
            return ValueAtom.T;
        }

        [Names("lt:test")]
        static IAtom Test(string name, IAtom content, IScope scope)
        {
            scope.Cases().Add((name, content));
            return ValueAtom.T;
        }

        [Names("lt:load")]
        static IAtom Load(string fileName, IScope scope)
        {
            var content = TestProvider.TestFiles.FirstOrDefault(x => x.RelativePath == fileName)?.Content ?? "t";
            TestProvider.Engine.Evaluate(content, scope);
            return ValueAtom.T;
        }

        [Names("lt:mock")]
        static IAtom Mock(string name, object mock, IScope scope)
        {
            if (mock is not Delegate d || scope is not MockableScope ms) return ValueAtom.Nil;
            
            ms.AddMock(name, d);
            return ValueAtom.T;
        }
    }
}