﻿using System;
using System.Collections.Generic;
using System.Linq;
using asjp.lithium.Contracts.Atoms;
using asjp.lithium.Contracts.Services;

namespace asjp.lithium.Testing
{
    public class MockableScope : IScope
    {
        readonly IScope innerScope;
        readonly Dictionary<string, Delegate> mocks;

        public MockableScope(IScope scope, IEnumerable<KeyValuePair<string, Delegate>>? predefinedMocks = null)
        {
            innerScope = scope;
            mocks = new(predefinedMocks ?? Array.Empty<KeyValuePair<string, Delegate>>());
        }

        public bool TryGetData(string name, out IAtom result) => innerScope.TryGetData(name, out result);

        public Delegate Resolve(string name) => mocks.TryGetValue(name, out var d) ? d : innerScope.Resolve(name);

        public void SetData(string name, IAtom value) => innerScope.SetData(name, value);

        public IScope AddParent(IScope scope)
        {
            innerScope.AddParent(scope);
            return this;
        }

        public IScope CreateChild(IExtensionsContainer? extensions = null, IScope? parent = null) =>
            new MockableScope(innerScope.CreateChild(extensions, parent), mocks);

        public bool AddImport(string i) => innerScope.AddImport(i);
        public Stack<StackFrame> Trace => innerScope.Trace;

        public IEnumerable<IScope> Parents => innerScope.Parents.Prepend(innerScope);

        public IReadOnlyDictionary<string, Delegate> Methods =>
            mocks.Concat(innerScope.Methods).ToDictionary(x => x.Key, x => x.Value);

        public IReadOnlyDictionary<string, IAtom> Data => innerScope.Data;
        public IEnumerable<string> ImportedNamespaces => innerScope.ImportedNamespaces;

        public void AddMock(string name, Delegate d) => mocks[name] = d;
    }
}