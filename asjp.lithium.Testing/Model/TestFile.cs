﻿using System;
using System.IO;

namespace asjp.lithium.Testing.Model
{
    public class TestFile
    {
        private static readonly Lazy<string> currentDirectory = new(Directory.GetCurrentDirectory);

        public TestFile(string relativePath)
        {
            Content = File.ReadAllText(relativePath);
            RelativePath = relativePath.Replace('\\', '/');
        }

        public static string CurrentDirectory => currentDirectory.Value;
        
        public string RelativePath { get; }
        public string Content { get; }

        public static TestFile Create(string path) => 
            new(path.Replace(CurrentDirectory, "").TrimStart('/').TrimStart('\\'));
    }
}