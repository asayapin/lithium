﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using asjp.lithium.Contracts.Atoms;
using asjp.lithium.Contracts.Services;
using asjp.lithium.Defaults;
using asjp.lithium.ObjectModel.Extensions;
using asjp.lithium.Testing.Bindings;
using asjp.lithium.Testing.Model;
using NUnit.Framework;

namespace asjp.lithium.Testing
{
    public static class TestProvider
    {
        public static readonly Engine Engine = Engine.Create();
        static readonly IScope? BaseScope;
        internal static readonly List<TestFile> TestFiles = new();

        static TestProvider()
        {
            BaseScope = new MockableScope(new LexicalScope(new Extensions()));

            TestFiles.AddRange(Directory.EnumerateFiles(
                    Directory.GetCurrentDirectory(),
                    "*.lisp",
                    SearchOption.AllDirectories)
                .Select(TestFile.Create));
        }
        
        public static IEnumerable<TestCaseData> GetCases()
        {
            foreach (var file in TestFiles) {
                var scope = BaseScope!.CreateChild();
                Engine.Evaluate(file.Content, scope);
                var name = scope.SuiteName();
                var cases = scope.Cases().ToArray();
                for (var index = 0; index < cases.Length; index++) {
                    var pair = cases[index];
                    yield return new TestCaseData(pair.content, scope, index == cases.Length - 1).SetName(pair.name).SetCategory(name);
                }
            }
        }
    }
}