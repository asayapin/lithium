﻿using System;
using System.Collections.Generic;
using System.Linq;
using asjp.lithium.Contracts.Atoms;
using asjp.lithium.Contracts.Extensibility;
using asjp.lithium.Contracts.Services;
using asjp.lithium.Defaults;
using asjp.lithium.ObjectModel;
using asjp.lithium.ObjectModel.Extensions;
using NUnit.Framework;

namespace asjp.lithium.Tests
{
    public class ArbitrarySignatureTests
    {
        class Container : IExtensionsContainer
        {
            public IEnumerable<EngineMethodInfo> GetMethodDefinitions()
            {
                yield return new(new Func<IAtom, IAtom>(SimpleConventionalNoArgs), new[] { "arbs:2" });
                yield return new(new Func<IAtom, bool>(NonAtomReturnType), new[] { "arbs:3" });
                yield return new(new Func<int, IAtom>(NonAtomParameter), new[] { "arbs:4" });
                yield return new(new Func<IScope, bool>(RequiresScopeSingle), new[] { "arbs:5" });
                yield return new(new Func<IAtom[], bool>(VariadicNoArgs), new[] { "arbs:7" });
                yield return new(new Func<IScope, IAtom[], bool>(ScopeAndVariadics), new[] { "arbs:8" });
                yield return new(new Func<IAtom, IScope, bool>(RequiresScopeLast), new[] { "arbs:9" });
                yield return new(new Func<IAtom, IScope, IAtom[], bool>(FullExample), new[] { "arbs:a" });
                yield return new(new Func<int, IAtom[], bool>(VariadicAndExplicit), new[] { "arbs:b" });
                yield return new(new Func<IAtom, IAtom, bool>(PartialEager), new[] { "arbs:c" });
                yield return new(new Func<IAtom, ValueAtom, bool>(PartialEagerWithSpecificType), new[] { "arbs:d" });
                yield return new(new Func<IAtom, IAtom, bool>(FullyEager), new[] { "arbs:e" });
                yield return new(new Func<IAtom, IAtom, bool>(EagerWithOverride), new[] { "arbs:f" });
                yield return new(new Func<IAtom[], bool>(EagerVariadic), new[] { "arbs:g" });
                yield return new(new Func<IAtom[], bool>(EagerMethodWithVariadic), new[] { "arbs:h" });
                yield return new(new Func<object[], bool>(Values), new[] { "arbs:i" });
            }

            public IAtom SimpleConventionalNoArgs(IAtom a) => a.IsT().Wrap();

            public bool NonAtomReturnType(IAtom a) => a.IsT();

            public IAtom NonAtomParameter(int b) => (b > 50).Wrap();

            public bool RequiresScopeSingle(IScope scope) => scope is not null;
            public bool RequiresScopeLast(IAtom a, IScope scope) => scope is not null && a.IsT();

            public bool VariadicNoArgs(params IAtom[] vs) => vs.All(x => x.IsT());
            public bool VariadicAndExplicit(int x, params IAtom[] vs) => x > 50 && vs.All(AtomExtensions.IsT);

            public bool ScopeAndVariadics(IScope s, params IAtom[] vs) => s is not null && vs.All(AtomExtensions.IsT);
            
            public bool FullExample(IAtom a, IScope s, params IAtom[] vs) => s is not null && vs.All(AtomExtensions.IsT) && a.IsT();

            public bool PartialEager(IAtom lazy, [Eager] IAtom eager) => lazy is QuotedAtom && eager is ValueAtom;
            public bool PartialEagerWithSpecificType(IAtom lazy, [Eager] ValueAtom eager) => lazy is QuotedAtom && eager.GetValue(null) is int;

            [Eager]
            public bool FullyEager(IAtom a, IAtom b) => a is ValueAtom && b is ValueAtom;

            [Eager]
            public bool EagerWithOverride([Lazy] IAtom lazy, IAtom eager) => lazy is QuotedAtom && eager is ValueAtom;

            public bool EagerVariadic([Eager] params IAtom[] eager) => eager.All(x => x is ValueAtom);
            
            [Eager]
            public bool EagerMethodWithVariadic(params IAtom[] eager) => eager.All(x => x is ValueAtom);

            public bool Values(params object[] vs) => vs.All(x => x is int);
        }

        Engine engine;
        IScope scope;

        [SetUp]
        public void Setup()
        {
            engine = Engine.Create().Extend(new Container());
            scope = new LexicalScope();
        }

        public static IEnumerable<TestCaseData> Cases()
        {
            yield return new TestCaseData("(arbs:2 123)", true).SetName("simple conventional, no args attr - all args");
            yield return new TestCaseData("((arbs:2) 123)", true).SetName("simple conventional, no args attr - curried");
            yield return new TestCaseData("(arbs:3 123)", true).SetName("non-atom ret type - all args");
            yield return new TestCaseData("((arbs:3) 123)", true).SetName("non-atom ret type - curried");
            yield return new TestCaseData("(arbs:4 123)", true).SetName("non-atom parameter - all args");
            yield return new TestCaseData("((arbs:4) 123)", true).SetName("non-atom parameter - curried");
            yield return new TestCaseData("(arbs:5)", true).SetName("requires scope - all args");
            yield return new TestCaseData("(arbs:7)", true).SetName("variadic without args - no args");
            yield return new TestCaseData("(arbs:7 123)", true).SetName("variadic without args - 1 arg");
            yield return new TestCaseData("(arbs:7 123 123 123)", true).SetName("variadic without args - 3 args");
            yield return new TestCaseData("(arbs:8)", true).SetName("scope and variadics - no args");
            yield return new TestCaseData("(arbs:8 123)", true).SetName("scope and variadics - 1 args");
            yield return new TestCaseData("(arbs:8 123 123 123)", true).SetName("scope and variadics - 3 args");
            yield return new TestCaseData("(arbs:9 123)", true).SetName("requires scope and arg - all args");
            yield return new TestCaseData("((arbs:9) 123)", true).SetName("requires scope and arg - curry");
            yield return new TestCaseData("(arbs:a 123)", true).SetName("full non eager - explicit, no vars");
            yield return new TestCaseData("((arbs:a) 123)", true).SetName("full non eager - explicit, no vars, curry");
            yield return new TestCaseData("(arbs:a 123 123 123 123)", true).SetName("full non eager - explicit, 3 vars");
            yield return new TestCaseData("(arbs:b 123)", true).SetName("variadic & explicit - explicit, no vars");
            yield return new TestCaseData("((arbs:b) 123)", true).SetName("variadic & explicit - explicit, no vars, curry");
            yield return new TestCaseData("(arbs:b 123 123 123 123)", true).SetName("variadic & explicit - explicit, 3 vars");
            yield return new TestCaseData("(arbs:c '123 '123)", true).SetName("partial eager");
            yield return new TestCaseData("(arbs:d '123 '123)", true).SetName("partial eager w/concrete type");
            yield return new TestCaseData("(arbs:e '123 '123)", true).SetName("fully eager");
            yield return new TestCaseData("(arbs:f '123 '123)", true).SetName("eager with override");
            yield return new TestCaseData("(arbs:g)", true).SetName("eager variadic - no args");
            yield return new TestCaseData("(arbs:g '123)", true).SetName("eager variadic - single");
            yield return new TestCaseData("(arbs:g '123 '123 '123)", true).SetName("eager variadic - three");
            yield return new TestCaseData("(arbs:h)", true).SetName("eager method variadic - no args");
            yield return new TestCaseData("(arbs:h '123)", true).SetName("eager method variadic - single");
            yield return new TestCaseData("(arbs:h '123 '123 '123)", true).SetName("eager method variadic - three");
            yield return new TestCaseData("(arbs:i)", true).SetName("objects - no args");
            yield return new TestCaseData("(arbs:i '123)", true).SetName("objects - single");
            yield return new TestCaseData("(arbs:i '123 '123 '123)", true).SetName("objects - three");
        }

        [TestCaseSource(nameof(Cases))]
        public void Tests(string source, object expectation)
        {
            var o = engine.Evaluate(source, scope);
            if (expectation is null) Assert.IsNull(o);
            else Assert.IsTrue(o.Equals(expectation));
        }
    }
}