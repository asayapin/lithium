﻿using asjp.lithium.Contracts.Services;
using asjp.lithium.Defaults;
using FluentAssertions;
using NUnit.Framework;

namespace asjp.lithium.Tests;

public class DebuggableEngineTests
{
    private (DebuggableEngine, IScope) Setup() => (DebuggableEngine.Create(), new LexicalScope());

    [Test]
    public void BasicEvaluationWorksAsExpected()
    {
        var (engine, scope) = Setup();
        
        engine.Load("(+ 1 2 3)", scope);
        var (res, off) = engine.Run();

        res.Should().Be(6);
        off.Should().BeNull();
        
        engine.Reset();
    }

    [Test]
    public void BreakingOnBreakpointsWorks()
    {
        var (engine, scope) = Setup();
        
        engine.Load("(+ (+ 1 2 3) 4 5)", scope);
        engine.SetBreakpoint(3);
        
        var (res, off) = engine.Run();

        res.Should().BeNull();
        off.Should().Be(3);

        (res, off) = engine.Run();

        res.Should().Be(15);
        off.Should().BeNull();
        
        engine.Reset();
    }

    [Test]
    public void SuccessiveBreakingOnBreakpointsWorks()
    {
        var (engine, scope) = Setup();
        
        engine.Load("(def rand () ((. (new 'System.Random) 'Next))) (rand)", scope);
        engine.SetBreakpoint(13);
        engine.SetBreakpoint(17);
        
        var (res, off) = engine.Run();

        res.Should().BeNull();
        off.Should().Be(13);
        
        (res, off) = engine.Run();

        res.Should().BeNull();
        off.Should().Be(17);

        (res, off) = engine.Run();

        res.Should().NotBeNull();
        off.Should().BeNull();
        
        engine.Reset();
    }
}