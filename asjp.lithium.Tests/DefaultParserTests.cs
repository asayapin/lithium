using System;
using System.Collections.Generic;
using System.Linq;
using asjp.lithium.ObjectModel;
using asjp.lithium.Defaults;
using FluentAssertions;
using NUnit.Framework;

namespace asjp.lithium.Tests
{
    public class DefaultParserTests
    {
        readonly DefaultParser _parser = new DefaultParser();

        public static IEnumerable<TestCaseData> CreateCasesForTokenizer()
        {
            yield return new TestCaseData("()", new[] { "(", ")" }) { TestName = "brackets - separate tokens" };
            yield return new TestCaseData("1", new[] { "1" }) { TestName = "literal" };
            yield return new TestCaseData(@"""as""", new[] { @"""as""" }) { TestName = "string" };
            yield return new TestCaseData(@"""\""as""", new[] { @"""""as""" }) { TestName = "string with back" };
            yield return new TestCaseData(@"\ a\ \q a\a", new[]{"\\", "a\\", "\\q", "a\\a"}){TestName = "backslash can be part of name"};
            yield return new TestCaseData("(1 2 3)", new[] { "(", "1", "2", "3", ")" }) { TestName = "list - brackets+literals" };
            yield return new TestCaseData("(\n)", new[] { "(", ")" }) { TestName = "line feeds do not influence tokenizing" };
            yield return new TestCaseData("(1;first\n2;second\r3;azaz\n)", new[] { "(", "1", "2", "3", ")" }) { TestName = "line comments" };
            yield return new TestCaseData("'(1 2 3)", new[] { "'", "(", "1", "2", "3", ")" }) { TestName = "quote is separate token" };
            yield return new TestCaseData(@"'""as""", new[] { "'", @"""as""" }) { TestName = "quote is separate token for string" };
            yield return new TestCaseData("'1", new[] { "'", "1" }) { TestName = "quote is separate token for number" };
            yield return new TestCaseData("s'", new[] { "s'" }) { TestName = "quote can be part of name" };
            yield return new TestCaseData("s'a", new[] { "s'a" }) { TestName = "quote can be part of name" };
            yield return new TestCaseData(@"""()';123 123""", new[] { @"""()';123 123""" }) { TestName = "string can contain anything" };
            yield return new TestCaseData("'''1", new[] { "'", "'", "'", "1" }) { TestName = "sequential quotes" };
        }

        public static IEnumerable<TestCaseData> CreateCasesForParsing()
        {
            yield return new TestCaseData("123:sbyte", new[] { typeof(ValueAtom) }, new object[] { (sbyte)123 }) { TestName = "sbyte value" };
            yield return new TestCaseData("123", new[] { typeof(ValueAtom) }, new object[] { 123 }) { TestName = "int (default) value" };
            yield return new TestCaseData("129:byte", new[] { typeof(ValueAtom) }, new object[] { (byte)129 }) { TestName = "byte value" };
            yield return new TestCaseData("1230:short", new[] { typeof(ValueAtom) }, new object[] { (short)1230 }) { TestName = "short value" };
            yield return new TestCaseData("40000:ushort", new[] { typeof(ValueAtom) }, new object[] { (ushort)40000 }) { TestName = "short value" };
            yield return new TestCaseData("700000", new[] { typeof(ValueAtom) }, new object[] { 700000 }) { TestName = "int value" };
            yield return new TestCaseData("4000000000:uint", new[] { typeof(ValueAtom) }, new object[] { 4000000000U }) { TestName = "uint value" };
            yield return new TestCaseData("8000000000:long", new[] { typeof(ValueAtom) }, new object[] { 8000000000L }) { TestName = "long value" };
            yield return new TestCaseData("8000000000:ulong", new[] { typeof(ValueAtom) }, new object[] { 8000000000UL }) { TestName = "ulong value" };
            yield return new TestCaseData("1.25:float", new[] { typeof(ValueAtom) }, new object[] { 1.25f }) { TestName = "float value" };
            yield return new TestCaseData("1.25", new[] { typeof(ValueAtom) }, new object[] { 1.25 }) { TestName = "double (default) value" };
            yield return new TestCaseData("1.0", new[] { typeof(ValueAtom) }, new object[] { 1.0 }) { TestName = "double value even if convertible to int" };
            yield return new TestCaseData("1.25:decimal", new[] { typeof(ValueAtom) }, new object[] { 1.25f }) { TestName = "decimal value" };
            yield return new TestCaseData("1e270", new[] { typeof(ValueAtom) }, new object[] { 1e270 }) { TestName = "double value" };
            yield return new TestCaseData("1e270:nex", new[] { typeof(ValueAtom) }, new object[] { 1e270 }) { TestName = "non-existent converter" };
            yield return new TestCaseData(@"""asdf""", new[] { typeof(ValueAtom) }, new object[] { "asdf" }) { TestName = "string value" };
            yield return new TestCaseData("t", new[] { typeof(ValueAtom) }, new object[] { true }) { TestName = "t" };
            yield return new TestCaseData("nil", new[] { typeof(ValueAtom) }, new object[] { null }) { TestName = "nil" };
            yield return new TestCaseData("()", new[] { typeof(ValueAtom) }, new object[] { null }) { TestName = "()" };
            yield return new TestCaseData("'123", new[] { typeof(QuotedAtom), typeof(ValueAtom) }, new object[] { new[]{123}, 123 }) { TestName = "quoted int value" };
            yield return new TestCaseData("asd", new[] { typeof(IdentifierAtom) }, new object[] { "asd" }) { TestName = "identifier value" };
            yield return new TestCaseData(
                "(1 2 3)",
                new[] {typeof(ListAtom)},
                new object[] {new object[] {1, 2, 3}}) {TestName = "list value"};
            yield return new TestCaseData(
                "(1 . 2)",
                new[] { typeof(TupleAtom) },
                new object[] { (1, 2) }) { TestName = "dot pair" };
            yield return new TestCaseData("'", new[] { typeof(IdentifierAtom) }, new object[] { "'" }) { TestName = "lone quote is identifier" };
        }

        [TestCaseSource(nameof(CreateCasesForTokenizer), Category = "tokenizer")]
        public void TestTokenizing(string input, string[] expected)
        {
            var tokens = _parser.GetTokens(input).ToArray();
            Assert.IsTrue(expected.SequenceEqual(tokens.Select(x => x.Item1)));
        }

        [TestCaseSource(nameof(CreateCasesForParsing), Category = "parser")]
        public void TestParsing(string script, Type[] expectedTypes, object[] expectedValues)
        {
            var atom = _parser.Parse(script)[0];

            var buf = atom;
            foreach (var type in expectedTypes)
            {
                Assert.IsTrue(type == buf.GetType());
                var tmp = buf.Evaluate(null);
                if (ReferenceEquals(tmp, buf)) break;
                buf = tmp;
            }

            buf = atom;
            foreach (var value in expectedValues)
            {
                Assert.AreEqual(value, buf.GetValue(null));
                var tmp = buf.Evaluate(null);
                if (ReferenceEquals(tmp, buf)) break;
                buf = tmp;
            }
        }

        [Test]
        public void TestOffsetsCollection()
        {
            var source = "(1 2 35)\r\n\r\n'(4 5 6)";

            var atoms = _parser.Parse(source);
            atoms.Length.Should().Be(2);
            atoms[0].Position.Should().Be((0, 0, 0));
            (atoms[0] as ListAtom)
                .Should().NotBeNull()
                .And.Subject.As<ListAtom>().Atoms
                .Select(x => x.Position)
                .Zip(new[] { (0, 1, 1), (0, 3, 3), (0, 5, 5) })
                .Should().AllSatisfy(pair => pair.First.Should().Be(pair.Second));
            (atoms[1] as QuotedAtom).Should().NotBeNull()
                .And.Subject.As<QuotedAtom>().Evaluate(null).As<ListAtom>()
                .Should().NotBeNull()
                .And.Match<ListAtom>(x => x.Position.line == 4 && x.Position.col == 2 && x.Position.offset == 13)
                .And.Subject.As<ListAtom>()
                .Atoms.Select(x => x.Position).
                Zip(new[] { (4, 3, 14), (4, 5, 16), (4, 7, 18) })
                .Should().AllSatisfy(pair => pair.First.Should().Be(pair.Second));
        }
    }
}