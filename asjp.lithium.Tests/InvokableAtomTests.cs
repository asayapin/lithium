﻿using System;
using asjp.lithium.Contracts.Atoms;
using asjp.lithium.ObjectModel;
using asjp.lithium.Contracts.Services;
using asjp.lithium.Defaults;
using asjp.lithium.ObjectModel.Extensions;
using FluentAssertions;
using NUnit.Framework;
// ReSharper disable InconsistentNaming

namespace asjp.lithium.Tests
{
    [TestFixture(Category = "invokable atom")]
    public class InvokableAtomTests
    {
        LexicalScope DefaultScope;
        static readonly DefaultParser Parser = new DefaultParser();
        private static readonly Engine engine = Engine.Create();

        readonly Func<IAtom, IAtom, IAtom, IAtom, IAtom> noVariadic = TestWithParams; 
        readonly Func<IAtom, IAtom[], IAtom> variadic = FuncWithParams;
        readonly Func<IScope, IAtom> noArg = NoArgFunction; 
        
        [SetUp]
        public void InitScope()
        {
            DefaultScope = new LexicalScope(StdLib.Instance);
        }

        static IAtom W(object o) => o.Wrap();

        static IAtom NoArgFunction(IScope scope)
        {
            Assert.IsNotNull(scope);
            return W(567);
        }

        static IAtom FuncWithParams(IAtom args, params IAtom[] varargs)
        {
            Assert.IsTrue(args is not IEnumerableAtom);
            return ValueAtom.Nil;
        }

        static IAtom TestWithParams(IAtom a, IAtom b, IAtom c, IAtom d)
        {
            Assert.IsTrue(a.IsT());
            Assert.IsTrue(b.IsT());
            Assert.IsTrue(c.IsT());
            Assert.IsTrue(d.IsT());
            return ValueAtom.Nil;
        }

        [Test]
        public void OnCall_NoArgs_NoArgIsEvaluated()
        {
            var atom = new MethodWrapperAtom(noArg);

            var res = atom.Invoke(new IAtom[0], DefaultScope);
            var res1 = atom.Evaluate(DefaultScope);

            Assert.AreEqual(W(567), res);
            Assert.AreEqual(res, res1);
        }

        [Test]
        public void OnCall_NoArgs_FuncWithArgsReturned()
        {
            var atom = new MethodWrapperAtom(variadic);

            var res = atom.Invoke(new IAtom[0], DefaultScope);
            var res1 = atom.Evaluate(DefaultScope);

            Assert.IsTrue(ReferenceEquals(atom, res));
            Assert.IsTrue(ReferenceEquals(atom, res1));
        }

        [Test]
        public void OnCall_WithParams_LeftoverIsBoundToSingleParam()
        {
            var atom = new MethodWrapperAtom(variadic);

            var _ = atom.Invoke(new[] { W(1), W(2), W(3), W(4), W(5) }, DefaultScope);
        }
        [Test]
        public void OnCall_IfVarArgIsUnbound_CallIsDone()
        {
            var atom = new MethodWrapperAtom(variadic);

            var _ = atom.Invoke(new[] { W(1) }, DefaultScope);
        }

        [Test]
        public void OnCall_FuncMayBeCurried()
        {
            var atom = new MethodWrapperAtom(noVariadic);

            var curry1 = atom.Invoke(new[] { W(1) }, DefaultScope) as MethodWrapperAtom;
            Assert.IsNotNull(curry1);

            var curry2 = curry1.Invoke(new[] { W(2), W(3) }, DefaultScope) as MethodWrapperAtom;
            Assert.IsNotNull(curry2);

            var final = curry2.Invoke(new[] { W(4) }, DefaultScope);
            Assert.IsTrue(final.IsNil());

            var over = curry2.Invoke(new[] { W(4), W(5) }, DefaultScope);
            Assert.IsTrue(over.IsNil());
        }

        [Test]
        public void LambdasCanCurry()
        {
            var atom = new InvokableAtom(
                new[] { "a", "b", "c", "d" },
                new ListAtom(
                    new[]
                    {
                        new IdentifierAtom("+", (0, 0, 0)),
                        new IdentifierAtom("a", (0, 0, 0)),
                        new IdentifierAtom("b", (0, 0, 0)),
                        new IdentifierAtom("c", (0, 0, 0)),
                        new IdentifierAtom("d", (0, 0, 0)),
                    }, (0, 0, 0)));

            var c1 = atom.Invoke(new[] { W(1) }, DefaultScope);
            Assert.IsTrue(c1 is IInvokableAtom);

            var c1i = c1 as IInvokableAtom;
            var c2 = c1i.Invoke(new[] { W(2), W(3) }, DefaultScope);
            Assert.IsTrue(c2 is IInvokableAtom);

            var c2i = c2 as IInvokableAtom;
            Assert.AreEqual(W(10), c2i.Invoke(new[] { W(4) }, DefaultScope));
            Assert.AreEqual(W(10), c2i.Invoke(new[] { W(4), W(5) }, DefaultScope));
        }

        [TestCase("1 2 3", 6, Description = "already evaluated should work normally")]
        [TestCase("'1 2 '3", 6, Description = "quoted should work normally")]
        [TestCase("(+ 2 5) 2 '3", 12, Description = "expression should work normally")]
        public void EagerEvaluationForInvokables(string args, int expected){
            var atom = new InvokableAtom(
                new[] { "a::eager", "b", "c::eager" },
                new ListAtom(
                    new[]{
                        new IdentifierAtom("+", (0, 0, 0)),
                        new IdentifierAtom("a", (0, 0, 0)),
                        new IdentifierAtom("b", (0, 0, 0)),
                        new IdentifierAtom("c", (0, 0, 0)),
                    }, (0, 0, 0)));

            var res = atom.Invoke(Parser.Parse(args), DefaultScope).GetValue(DefaultScope);

            Assert.AreEqual(expected, res);
        }

        [Test]
        public void LispFunctionDefinitionCanAcceptVararg()
        {
            var definition = "(def variadic (...args) (length args))";

            engine.Evaluate(definition, DefaultScope)
                .Should().NotBeNull()
                .And.BeAssignableTo<Delegate>();

            // the definition should already be loaded into scope - now we can run assertions

            engine.Evaluate(definition + " (variadic)", DefaultScope)
                .Should().Be(0);

            engine.Evaluate(definition + " (variadic 123)", DefaultScope)
                .Should().Be(1);

            engine.Evaluate(definition + " (variadic 123 234)", DefaultScope)
                .Should().Be(2);
        }

        [Test]
        public void VariadicLispFunctionsCorrectlyConsiderEvaluationMode()
        {
            var definition = "(def variadic (...args::eager) (length args))";

            engine.Evaluate(definition, DefaultScope)
                .Should().NotBeNull()
                .And.BeAssignableTo<Delegate>();

            // the definition should already be loaded into scope - now we can run assertions

            engine.Evaluate(definition + " (variadic)", DefaultScope)
                .Should().Be(0);

            engine.Evaluate(definition + " (variadic 123)", DefaultScope)
                .Should().Be(1);

            engine.Evaluate(definition + " (variadic 123 234)", DefaultScope)
                .Should().Be(2);
        }
    }
}