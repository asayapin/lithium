﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using asjp.lithium.ObjectModel.Extensions;
using asjp.lithium.ObjectModel;
using asjp.lithium.Contracts.Services;
using asjp.lithium.Defaults;
using System.Linq;
using asjp.lithium.Contracts.Atoms;

namespace asjp.lithium.Tests
{
    public class NetIntegrationTests
    {
        static DefaultParser parser;
        static Func<IAtom, IScope, IAtom> typeResolver;

        [OneTimeSetUp]
        public static void Init()
        {
            parser = new();
            var definitions = StdLib.Instance.GetMethodDefinitions();
            typeResolver = definitions.First(x => x.Names.Contains("type")).Method as Func<IAtom, IScope, IAtom>;
        }

        static Func<object, string, TestCaseData> CaseGeneratorFor<T>() => (o, s) => new TestCaseData(o, typeof(T)) { TestName = s };

        public static IEnumerable<TestCaseData> CreateCasesForPrimitiveValues()
        {
            var gen = CaseGeneratorFor<ValueAtom>();
            yield return gen(1, "int");
            yield return gen(1d, "double");
            yield return gen(1f, "float");
            yield return gen(1m, "decimal");
            yield return gen(1L, "long");
            yield return gen(1u, "uint");
            yield return gen(1ul, "ulong");
            yield return gen((sbyte)1, "sbyte");
            yield return gen((byte)1, "byte");
            yield return gen((short)1, "short");
            yield return gen((ushort)1, "ushort");
            yield return gen(true, "true");
            yield return gen(null, "()");
            yield return gen("asd", "string");
            gen = CaseGeneratorFor<TupleAtom>();
            yield return gen((123, 234), "tuple");
            gen = CaseGeneratorFor<ListAtom>();
            yield return gen(new[] { 1, 2, 3, 4 }, "list");
            yield return gen((1, 2, 3, 4), "4-tuple");
            yield return gen(new Dictionary<string, int>(), "dict");
            gen = CaseGeneratorFor<MethodWrapperAtom>();
            yield return gen(new Func<string, int, bool, string>((_, _, _) => ""), "delegate wrapping");
            yield return gen(typeof(int).GetMethod("Parse", new[] { typeof(string) }), "method info");
            gen = CaseGeneratorFor<NetWrapperAtom>();
            yield return gen(new Random(), "random");
        }

        static IScope WithNamespaces(IScope scope, params string[] imports)
        {
            var s = scope.CreateChild();
            foreach (var i in imports) s.AddImport(i);
            return s;
        }

        public static IEnumerable<TestCaseData> CreateCasesForTypeResolution()
        {
            var scope = new LexicalScope();
            var scopeWithSystem = WithNamespaces(scope, "System", "System.Collections.Generic");
            scopeWithSystem.SetData("c", new IdentifierAtom("Int32", (0, 0, 0)));
            scopeWithSystem.SetData("cc", new MethodWrapperAtom(new Func<IAtom, IAtom>(a => new ListAtom(new[] { new IdentifierAtom("List`1", (0, 0, 0)), a }, (0, 0, 0)))));
            yield return new TestCaseData("'System.Int32", typeof(int), scope);
            yield return new TestCaseData("'Int64", typeof(long), scopeWithSystem);
            yield return new TestCaseData("'Guid", typeof(Guid), scopeWithSystem);
            yield return new TestCaseData("'(List`1 Int32)", typeof(List<int>), scopeWithSystem);
            yield return new TestCaseData("'(List`1)", null, scopeWithSystem);
            yield return new TestCaseData("'(List`1 Int32 Int32)", null, scopeWithSystem);
            yield return new TestCaseData("'(List`1 (List`1 (List`1 Int32)))", typeof(List<List<List<int>>>), scopeWithSystem);
            yield return new TestCaseData("c", typeof(int), scopeWithSystem);
            yield return new TestCaseData("(cc Int32)", typeof(List<int>), scopeWithSystem);
            yield return new TestCaseData("'(Func`1 Int32)", typeof(Func<int>), scopeWithSystem);
        }

        [TestCaseSource(nameof(CreateCasesForPrimitiveValues))]
        public void PrimitiveValuesAreWrappedToCorrespondingAtoms(object value, Type expectedType)
        {
            Assert.AreEqual(expectedType, value.Wrap().GetType());
        }

        [TestCaseSource(nameof(CreateCasesForTypeResolution))]
        public void TestTypeResolution(string definition, Type expected, IScope scope)
        {
            var atom = parser.Parse(definition).First();
            var type = typeResolver(atom.Evaluate(scope), scope)?.GetValue(scope);

            if (expected is null)
            {
                Assert.IsNull(type);
            }
            else
            {
                Assert.IsTrue(type is Type);
                Assert.AreEqual(expected, type);
            }
        }
    }
}
