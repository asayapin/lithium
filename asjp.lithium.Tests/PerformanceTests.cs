﻿using asjp.lithium.Contracts.Services;
using asjp.lithium.Defaults;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace asjp.lithium.Tests
{
    public class PerformanceTests
    {
        const int Runs = 1_000_000;

        static IScope scope;
        static DefaultParser parser;

        [OneTimeSetUp]
        public static void Init()
        {
            parser = new DefaultParser();
            scope = new LexicalScope();
        }

        public static IEnumerable<TestCaseData> CreateCases()
        {
            if (Environment.GetEnvironmentVariable("PERFORMANCE_TESTING") is null) yield break;
            yield return new TestCaseData("(+ 123 456)", new Func<object>(() => 123 + 456), 10000m) { TestName = "addition" };
        }

        [TestCaseSource(nameof(CreateCases))]
        public void RunTest(string script, Func<object> etalon, decimal expectedDowngradePercent)
        {
            var atom = parser.Parse(script).First();
            Func<object> raw = () => atom.Evaluate(scope);

            var measRaw = MeasureRuns(raw);
            var measEtl = MeasureRuns(etalon);
            var expectedMax = measEtl * (1 + expectedDowngradePercent / 100);

            Console.WriteLine($"  etalon: {measEtl}");
            Console.WriteLine($"     max: {expectedMax}");
            Console.WriteLine($"     raw: {measRaw} (+{(measRaw - measEtl) / measEtl * 100}%)");
            Assert.IsTrue(expectedMax > measRaw, $"{expectedMax} vs {measRaw}");
        }

        static decimal MeasureRuns(Func<object> act)
        {
            var stopwatch = Stopwatch.StartNew();
            for (var i = 0; i < Runs; i++)
                act();
            stopwatch.Stop();
            return stopwatch.ElapsedMilliseconds * 1m;
        }

    }
}
