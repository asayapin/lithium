﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using asjp.lithium.Contracts.Atoms;
using asjp.lithium.Contracts.Extensibility;
using asjp.lithium.ObjectModel;
using asjp.lithium.Contracts.Services;
using asjp.lithium.Defaults;
using asjp.lithium.ObjectModel.Extensions;
using NUnit.Framework;

namespace asjp.lithium.Tests
{
    public class Deep
    {
        public Deep()
        {
            D = this;
        }
        public Deep D;
        public List<int> L = new();
        public int Value;
        public int Prop { get; set; }
        public DoubleIx X = new();

        public int First
        {
            get => L.Count > 0 ? L[0] : 0;
            set
            {
                if (L.Count > 0) L[0] = value;
                else L.Add(value);
            }
        }

        public int this[int index]
        {
            get => L.Count > index ? L[index] : 0;
            set
            {
                while (L.Count <= index)
                    L.Add(0);
                L[index] = value;
            }
        }

        public class DoubleIx
        {
            readonly Dictionary<int, int> cache = new();
            public int this[int l, int r]
            {
                get => cache.TryGetValue(l + r, out var t) ? t : 0;
                set => cache[l + r] = value;
            }
        }
    }

    public class StdLibTests
    {
        class LispAssertions : IExtensionsContainer
        {
            public IEnumerable<EngineMethodInfo> GetMethodDefinitions()
            {
                yield return new(new Func<IAtom, IScope, IAtom>(AssertTrue), new[] { "?!", "assert-true" });
            }

            static IAtom AssertTrue([Eager] IAtom res, IScope scope)
            {
                if (res.IsT()) return ValueAtom.T;
                if (res is ErrorAtom ea) throw ea;
                throw new AssertionException($"<{res.GetValue(scope)}> failed to evaluate to t");
            }
        }

        static Engine engine;

        [OneTimeSetUp]
        public static void InitEngine()
        {
            engine = Engine.Create().Extend(new LispAssertions());
        }

        delegate TestCaseData TestCaseGenerator(string script, string expected, string message);

        static int n;

        static TestCaseGenerator WithCategory(string category)
        {
            return (s, e, m) => new TestCaseData(s, e) { TestName = $"[{n++:D4}] {m}" }.SetCategory(category);
        }

        public static IEnumerable<TestCaseData> CreateStdLibCases()
        {
            var gen = WithCategory("general");
            yield return gen("1", @"1", "reflexive");
            yield return gen(@"(set a '(1 2 3))", @"'(1 2 3)", "set");
            yield return gen(@"(|> () (set (? t 'c1 'c2) 123) c1)", "123", "crazy set - with runtime name definition");
            yield return gen(@"(|> () (set a '(1 2 3)) a)", @"'(1 2 3)", "set + get");
            yield return gen(@"(|> () (set a t) (cond a 1 2))", "1", "cond eval true");
            yield return gen(@"(|> () (set a nil) (cond a 1 2))", "2", "cond eval false");
            yield return gen(@"(isdef |>)", "'(t)", "isdef - defined func");
            yield return gen(@"(isdef list set car)", "'(t t t)", "isdef - 3 defined funcs");
            yield return gen(@"(?? ? !)", "'(t nil)", "isdef - mixed content");
            yield return gen(@"(?? ???)", "'(nil)", "isdef - not defined");
            yield return gen(@"(|> () (set ax 123) (?? ax))", "'(t)", "isdef - defined var");
            gen = WithCategory("seq");
            yield return gen("(list 1 2 3)", "'(1 2 3)", "list");
            yield return gen("(append '(1 2 3) 4)", "'(1 2 3 4)", "append");
            yield return gen("(car '(1 2 3))", "1", "car");
            yield return gen("(car (cons 1 2))", "1", "car on tuple");
            yield return gen("(|> () (set a '(1 2 3)) (car a))", "1", "car + set");
            yield return gen("(cdr '(1 2 3))", "'(2 3)", "cdr");
            yield return gen("(cdr (cons 1 2))", "2", "cdr on tuple");
            yield return gen("(cdr '())", "nil", "cdr on empty");
            yield return gen("(cdr '(1))", "nil", "cdr on single item");
            yield return gen("(cddr '(1))", "nil", "cddr on empty");
            yield return gen("(|> () (set a '(1 2 3)) (cdr a))", "'(2 3)", "cdr + set");
            yield return gen("(cadr '(1 2 3))", "2", "cadr");
            yield return gen("(caddr '(1 2 3))", "3", "caddr");
            yield return gen(@"(map (lambda (x) (< x 2)) '(1 2 3))", "'(t nil nil)", "map");
            yield return gen(@"(filter (lambda (x) (< x 2)) '(1 2 3))", "'(1)", "filter");
            yield return gen(@"(reduce * 1 '(1 2 3))", "6", "reduce");
            yield return gen(@"(reduce + '(1 2 3))", "6", "reduce no seed");
            yield return gen(@"(cat '(1 2 3) '(4 5 6))", "'(1 2 3 4 5 6)", "concat");
            yield return gen(@"(@ '(0 1 2 3 4) 2)", @"2", "index - present");
            yield return gen(@"(@ '(0 1 2 3 4) 9)", @"4", "index - wrapping");
            yield return gen(@"(@ '(0 1 2 3 4) -9)", @"1", "index - negative wrapping");
            gen = WithCategory("flow");
            yield return gen(@"((λ (x) (caddr x)) (1 2 3))", "3", "caddr+λ");
            yield return gen(@"(|> () (set cc (lambda (x) (caddr x))) (cc (1 2 3)))", "3", "caddr+λ+set");
            yield return gen("(|> () (defun cc (x) (caddr x)) (cc (1 2 3)))", "3", "caddr+defun");
            yield return gen(@"(eval '(car '(1 2 3)))", "1", "eval quote");
            yield return gen(@"(|> () (set x 123) (proc (x) (set x ""456"")) x)", "123", "proc+inner var");
            yield return gen(@"(|> () (set a 5) (++ a) a)", "6", "inc");
            yield return gen(@"(|> () (set a 5) (-- a) a)", "4", "dec");
            yield return gen(@"((eval '(lambda (a) (* a a))) 10)", "100", "eval+application");
            yield return gen(@"(eval (cons 'car ''(1 2 3)))", "1", "tuple evaluation");
            yield return gen(@"((pipe car atom) ''(1 2 3))", "t", "pipe - two single-args");
            yield return gen(@"((pipe car (> 2)) ''(1 2 3))", "t", "pipe - single-arg + curried");
            yield return gen(@"((pipe car) ''(1 2 3))", "1", "pipe - single func");
            yield return gen(@"((~> (& t) (| nil) ~ atom) t)", "t", "pipe - a lot of currying");
            gen = WithCategory("logic");
            yield return gen(@"(cond t 1 2)", "1", "cond true");
            yield return gen(@"(cond nil 1 2)", "2", "cond false");
            yield return gen("(atom 5)", "t", "is atom - atom");
            yield return gen("(atom '(1 2 3))", "nil", "is atom - list");
            yield return gen("(& t t)", "t", "t t & == t");
            yield return gen("(& t ())", "nil", "t () & == nil");
            yield return gen("(& nil t)", "nil", "nil t & == ()");
            yield return gen("(& nil ())", "nil", "nil () & == nil");
            yield return gen("(| t t)", "t", "t t | == t");
            yield return gen("(| t ())", "t", "t () | == t");
            yield return gen("(| nil t)", "t", "nil t | == t");
            yield return gen("(| nil ())", "nil", "nil () | == nil");
            yield return gen("(~ ())", "t", "() ~ == t");
            yield return gen("(~ t)", "nil", "t ~ == nil");
            gen = WithCategory("io");
            yield return gen(@"($ ""{0} {1} {2}"" 1 ""az"" 2)", @"""1 az 2""", "format - simple");
            yield return gen(@"($ ""{0} {1} {2}"" (* 4 5) ""az"" 2)", @"""20 az 2""", "format + ivk");
            yield return gen(@"($ ""{0:#.0} {1} {2}"" 10.34 ""az"" 2)", @"""10.3 az 2""", "format - specifiers");
            yield return gen(@"($$ 1 ""az"" 2)", @"""1 az 2""", "inline format - simple");
            yield return gen(@"($$ (* 4 5) ""az"" 2)", @"""20 az 2""", "inline format + ivk");
            gen = WithCategory("comparison");
            yield return gen("(> 1 1)", "nil", "1 > 1 == f");
            yield return gen("(> 1 3)", "nil", "1 > 3 == f");
            yield return gen("(> 1 -1)", "t", "1 > -1 == t");
            yield return gen("(< 1 1)", "nil", "1 < 1 == f");
            yield return gen("(< 1 3)", "t", "1 < 3 == t");
            yield return gen("(< 1 -1)", "nil", "1 < -1 == f");
            yield return gen("(>= 1 1)", "t", "1 >= 1 == t");
            yield return gen("(>= 1 3)", "nil", "1 >= 3 == f");
            yield return gen("(>= 1 -1)", "t", "1 >= -1 == t");
            yield return gen("(<= 1 1)", "t", "1 <= 1 == t");
            yield return gen("(<= 1 3)", "t", "1 <= 3 == t");
            yield return gen("(<= 1 -1)", "nil", "1 <= -1 == f");
            yield return gen("(neq 1 1)", "nil", "1 neq 1 == f");
            yield return gen("(neq 1 3)", "t", "1 neq 3 == t");
            yield return gen("(neq '(1 2 3) '(1 2 3))", "nil", "(1 2 3) neq (1 2 3) == f");
            yield return gen("(neq '(1 2 3) 3)", "t", "(1 2 3) neq 3 == t");
            yield return gen("(> 'asd 'asd)", "nil", "asd > asd == f");
            yield return gen("(>= 'asd 'asd)", "t", "asd >= asd == t");
            yield return gen("(< 'asd 'asd)", "nil", "asd < asd == f");
            yield return gen("(<= 'asd 'asd)", "t", "asd <= asd == t");
            yield return gen("(!= 'asd 'asd)", "nil", "asd != asd == f");
            gen = WithCategory("equality");
            yield return gen("1", "1", "1 == 1");
            yield return gen(@"""asd""", @"""asd""", "'asd' == 'asd'");
            yield return gen("()", "nil", "() == nil");
            yield return gen("nil", "nil", "nil == nil");
            yield return gen("t", "T", "t == T");
            yield return gen("'asd", "'asd", "identifier equality");
            yield return gen("'(1 2 3)", "'(1 2 3)", "'(1 2 3) == '(1 2 3)");
            gen = WithCategory("arithmetic");
            yield return gen("(+ 1)", "1", "1 + == 1");
            yield return gen("(+ 1 2)", "3", "1 2 + == 3");
            yield return gen("(+ 1 2 3)", "6", "1 2 3 + == 6");
            yield return gen("(- 1)", "1", "1 - == 1");
            yield return gen("(- 1 2)", "-1", "1 2 - == -1");
            yield return gen("(- 1 2 3)", "-4", "1 2 3 - == -4");
            yield return gen("(* 1)", "1", "1 * == 1");
            yield return gen("(* 1 2)", "2", "1 2 * == 2");
            yield return gen("(* 1 2 3)", "6", "1 2 3 * == 6");
            yield return gen("(/ 0.5 1)", "0.5", "1/2 1 / == 0.5");
            yield return gen("(/ 4 2)", "2", "4 2 / == 2");
            yield return gen("(/ 4 22)", "0", "4 22 / == 0");
            yield return gen("(% 4 3)", "1", "4 3 % == 1");
            yield return gen("(/ 0.5 1)", "0.5", "1/2 1 / == 0.5");
            yield return gen("(neg 1)", "-1", "1 neg == -1");
            yield return gen("(neg -1)", "1", "-1 neg == 1");
            yield return gen("(^ 1 2)", "1.0", "1 2 ^ == 1");
            yield return gen("(^ 2 1)", "2.0", "2 1 ^ == 2");
            yield return gen("(^ 2 2)", "4.0", "2 2 ^ == 4");
            gen = WithCategory("should fail");
            yield return gen("(|> asd 123)", "nil", "proc with empty scope override");
            yield return gen("(|> ())", "nil", "proc with empty instructions");
            yield return gen("(<- asd '(1 2 3))", "nil", "reduce with no func");
            yield return gen("(<- + asd)", "nil", "reduce on non-list");
            yield return gen("(?> asd '(1 2 3))", "nil", "filter with no func");
            yield return gen("(?> (> 5) asd)", "nil", "filter on non-list");
            yield return gen("(-> asd '(1 2 3))", "nil", "map with no func");
            yield return gen("(-> (> 5) asd)", "nil", "map on non-list");
            yield return gen("(@ asd 3)", "nil", "index on non-list");
            yield return gen("(@ '(1 2 3) asd)", "nil", "index with non-int");
            yield return gen("(-- asd)", "nil", "dec on junk");
            yield return gen("(++ asd)", "nil", "inc on junk");
            yield return gen("(cat asd '(1 2 3))", "nil", "cat with junk 1");
            yield return gen("(cat '(1 2 3) asd)", "nil", "cat with junk 2");
            yield return gen("(append asd 3)", "nil", "append to junk");
            yield return gen("(car asd)", "nil", "car junk");
            yield return gen("(neg '(1 2 3))", "nil", "neg list");
            yield return gen("(set '(1 2 3) 123)", "nil", "set to list");
            //gen = WithCategory("oop");
            //yield return gen(@"(|> () ((class az) (?? az)))", "'(t)", "isdef - defined class");
            //yield return gen(@"(|> () ((class a (test 123)) (set ci (new a)) (nav ci test)))", "123", "define class + instantiate + get member");
            //yield return gen(@"(|> () ((class a (mtd (/\ () t))) (set ci (new a)) ((nav ci mtd))))", "t", "define class + instantiate + get member + invoke");
            //yield return gen(@"(|> () ((class a (test 123) ((a) (set (nav self test) a))) (set ci (new a 456)) (nav ci test)))", "456", "define class + instantiate w/arg + get member");
            gen = WithCategory(".net integration");
            yield return gen("(import System System)", "'(t nil)", "import same");
            yield return gen("(atom (new 'System.Int32))", "t", "new+wrapper");
            yield return gen(@"((nav 'System.Linq.Enumerable 'Range) 1 5)", "'(1 2 3 4 5)", "static method");
            yield return gen("(nav 'System.Int32 'MaxValue)", int.MaxValue.ToString(), "static field");
            yield return gen("(nav 'System.Int32 'Miss)", "()", "static missing field");
            yield return gen("(nav 'System.Int32 ())", "()", "nav to nil");
            yield return gen("((nav 'System.Diagnostics.Debug 'Assert) t)", "nil", "static method group");
            yield return gen(@"((nav (new 'System.Text.StringBuilder ""123"") 'ToString))", @"""123""", "new+args+wrapper");
            yield return gen(@"(|> () (import System) (atom (new 'Int32)))", "t", "import single & resolve");
            yield return gen(@"(|> () (import System.Collections.Generic System) (set a (new '(Dictionary`2 String Int32))) ((nav a 'Add) ""q"" 1) (eq (nav a ""q"") 1))", @"t", "generic instantiation");
            yield return gen("(new 'System.Int33)", "nil", "not found type");
            yield return gen("(new 'System.Collections.Generic.List`1)", "nil", "generic - no params");
            yield return gen("(|> () (import System.Collections.Generic) (new '(Dictionary`2 System.Int32)))", "nil", "generic - count mismatch - less");
            yield return gen("(|> () (import System.Collections.Generic System) (new '(Dictionary`2 Int32 Int32 Int32)))", "nil", "generic - count mismatch - more");
            yield return gen("((new '(System.Func`1 asjp.lithium.Contracts.Atoms.IAtom) (λ () 42)))", "42", "System.Func creation");
            yield return gen("((new '(System.Func`2 asjp.lithium.Contracts.Atoms.IAtom asjp.lithium.Contracts.Atoms.IAtom) (λ (a) (+ a 42))) 42)", "84", "System.Func creation");
            yield return gen("(|> () (set d (new 'asjp.lithium.Tests.Deep)) ((nav d 'D 'D 'D 'L 'Add) 5) (nav d 'L 0))", "5", "long navigation");
            gen = WithCategory("tasks");
            yield return gen("(wait (task (λ () 42)))", "42", "wait single task");
            yield return gen("(wait (chain (task (λ () 1)) (λ (r) (+ r 2)) (λ (r) (+ r 3))))", "6",
                "wait chain");
        }

        [TestCaseSource(nameof(CreateStdLibCases))]
        public void TestScript(string script, string expected)
        {
            engine.Evaluate($"(?! (eq {script} {expected}))", new LexicalScope());
        }

        [TestCase("42", "42", TestName = "sanity check")]
        [TestCase("(task (λ () 42))", "42", TestName = "simple task")]
        [TestCase("(chain (task (λ () 1)) (λ (r) (+ r 2)) (λ (r) (+ r 3)))", "6", TestName = "chains")]
        public async Task TestEvalAsync(string script, string expected)
        {
            var scope = new LexicalScope();
            
            Assert.AreEqual(await engine.EvaluateAsync(script, scope), engine.Evaluate(expected, scope), $"await {script} != {expected}");
        }

        [TestCase(@"(|> () (setx a 1) a)", "1", TestName = "plain set, unquoted")]
        [TestCase(@"(|> () (setx a '1) a)", "1", TestName = "plain set, quoted")]
        [TestCase(@"(|> () (setx d (new 'asjp.lithium.Tests.Deep)) (setx (nav d 'Value) 3) (nav d 'Value))", "3", TestName = "navigated set, field, unquoted")]
        [TestCase(@"(|> () (setx d (new 'asjp.lithium.Tests.Deep)) (setx (nav d 'Value) '3) (nav d 'Value))", "3", TestName = "navigated set, field, quoted")]
        [TestCase(@"(|> () (setx d (new 'asjp.lithium.Tests.Deep)) (setx (nav d 'Prop) 3) (nav d 'Prop))", "3", TestName = "navigated set, prop, unquoted")]
        [TestCase(@"(|> () (setx d (new 'asjp.lithium.Tests.Deep)) (setx (nav d 'Prop) '3) (nav d 'Prop))", "3", TestName = "navigated set, prop, quoted")]
        [TestCase(@"(|> () (setx d (new 'asjp.lithium.Tests.Deep)) (setx (nav d 'First) 3) (nav d 'First))", "3", TestName = "navigated set, prop-router, unquoted")]
        [TestCase(@"(|> () (setx d (new 'asjp.lithium.Tests.Deep)) (setx (nav d 'First) '3) (nav d 'First))", "3", TestName = "navigated set, prop-router, quoted")]
        [TestCase(@"(|> () (setx d (new 'asjp.lithium.Tests.Deep)) (setx (nav d 0) '3) (nav d 0))", "3", TestName = "navigated set, indexer, quoted")]
        [TestCase(@"(|> () (setx d (new 'asjp.lithium.Tests.Deep)) (setx (nav d 'X '(1 2)) '3) (nav d 'X '(1 2)))", "3", TestName = "navigated set, indexer, 2 args, quoted")]
        public void TestSetImprovements(string script, string expected)
        {
            engine.Evaluate($"(?! (eq {script} {expected}))", new LexicalScope());
        }
    }
}