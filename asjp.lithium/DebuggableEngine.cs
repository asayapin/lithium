﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using asjp.lithium.Contracts.Atoms;
using asjp.lithium.Contracts.Services;
using asjp.lithium.Defaults;
using asjp.lithium.ObjectModel;

#nullable enable

namespace asjp.lithium
{
    /// <summary>
    /// Interpreter engine allowing to interfere with evaluation process
    /// </summary>
    public class DebuggableEngine
    {
        readonly IScriptParser _parser;
        private IScope? _baseScope;
        public IScope? Scope { get; private set; }
        private readonly HashSet<int> _breakpoints = new();
        private IAtom[]? _atoms;
        private bool _breakpointReached;
        private IEnumerator<IAtom>? _enumerator;
        private bool _considerBreakpoints = true;
        private Task<IAtom>? _evaluation;
        private IAtom? _brokeAtAtom;
        
        DebuggableEngine(IScriptParser parser)
        {
            _parser = parser;
        }

        /// <summary>
        /// Load extensions to current scope
        /// </summary>
        /// <param name="extensions">entity to provide extensions</param>
        /// <returns>Engine instance (for fluent syntax)</returns>
        public DebuggableEngine Extend(IExtensionsContainer extensions)
        {
            _baseScope = _baseScope?.CreateChild(extensions) ?? new LexicalScope(extensions);
            return this;
        }

        /// <summary>
        /// Create engine. Default way to obtain instance of engine
        /// </summary>
        /// <param name="parser">Instance of script parser</param>
        /// <returns>Engine with preloaded standard library</returns>
        public static DebuggableEngine Create(IScriptParser? parser = null) => CreateEmpty(parser).Extend(StdLib.Instance);

        /// <summary>
        /// Create engine without standard library
        /// </summary>
        /// <param name="parser">Instance of script parser</param>
        /// <returns>Engine without standard extensions, still capable of evaluating some scripts</returns>
        /// <remarks>
        /// Since standard library contains method <code>set</code>, you should load your own implementation to be able
        /// to evaluate anything more complex than plain atoms 
        /// </remarks>
        public static DebuggableEngine CreateEmpty(IScriptParser? parser = null) => new(parser ?? new DefaultParser());

        public bool SetBreakpoint(int offset) => _breakpoints.Add(offset);
        public bool ResetBreakpoint(int offset) => _breakpoints.Remove(offset);
        public void ResetAllBreakpoints() => _breakpoints.Clear();

        private IEnumerable<IAtom> GetAllAtoms(IAtom atom) =>
            atom switch
            {
                QuotedAtom q => GetAllAtoms(q.WrappedAtom).Prepend(q),
                ListAtom la => la.Atoms.SelectMany(GetAllAtoms).Prepend(la),
                _ => new[] { atom }
            };

        public (int offset, string atom)[] AtomLocations() =>
            _atoms is null
                ? Array.Empty<(int, string)>()
                : _atoms.SelectMany(GetAllAtoms).Select(x => (x.Position.offset, x.ToString())).OrderBy(x => x.offset).ToArray();

        public (int offset, string atom)[] BreakpointLocations() => AtomLocations().Where(p => _breakpoints.Contains(p.offset)).ToArray();

        public void Load(string script, IScope scope)
        {
            CleanupHandlers();
            _enumerator = null;
            _atoms = _parser.Parse(script);
            Scope = scope.AddParent(_baseScope);
            SetupHandlers();
            _breakpointReached = false;
            _evaluation = null;
            _breakpoints.Clear();
        }

        /// <summary>
        /// Run without hitting breakpoints
        /// </summary>
        /// <returns>evaluation result</returns>
        public object? ForceRun()
        {
            _considerBreakpoints = false;
            var (res, _) = Run();
            _considerBreakpoints = true;
            return res;
        }

        /// <summary>
        /// Run stopping on every breakpoint hit
        /// </summary>
        /// <returns>either evaluation result or current breakpoint offset</returns>
        /// <remarks>this method can be invoked successively until last item in tuple is null</remarks>
        public (object?, int?) Run()
        {
            if (_atoms is null) return (null, null);
            
            _breakpointReached = false;
            _enumerator ??= _atoms.AsEnumerable().GetEnumerator();
            if (_evaluation is not null)
            {
                Task.WaitAny(_evaluation, WaitForBreakpoint(true));
            }
            IAtom? atom;
            while (!_breakpointReached && _enumerator.MoveNext())
            {
                atom = _enumerator.Current;
                if (atom is null) continue;
                Task.WaitAny(
                    _evaluation = Task.Run(() => atom.Evaluate(Scope)),
                    WaitForBreakpoint(true));
            }

            if (_evaluation?.Status == TaskStatus.RanToCompletion)
            {
                // enumeration finished
                _enumerator = null;
                return (_evaluation.Result?.GetValue(Scope), null);
            }

            return (null, _brokeAtAtom?.Position.offset);
        }

        private void SetupHandlers()
        {
            if (_atoms is null) return;
            foreach (var atom in _atoms.SelectMany(GetAllAtoms))
                atom.EvaluationStarted += CheckBreakpointAndSpinIfNeeded;
        }

        private void CleanupHandlers()
        {
            if (_atoms is null) return;
            foreach (var atom in _atoms.SelectMany(GetAllAtoms))
                atom.EvaluationStarted -= CheckBreakpointAndSpinIfNeeded;
        }

        private Task CheckBreakpointAndSpinIfNeeded(IAtom a)
        {
            if (!_considerBreakpoints || !_breakpoints.Contains(a.Position.offset)) return Task.CompletedTask;
            
            return Task.Run(async () =>
            {
                _brokeAtAtom = a;
                _breakpointReached = true;
                await WaitForBreakpoint(false);
            });
        }

        private Task WaitForBreakpoint(bool reached) => WaitFor(() => _breakpointReached == reached);
        private Task WaitFor(Func<bool> condition) => Task.Run(() => SpinWait.SpinUntil(condition));

        public void Reset()
        {
            CleanupHandlers();
            _enumerator = null;
            _atoms = null;
        }
    }
}