﻿using System.Threading.Tasks;
using asjp.lithium.ObjectModel;
using asjp.lithium.Contracts.Services;
using asjp.lithium.Defaults;
using asjp.lithium.ObjectModel.Extensions;

namespace asjp.lithium
{
    /// <summary>
    /// Interpreter engine. Main entry point to interpret LISP code 
    /// </summary>
    public class Engine
    {
        readonly IScriptParser _parser;
        IScope _scope;

        Engine(IScriptParser parser)
        {
            _parser = parser;
        }

        /// <summary>
        /// Load extensions to current scope
        /// </summary>
        /// <param name="extensions">entity to provide extensions</param>
        /// <returns>Engine instance (for fluent syntax)</returns>
        public Engine Extend(IExtensionsContainer extensions)
        {
            _scope = _scope?.CreateChild(extensions) ?? new LexicalScope(extensions);
            return this;
        }

        /// <summary>
        /// Create engine. Default way to obtain instance of engine
        /// </summary>
        /// <param name="parser">Instance of script parser</param>
        /// <returns>Engine with preloaded standard library</returns>
        public static Engine Create(IScriptParser parser = null) => CreateEmpty(parser).Extend(StdLib.Instance);

        /// <summary>
        /// Create engine without standard library
        /// </summary>
        /// <param name="parser">Instance of script parser</param>
        /// <returns>Engine without standard extensions, still capable of evaluating some scripts</returns>
        /// <remarks>
        /// Since standard library contains method <code>set</code>, you should load your own implementation to be able
        /// to evaluate anything more complex than plain atoms 
        /// </remarks>
        public static Engine CreateEmpty(IScriptParser parser = null) => new Engine(parser ?? new DefaultParser());

        /// <summary>
        /// Evaluate script to CLR object
        /// </summary>
        /// <param name="script">Script to evaluate</param>
        /// <param name="scope">Additional scope to evaluate in</param>
        /// <returns>Evaluation result converted to CLR object form</returns>
        public object Evaluate(string script, IScope scope)
        {
            var evalResult = ValueAtom.Nil;
            scope = scope.AddParent(_scope);
            foreach (var atom in _parser.Parse(script))
                evalResult = atom.Evaluate(scope);

            return evalResult.GetValue(scope);
        }

        /// <summary>
        /// Evaluate script asynchronously - waiting on all the created tasks
        /// </summary>
        /// <param name="script">Script to evaluate</param>
        /// <param name="scope">Additional scope to evaluate in</param>
        /// <returns>Evaluation result, unlifted from Task</returns>
        public async Task<object> EvaluateAsync(string script, IScope scope)
        {
            var result = ValueAtom.Nil;
            scope = scope.AddParent(_scope);
            foreach (var atom in _parser.Parse(script)) result = await atom.Evaluate(scope);

            return (await result).GetValue(scope);
        } 
    }
}