# Lithium API reference

- asjp.lithium - core library to use
    - .Contracts - contracts (obviously) used by other parts of solution
    - .Defaults - pre-shipped extensions, loaded when `Engine.Create` is called
    - .ObjectModel - atoms used under the hood, and helpers for them