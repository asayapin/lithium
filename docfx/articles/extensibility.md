﻿# Engine extensibility

The package provides two ways to extend scope/engine with custom functions - 
automatic and sort-of conventional, and more manual one.

Both approaches requires creation of class implementing `asjp.lithium.Contracts.Services.IExtensionsContainer`

## Extending manually

...is as simple as creating the class and implementing the interface:

```c#
public class Example : IExtensionsContainer {
  public IEnumerable<EngineMethodInfo> GetMethodDefinitions() {
    yield return new EngineMethodInfo(
      method: new Func<IAtom, IAtom>(Example),
      names: new[]{ "example" },
      canHide: false
    );
  }
  
  private static IAtom Example(IAtom a) => a.IsT().Wrap();
}
```

`EngineMethodInfo` requires delegate to be passed, thus you should wrap references
to specified methods in Func instantiation.

Each method can specify several names; you can manually control whether method
will overwrite already bound names

## Automatic collection

Such method requires:
- collector class, which will provide instance and will be used for engine/scope extension:
```c#
public class Collector: asjp.lithium.Contracts.Extensibility.ExtensionsPartsCollectorBase
{
  // in general, nothing is required here
  // you can, though, collect extensions from other assemblies:
  Collector(){
    Collect(typeof(TypeInAnotherAssembly).Assembly);
  }
}
```
- create 1+ class holding extension methods:
```c#
[ExtensionPart]
internal class Extensions {
}
```
- declare 1+ methods in extension classes:
```c#
[Names("Declare names here; any symbol except \" and space is allowed")]
public IAtom Test(IAtom a, IScope s) => a.Evaluate(s).IsT().Wrap();
```

## Using extensions

- first, you should load instance of extensions container to engine/scope:
```c#
engine.Extend(container);
scope = new LexicalScope(container);
```
- then, when script is evaluated, names of extension methods are resolved to loaded delegates

## Method conventions

You can specify any signature you want, with following effect:
- specifying return type deriving from IAtom requires, obviously, wrapping, which is done by calling Wrap extension method or manually creating instance of required class
- arguments for `IAtom` parameters passed as-is
  - `Method(IAtom)` receives whatever it was called with in script, without prior evaluation
- if parameter has `[EagerAttribute]`, argument is evaluated prior to passing
- if method has `[EagerAttribute]`, all arguments passed to `IAtom` parameters are evaluated before passing
  - this behavior can be overriden by annotating parameters with `[LazyAttribute]` - arguments passed to such parameters are not evaluated
- if parameter is not assignable to `IAtom`, it's evaluated and then converted to CLR form
- `IScope` can be bound as a parameter, but it should be passed as last parameter
- method can contain variadic parameter (`params`), and all Eager/Lazy evaluation rules apply (per value in array)
- `ref`/`out` parameters are not supported

Refer to asjp.lithium.Tests/ArbitrarySignatureTests.cs for examples