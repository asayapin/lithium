﻿# Quick start guide

**Purpose**: create a setup for script evaluation

Steps:

- setup package source - refer to instruction in any package in [registry](https://gitlab.com/asayapin/lithium/-/packages/)
```powershell
nuget source Add -Name "GitLab" -Source "https://gitlab.com/api/v4/projects/12227895/packages/nuget/index.json" -UserName <your_username> -Password <your_token>
```

> Note: you can use public credentials (read-only for package registry)
> 
> username: `lrp`
> 
> password: `ZDkaFkSMKLFCsHWZ9444`

- add lithium
```powershell
nuget install asjp.lithium -Source "GitLab"
```

in place where evaluation is required:
- add imports
```c#
using asjp.lithium;
using asjp.lithium.Defaults;
```
- instantiate engine and scope
```c#
var engine = Engine.Create();
var scope = new LexicalScope();
```
- evaluate your script
```c#
var result = engine.Evaluate(@"""hello from lithium!""", scope);
```