﻿using System.Collections;
using asjp.lithium.Contracts.Atoms;

namespace lithium.REPL;

public static class AtomFormatter
{
    public static string PrettyPrint(object? o, HashSet<object>? visited = null)
    {
        visited ??= new HashSet<object>();
        if (!visited.Add(o ?? new object()))
            return $"<object {o?.GetHashCode()}>";
        return $"[#{o?.GetHashCode()}] " + o switch
        {
            string s => s,
            ValueType vt => vt.ToString()!,
            Delegate d => string.Join(" -> ",
                d.Method.GetParameters().DefaultIfEmpty().Select(x => x?.ParameterType.Name ?? "()").Append(d.Method.ReturnType.Name)),
            IEnumerable ie => $"[{string.Join(" ", ie.OfType<object>().Select(t => PrettyPrint(t, visited)))}]",
            IAtom a => $"{a} | {PrettyPrint(a.GetValue(State.Scope), visited)}",
            _ => o?.ToString() ?? "nil",
        };
    }    
}