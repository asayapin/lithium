﻿using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using static lithium.REPL.AtomFormatter;
using static lithium.REPL.State;

namespace lithium.REPL;

[SuppressMessage("ReSharper", "UnusedMember.Local")]
[SuppressMessage("ReSharper", "UnusedParameter.Local")]
public static class Commands
{
    [AttributeUsage(AttributeTargets.Method)]
    private class CommandAttribute : Attribute
    {
        public string[] Aliases { get; }

        public CommandAttribute(params string[] aliases)
        {
            Aliases = aliases;
        }
    }
    
    [AttributeUsage(AttributeTargets.Method)]
    private class HelpAttribute : Attribute
    {
        public string Text { get; }

        public HelpAttribute(string text)
        {
            Text = text;
        }
    }
    
    private static readonly Dictionary<string, Func<string[], string?>> commands;

    static Commands()
    {
        commands = new();

        foreach (var (method, attr) in typeof(Commands).GetMethods(BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public)
                     .Select(m => (method: m, attr: m.GetCustomAttribute<CommandAttribute>()))
                     .Where(p => p.attr is not null
                                 && p.method.ReturnType == typeof(string)
                                 && p.method.GetParameters().Select((x, i) => x.ParameterType == typeof(string[]) && i == 0)
                                     .All(x => x)))
        {
            var dlg = Delegate.CreateDelegate(typeof(Func<string[], string>), method) as Func<string[], string>;
            if (dlg is null) continue;
            foreach (var name in attr!.Aliases)
                commands[name] = dlg;
        }
    }

    public static Func<string?>? Process(string input)
    {
        if (!input.StartsWith(":")) return null;

        var cmd = input.Split(new[] { ' ' }, StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

        return commands.TryGetValue(cmd[0].TrimStart(':'), out var func)
            ? () => func(cmd.Skip(1).ToArray())
            : Report($"{cmd[0]}: not found");
    }

    public static Func<string> Report(string s) => () => s;  
    
    [Command("h", "help")]
    [Help("print commands reference")]
    static string Help(string[] _)
    {
        var texts = commands.GroupBy(x => x.Value)
            .Select(x => (names: x.Select(p => p.Key).ToArray(),
                help: x.Key.Method.GetCustomAttribute<HelpAttribute>()?.Text))
            .Select(x => $"{string.Join(" | ", x.names),-24}{x.help}");
        return string.Join("\n\n", texts);
    }

    [Command("q", "quit")]
    [Help("end REPL session")]
    static string Exit(string[] _)
    {
        Environment.Exit(0);
        return "";
    }

    [Command("i", "inspect")]
    [Help("requires <name>; inspect current value of atom bound to <name>")]
    static string Inspect(string[] cmd)
    {
        return cmd.Length < 1
            ? "name is required for inspection"
            : PrettyPrint(Scope!.TryGetData(cmd[1], out var a)
                ? a.GetValue(Scope)
                : $"{cmd[1]} not found in scope");
    }

    [Command("b", "toggle-breakpoint")]
    [Help("requires <offset>; toggle state of breakpoint at <offset>")]
    static string ToggleBreakpoint(string[] cmd)
    {
        if (cmd.Length < 1)
            return ("offset is required to place breakpoint");
        return int.TryParse(cmd[0], out var offset)
            ? $"{offset}: " + (Engine!.SetBreakpoint(offset) ? "set" :
                Engine.ResetBreakpoint(offset) ? "reset" : "failed")
            : $"failed to parse {cmd[0]} as location";
    }

    [Command("c", "clear-breakpoints")]
    [Help("remove all breakpoints")]
    static string ClearBreakpoints(string[] _)
    {
        Engine!.ResetAllBreakpoints();
        return "all breakpoints removed";
    }

    [Command("t", "target-locations")]
    [Help("list all locations which can be associated with a breakpoint")]
    static string ListLocations(string[] _)
    {
        return string.Join(Environment.NewLine, Engine!.AtomLocations().Select(p => $"{p.offset}: {p.atom}"));
    }

    [Command("l", "list-breakpoints")]
    [Help("list all currently active breakpoints")]
    static string ListBreakpoints(string[] _)
    {
        return string.Join(Environment.NewLine, Engine!.BreakpointLocations().Select(p => $"{p.offset}: {p.atom}"));
    }

    [Command("x", "execute")]
    [Help("execute currently loaded script OR continue execution from where it broke")]
    static string Execute(string[] _)
    {
        var (res, off) = Engine!.Run();
        if (off is null) return $"{PrettyPrint(res)}{Environment.NewLine}done";
            
        var (_, atom) = Engine.BreakpointLocations().FirstOrDefault(x => x.offset == off);
        return $"{PrettyPrint(res)}{Environment.NewLine}Breakpoint reached: [{off}] {atom}";
    }
}