using asjp.lithium;
using asjp.lithium.Defaults;
using System.Text;
using lithium.REPL;

State.Engine = DebuggableEngine.Create();
State.Scope = new LexicalScope();
while (true)
{
    try
    {
        Console.Write("> ");
        var script = ReadScript();
        var response = ProcessCommand(script) ?? EvaluateScript(script);
        Console.WriteLine(response());
    }
    catch (Exception e)
    {
        Console.WriteLine(e);
    }
}

Func<string?> EvaluateScript(string script)
{
    State.Engine!.Load(script, State.Scope!);
    return Commands.Report("loaded");
}

Func<string?>? ProcessCommand(string script) => Commands.Process(script);

string ReadScript()
{
    var sb = new StringBuilder();

    while (true)
    {
        var buffer = Console.ReadLine() ?? "";
        var moreInputRequired = buffer.EndsWith('\\');
        if (moreInputRequired)
        {
            Console.Write(". ");
            buffer = buffer.Substring(0, buffer.Length - 1);
        }

        sb.Append(buffer);
        if (!moreInputRequired) break;
    }

    return sb.ToString().Trim();
}