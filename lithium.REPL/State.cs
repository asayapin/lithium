﻿using asjp.lithium;
using asjp.lithium.Contracts.Services;

namespace lithium.REPL;

public static class State
{
    public static IScope? Scope { get; set; }
    public static DebuggableEngine? Engine { get; set; }
}