﻿dotnet new nugetconfig --force
dotnet nuget add source "$CI_SERVER_URL/api/v4/projects/$CI_PROJECT_ID/packages/nuget/index.json" --name pkgfeed --username gitlab-ci-token --password "$CI_JOB_TOKEN" --store-password-in-clear-text --configfile nuget.config
dotnet nuget push ./*.nupkg -k az -s pkgfeed --skip-duplicate