rm *.nupkg
dotnet pack -c Release /p:Version="$(get-date -format yyyy.MM.dd.hhmm)" -o .

rm ./*.REPL.nupkg

cd ./lithium.REPL/
dotnet publish -o ./tools/linux-x64/ -p:PublishSingleFile=true -r linux-x64
dotnet publish -o ./tools/win-x64/ -p:PublishSingleFile=true -r win-x64
dotnet pack -p:NuspecFile=lithium.REPL.nuspec -p:NuspecProperties="version=$(get-date -format yyyy.MM.dd.hhmm)" -o ../