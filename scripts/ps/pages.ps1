﻿dotnet add asjp.lithium.Tests/asjp.lithium.Tests.csproj package docfx.console
dotnet restore asjp.lithium.Tests/asjp.lithium.Tests.csproj
$dfx = $(resolve-path ~/.nuget/packages/*docfx*/*/tools/docfx.exe).Path
Copy-Item ./README.md ./docfx/index.md
& $dfx docfx/docfx.json --force